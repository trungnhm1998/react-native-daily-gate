import * as Regex from './regex'
import * as RSAService from './rsa-service'
import APIManager from './api-manager'

const AppHelper = {
  Regex,
  RSAService,
  APIManager
}

export default AppHelper

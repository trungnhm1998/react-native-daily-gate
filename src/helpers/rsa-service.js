import './shim'
import constants from 'constants'
import crypto from 'crypto'
import JSEncrypt from 'jsencrypt'
import CryptoJS from 'crypto-js'
import AppConstants from '../configs/constant.config'


const verifySignature = (message, signature) => {
  const verify = new JSEncrypt()
  verify.setPublicKey(AppConstants.KEY.PUBLIC)
  const verified = verify.verify(message, signature, CryptoJS.SHA1)
  return verified
}

const createSignature = (message) => {
  const sign = new JSEncrypt()
  sign.setPrivateKey(AppConstants.KEY.PRIVATE)
  const signature = sign.sign(message, CryptoJS.SHA1, 'sha1')

  return signature
}

const Encrypt = (message) => {
  const publicKey = `-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCI6T12WztLVpN3yV1JIzbqT4Tc
GohOXOtEjNmSvutblJZSSiesqLbNTPy9prmn1xswECPpO+YaxZkuD8PCQYtI+HGM
i51ICZfxTW6EKiIwFbfy9M4hSvqE5o7W8SgC163+s72WrT1WIVyk6lUfV4u36Wz2
I0qVvZ0ZiQlqWGt8/wIDAQAB
-----END PUBLIC KEY-----`
  console.log('message', message)
  let buffer = Buffer.from(message)
  let encrypted = crypto.publicEncrypt({
    key: AppConstants.KEY.PUBLIC,
    padding: constants.RSA_PKCS1_OAEP_PADDING
  }, buffer)
  const encryptedData = encrypted.toString('base64')
  return encryptedData
}

const Decrypt = (message) => {
  const buffer = Buffer.from(message, 'base64')
  const decrypted = crypto.privateDecrypt({
    key: AppConstants.KEY.PRIVATE,
    padding: constants.RSA_PKCS1_OAEP_PADDING
  }, buffer)
  let decryptedString = Buffer.from(decrypted).toString('utf8')
  decryptedString = JSON.stringify(decryptedString)
  decryptedString = decryptedString.replace(/\\u0000/g, '')
  decryptedString = JSON.parse(decryptedString)
  return decryptedString
}

export {
  verifySignature,
  createSignature,
  Encrypt,
  Decrypt
}
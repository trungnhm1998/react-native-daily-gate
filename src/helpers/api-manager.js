import RequestDocdac from '../api/middleware/daily-gate.request'

export default class APIManager {
  static instance = null

  apiMap = null

  constructor() {
    this.apiMap = new Map()
  }

  static getInstance() {
    if (APIManager.instance === null) {
      APIManager.instance = new APIManager()
    }
    return this.instance
  }

  callAPI(api = '', data = undefined, method = 'post', timeout = 0) {
    // timeout > 0 means need timeout for this api
    const apiObj = this.apiMap.get(api)
    const currentTime = (new Date()).getTime()
    if (apiObj === undefined || (timeout > 0 && currentTime - apiObj.lastCalled >= timeout)) {
      this.apiMap.set(api, {
        lastCalled: currentTime,
        timeout
      })
      return RequestDocdac.callAPI(api, data, method)
    }

    return null
  }
}

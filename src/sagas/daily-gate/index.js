import GetBanners from './getBanners.saga'
import RenewAccessToken from './renewAccessToken.saga'
import GetInitConfig from './getInitConfig.saga'
import GetServicePlaces from './getServicePlaces.saga'
import AuthorizeUser from './authorizeUser.saga'
import CheckAccessToken from './checkAccessToken.saga'
import GetInforOption from './getInforOption.saga'
import checkAccountExist from './checkAccountExist.saga'
import AuthorizeFingerprint from './authorizeFingerprint.saga'
import VerifyInforRegister from './verifyInfoRegister.saga'
import GetListNewsByCategory from './getListNewsByCategory.saga'
import GetListAddress from './getListAddress.saga'
import CreateNewAccount from './createNewAccount.saga'
import UpdateAccountPassword from './updateAccountPassword.saga'
import GetNewsDetail from './getNewsDetail.saga'
import GetListCashDeposit from './getListCashDeposit.saga'
import GetBankList from './getBankList.saga'
const DailyGateSagas = {
  GetBanners,
  GetServicePlaces,
  RenewAccessToken,
  GetInitConfig,
  AuthorizeUser,
  CheckAccessToken,
  GetInforOption,
  AuthorizeFingerprint,
  VerifyInforRegister,
  checkAccountExist,
  GetListNewsByCategory,
  GetListAddress,
  CreateNewAccount,
  UpdateAccountPassword,
  GetNewsDetail,
  GetListCashDeposit,
  GetBankList
}

export default DailyGateSagas

import { takeLatest, call, put } from 'redux-saga/effects'
import { DailyGateTypes } from '../../redux/daily-gate'
import AppAPI from '../../api'
import AppResponseCode from '../../configs/response-code.config'

function* doAction(action) {
  try {
    yield put({ type: DailyGateTypes.SHOW_LOADING })

    const { data, code } = yield call(AppAPI.DailyGateAPI.getListCashDeposit, {})

    if (code === AppResponseCode.SUCCESS) {
      console.log('data return', data)
      yield put({
        type: DailyGateTypes.GET_LIST_CASH_DEPOSIT_SUCCESS,
        payload: {
          listCashDeposit: data
        }
      })
      yield put({ type: DailyGateTypes.HIDE_LOADING })
    } else {
      yield put({
        type: DailyGateTypes.GET_LIST_CASH_DEPOSIT_FAIL,
      })
      yield put({ type: DailyGateTypes.HIDE_LOADING })
    }
  } catch (error) {
    yield put({ type: DailyGateTypes.HIDE_LOADING })
  }
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.GET_LIST_CASH_DEPOSIT, doAction)
}

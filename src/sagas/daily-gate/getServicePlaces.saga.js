import { takeLatest, call, put } from 'redux-saga/effects'
import { DailyGateTypes } from '../../redux/daily-gate'

function* doAction(action) {
  // const { mockupPayload } = action.payload
  // calling api some where here

  // get the data from api
  const mockupPayload = {
    service_place_list: [
      {
        city: 'Vung Tau',
        provine: 'Phuong 1',
        placeName: 'Cua hang vien thong',
        address: '1000 Nguyen An Ninh',
        phone: '19002099',
        latitude: 'XXX',
        longtitude: 'YYY'
      },
      {
        city: 'Vung Tau',
        provine: 'Phuong 1',
        placeName: 'Cua hang vien thong',
        address: '1000 Nguyen An Ninh',
        phone: '19002099',
        latitude: 'XXX',
        longtitude: 'YYY'
      },
      {
        city: 'Vung Tau',
        provine: 'Phuong 1',
        placeName: 'Cua hang vien thong',
        address: '1000 Nguyen An Ninh',
        phone: '19002099',
        latitude: 'XXX',
        longtitude: 'YYY'
      }
    ]
  }

  yield put({ type: DailyGateTypes.GET_SERVICE_PLACE_SUCCESS, payload: mockupPayload.service_place_list })
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.GET_SERVICE_PLACE, doAction)
}

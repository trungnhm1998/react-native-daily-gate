import { takeLatest, call, put } from 'redux-saga/effects'
import { DailyGateTypes } from '../../redux/daily-gate'
import AppResponseCode from '../../configs/response-code.config'
import AppAPI from '../../api'

function* doAction(action) {
  try {
    yield put({ type: DailyGateTypes.SHOW_LOADING })
    const { status_code, version, data } = yield call(AppAPI.DailyGateAPI.getInitConfig)
    const { code } = data

    switch (code) {
      case AppResponseCode.SUCCESS:
        yield put({ type: DailyGateTypes.HIDE_LOADING })
        yield put({ type: DailyGateTypes.GET_INIT_CONFIG_SUCCESS, payload: data })
        break
      default:
        yield put({ type: DailyGateTypes.HIDE_LOADING })
        yield put({ type: DailyGateTypes.GET_INIT_CONFIG_FAIL })
        break
    }
  } catch (error) {
    yield put({ type: DailyGateTypes.HIDE_LOADING })
    yield put({ type: DailyGateTypes.GET_INIT_CONFIG_FAIL })
  }
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.GET_INIT_CONFIG, doAction)
}

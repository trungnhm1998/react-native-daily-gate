import { takeLatest, call, put } from 'redux-saga/effects'
import DailyGateTypes from '../../redux/daily-gate/daily-gate.types'
import AppResponseCode from '../../configs/response-code.config'

// import { accountAPI, docdacAPI } from '../../api'

function* doAction(action) {
  const { phone, password, navigator } = action.payload

  const mockupPayload = {
    code: 1000,
    data: {
      accountId: 'wqoiu342io12',
      money: 15000000,
      email: 'abc@gmail.com',
      identityNumber: '123123123',
      frontIdentityUrl: 'https://example.com/front.jpg',
      backIdentityUrl: 'https://example.com/back.jpg',
      address: '123 nguyen tat thanh',
      city: 'Hồ Chí Minh',
      distrist: 'Quận 7',
      displayName: 'SevenA',
      avatar: 'https://example.com/avatar/qowueoquoqwirq.png',
      phone: '84909000100',
      isVerify: 'false',
      accessToken: '1OTEiLCJpYXQiOjE0OTQ1NzY5NzR9.Xu_vUlTCaGXUpSv8P6YkFKz4OrxRebie7cB2uFYC1us==',
      tradeSecurity: 0
    },
    message: 'nice'
  }
  const { code, data, message } = mockupPayload
  switch (code) {
    case AppResponseCode.SUCCESS: {
      yield put({
        type: DailyGateTypes.AUTHORIZE_USER_SUCCESS,
        payload: data
      })

      navigator.nextScreen()
      break
    }
    case AppResponseCode.AUTHORIZE_FAIL: {
      yield put({
        type: DailyGateTypes.AUTHORIZE_USER_FAIL,
        payload: {
          code,
          message
        }
      })
      break
    }
    default: {
      return null
    }
  }
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.AUTHORIZE_FINGERPRINT, doAction)
}

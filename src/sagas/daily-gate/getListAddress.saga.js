import { takeLatest, call, put } from 'redux-saga/effects'
import { DailyGateTypes } from '../../redux/daily-gate'
import AppAPI from '../../api'
import AppResponseCode from '../../configs/response-code.config'

function* doAction(action) {
  try {
    yield put({ type: DailyGateTypes.SHOW_LOADING })

    const { cityList, codeCity } = yield call(AppAPI.DailyGateAPI.getListAddress, {})

    if (codeCity === AppResponseCode.SUCCESS) {
      console.log('data return', cityList)
      yield put({
        type: DailyGateTypes.GET_LIST_ADDRESS_SUCCESS,
        payload: {
          listCity: cityList
        }
      })
      yield put({ type: DailyGateTypes.HIDE_LOADING })
    } else {
      yield put({
        type: DailyGateTypes.GET_LIST_ADDRESS_FAIL,
      })
      yield put({ type: DailyGateTypes.HIDE_LOADING })
    }
  } catch (error) {
    yield put({ type: DailyGateTypes.HIDE_LOADING })
  }
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.GET_LIST_ADDRESS, doAction)
}

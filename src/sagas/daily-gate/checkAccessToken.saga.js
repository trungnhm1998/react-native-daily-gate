import { takeLatest, call, put } from 'redux-saga/effects'
import DailyGateTypes from '../../redux/daily-gate/daily-gate.types'
import AppResponseCode from '../../configs/response-code.config'
import AppAPI from '../../api';

// import { accountAPI, docdacAPI } from '../../api'

function* doAction(action) {
  const { accessToken, navigator } = action.payload

  yield put({ type: DailyGateTypes.SHOW_LOADING })

  const { code, data, message } = yield call(AppAPI.DailyGateAPI.checkAccessToken, { accessToken })

  switch (code) {
    case AppResponseCode.SUCCESS: {
      yield put({
        type: DailyGateTypes.CHECK_TOKEN_VALIDATE,
        payload: data
      })
      yield put({ type: DailyGateTypes.HIDE_LOADING })
      navigator.navToHome()
      break
    }
    case AppResponseCode.CHECK_TOKEN_INVALIDATE: {
      yield put({
        type: DailyGateTypes.CHECK_TOKEN_INVALIDATE,
        payload: {
          code,
          message
        }
      })
      yield put({ type: DailyGateTypes.HIDE_LOADING })
      navigator.navToLogin()
      break
    }
    default: {
      yield put({ type: DailyGateTypes.HIDE_LOADING })
      return null
    }
  }
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.CHECK_TOKEN, doAction)
}

import { takeLatest, call, put } from 'redux-saga/effects'
import DailyGateTypes from '../../redux/daily-gate/daily-gate.types'
import AppResponseCode from '../../configs/response-code.config'
import AppAPI from '../../api'
import { apiActions } from '../../redux/api'

// import { accountAPI, docdacAPI } from '../../api'

function* doAction(action) {
  yield put({ type: DailyGateTypes.SHOW_LOADING })

  const { code, data, message } = yield call(AppAPI.DailyGateAPI.getBankList)
  yield put(apiActions.storeResponseMessage({ code, message }))

  switch (code) {
    case AppResponseCode.SUCCESS: {
      yield put({
        type: DailyGateTypes.GET_BANK_LIST_SUCCESS,
        payload: data.bankList
      })
      yield put({ type: DailyGateTypes.HIDE_LOADING })

      break
    }
    case AppResponseCode.GET_BANK_LIST_FAIL: {
      yield put({
        type: DailyGateTypes.GET_BANK_LIST_FAIL,
        payload: {
          code,
          message
        }
      })
      yield put({ type: DailyGateTypes.HIDE_LOADING })
      break
    }
    default: {
      return null
    }
  }
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.GET_BANK_LIST, doAction)
}

import { takeLatest, call, put } from 'redux-saga/effects'
import { DailyGateTypes } from '../../redux/daily-gate'
import AppImages from '../../assets/images'

function* doAction(action) {
  // const { mockupPayload } = action.payload
  // calling api some where here

  // get the data from api
  const mockupPayload = {
    adsBannerList: [
      { id: 'banner-01', bannerSrc: AppImages.iconHomeBannerDemo },
      { id: 'banner-02', bannerSrc: AppImages.iconHomeBannerDemo },
      { id: 'banner-03', bannerSrc: AppImages.iconHomeBannerDemo },
      { id: 'banner-04', bannerSrc: AppImages.iconHomeBannerDemo }
    ]
  }

  yield put({ type: DailyGateTypes.GET_BANNER_SUCCESS, payload: mockupPayload })
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.GET_BANNER, doAction)
}

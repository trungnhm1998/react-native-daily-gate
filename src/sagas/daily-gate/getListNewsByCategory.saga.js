import { takeLatest, call, put } from 'redux-saga/effects'
import DailyGateTypes from '../../redux/daily-gate/daily-gate.types'
import AppResponseCode from '../../configs/response-code.config'
import AppAPI from '../../api'
import { apiActions } from '../../redux/api'

// import { accountAPI, docdacAPI } from '../../api'

function* doAction(action) {
  const { id, page } = action.payload

  yield put({
    type: DailyGateTypes.LOADING_LIST_NEW,
    payload: {
      page
    }
  })
  const { code, listNews, message } = yield call(AppAPI.DailyGateAPI.getListNewsByCategory, { id, page })
  yield put(apiActions.storeResponseMessage({ code, message }))

  switch (code) {
    case AppResponseCode.SUCCESS: {
      console.log('data return', listNews)
      yield put({
        type: DailyGateTypes.GET_LIST_NEWS_BY_CATEGORY_SUCCESS,
        payload: {
          listNews,
          page
        }
      })

      break
    }
    case AppResponseCode.GET_LIST_NEWS_BY_CATEGORY_FAIL: {
      yield put({
        type: DailyGateTypes.GET_LIST_NEWS_BY_CATEGORY_FAIL,
        payload: {
          code,
          message,
          page
        }
      })
      break
    }
    default: {
      return null
    }
  }
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.GET_LIST_NEWS_BY_CATEGORY, doAction)
}

import { takeLatest, call, put } from 'redux-saga/effects'
import { DailyGateTypes } from '../../redux/daily-gate'
import AppAPI from '../../api'
import AppResponseCode from '../../configs/response-code.config'

function* doAction(action) {
  try {
    yield put({ type: DailyGateTypes.SHOW_LOADING })
    const { dataList, navigator } = action.payload
    const cmnd = action.payload.cmndNumber
    const { code, data, message } = yield call(AppAPI.DailyGateAPI.checkCMNDNumber, { cmnd })

    if (code === AppResponseCode.SUCCESS) {
      const { code, data } = yield call(AppAPI.DailyGateAPI.verifyInforRegister, { dataList })
      switch (code) {
        case AppResponseCode.SUCCESS: {
          console.log('data return', data)
          yield put({
            type: DailyGateTypes.VERIFY_INFO_REGISTER_SUCCESS,
            payload: {
              infoRegister: data
            }
          })
          yield put({ type: DailyGateTypes.HIDE_LOADING })
          navigator.nextScreen()
          break
        }

        case AppResponseCode.AUTHORIZE_FAIL: {
          yield put({
            type: DailyGateTypes.VERIFY_INFO_REGISTER_FAIL,
            payload: {
              code,
              message
            }
          })
          yield put({ type: DailyGateTypes.HIDE_LOADING })
          break
        }
        default: {
          yield put({ type: DailyGateTypes.HIDE_LOADING })
          return null
        }
      }
    } else {
      yield put({
        type: DailyGateTypes.CHECK_CMND_NUMBER_FAIL,
        payload: {
          code,
          message
        }
      })
      yield put({ type: DailyGateTypes.HIDE_LOADING })
    }
  } catch (error) {
    yield put({ type: DailyGateTypes.HIDE_LOADING })
  }
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.VERIFY_INFO_REGISTER, doAction)
}

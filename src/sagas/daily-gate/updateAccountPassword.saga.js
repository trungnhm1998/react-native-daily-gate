import { takeLatest, call, put } from 'redux-saga/effects'
import { DailyGateTypes } from '../../redux/daily-gate'
import AppResponseCode from '../../configs/response-code.config'
import AppAPI from '../../api'
import { apiActions } from '../../redux/api'
import AppHelper from '../../helpers';

function* doAction(action) {
  try {
    yield put({ type: DailyGateTypes.SHOW_LOADING })
    console.log('saga::updateAccountPassword', action.payload)
    const { phoneNumber, password, activationCode } = action.payload
    // check activationCode/otp first if passed then hash the password
    // after that create new account
    const { code, message } = yield call(AppAPI.DailyGateAPI.checkOtp, { otp: activationCode })
    let otpPassed = false
    yield put(apiActions.storeResponseMessage({ code, message }))
    switch (code) {
      case AppResponseCode.SUCCESS: {
        // { code, message }
        otpPassed = true
        break
      }
      default:
        yield put({ type: DailyGateTypes.HIDE_LOADING })
        yield put({ type: DailyGateTypes.CHECK_OTP_FAIL })
        break
    }

    if (otpPassed) {
      const encryptedPassword = AppHelper.RSAService.Encrypt(password)
      const updateAccountPassword = yield call(AppAPI.DailyGateAPI.updateAccountPassword, { phoneNumber, encryptedPassword })
      switch (updateAccountPassword.code) {
        case AppResponseCode.SUCCESS: {
          // { code, message }
          yield put({ type: DailyGateTypes.HIDE_LOADING })
          yield put({ type: DailyGateTypes.UPDATE_ACCOUNT_PASSWORD_SUCCESS })
          break
        }
        default:
          yield put({ type: DailyGateTypes.HIDE_LOADING })
          yield put({ type: DailyGateTypes.UPDATE_ACCOUNT_PASSWORD_FAIL })
          break
      }
    }
    // const { code, message } = yield call(AppAPI.DailyGateAPI.createNewAccount, { phoneNumber, password })
  } catch (error) {
    yield put({ type: DailyGateTypes.HIDE_LOADING })
    yield put({ type: DailyGateTypes.UPDATE_ACCOUNT_PASSWORD_FAIL })
  }
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.UPDATE_ACCOUNT_PASSWORD, doAction)
}

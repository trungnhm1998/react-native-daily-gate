import { takeLatest, call, put } from 'redux-saga/effects'
import { DailyGateTypes } from '../../redux/daily-gate'
import appSymbol from '../../assets/symbols'

function* doAction(action) {
  // get data form api
  const mockupPayload = {
    someItem: [{
      nameIcon: appSymbol.icon_home_ser_1_dt,
      url: '',
      title: 'NẠP TIỀN ĐIỆN THOẠI'
    },
    {
      nameIcon: appSymbol.icon_home_ser_2_card,
      url: '',
      title: 'MUA MÃ THẺ CÀO'
    },
    {
      nameIcon: appSymbol.icon_home_ser_3_game,
      url: '',
      title: 'NẠP TIỀN GAME'
    },
    {
      nameIcon: appSymbol.icon_home_ser_4_card,
      url: '',
      title: 'MUA NHIỀU LOẠI THẺ'
    },
    {
      nameIcon: appSymbol.icon_home_ser_5_bill,
      url: '',
      title: 'THANH TOÁN HOÁ ĐƠN'
    },
    {
      nameIcon: appSymbol.icon_home_ser_6_sim,
      url: '',
      title: 'CHUYỂN TIỀN SIM ĐA NĂNG'
    },
    {
      nameIcon: appSymbol.icon_home_ser_7_sevice,
      url: '',
      title: 'DỊCH VỤ THANH TOÁN'
    },
    ]
  }
  yield put({ type: DailyGateTypes.GET_INFOR_OPTION_SUCCESS, payload: mockupPayload })
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.GET_INFOR_OPTION, doAction)
}
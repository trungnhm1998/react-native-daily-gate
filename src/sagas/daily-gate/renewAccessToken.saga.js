import { takeLatest, call, put } from 'redux-saga/effects'
import { DailyGateTypes } from '../../redux/daily-gate'

function* doAction(action) {
  // const { mockupPayload } = action.payload
  // calling api some where here

  console.info('current AccessToken', action.payload.currentAccessToken)

  // get the data from api
  const mockupPayload = {
    accessToken: 'someNewAccessTokenHere'
  }

  yield put({ type: DailyGateTypes.RENEW_ACCESS_TOKEN_SUCCESS, payload: mockupPayload })
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.RENEW_ACCESS_TOKEN, doAction)
}

import { takeLatest, call, put } from 'redux-saga/effects'
import DailyGateTypes from '../../redux/daily-gate/daily-gate.types'
import AppResponseCode from '../../configs/response-code.config'
import AppAPI from '../../api'
import { apiActions } from '../../redux/api'

// import { accountAPI, docdacAPI } from '../../api'

function* doAction(action) {
  const { phone, password, navigator } = action.payload

  yield put({ type: DailyGateTypes.SHOW_LOADING })

  const { code, data, message } = yield call(AppAPI.DailyGateAPI.authorizeUser, { phone, password })
  yield put(apiActions.storeResponseMessage({ code, message }))

  switch (code) {
    case AppResponseCode.SUCCESS: {
      yield put({
        type: DailyGateTypes.AUTHORIZE_USER_SUCCESS,
        payload: data
      })
      yield put({ type: DailyGateTypes.HIDE_LOADING })

      navigator.nextScreen()
      break
    }
    case AppResponseCode.AUTHORIZE_FAIL: {
      yield put({
        type: DailyGateTypes.AUTHORIZE_USER_FAIL,
        payload: {
          code,
          message
        }
      })
      yield put({ type: DailyGateTypes.HIDE_LOADING })
      break
    }
    default: {
      return null
    }
  }
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.AUTHORIZE_USER, doAction)
}

import { takeLatest, call, put } from 'redux-saga/effects'
import { DailyGateTypes } from '../../redux/daily-gate'
import AppAPI from '../../api'
import AppResponseCode from '../../configs/response-code.config'
import { apiActions } from '../../redux/api'
import AppConstants from '../../configs/constant.config';

function* doAction(action) {
  try {
    yield put({ type: DailyGateTypes.SHOW_LOADING })

    const { phoneNumber, signupState, navigator } = action.payload
    const { data, status_code, version} = yield call(AppAPI.DailyGateAPI.checkAccountExist, { username: phoneNumber })
    const { code, message } = data

    let requestSuccess = false

    yield put(apiActions.storeResponseMessage({ code, message }))

    switch (code) {
      case AppResponseCode.PHONE_NUMBER_VALID:
        requestSuccess = true
        yield put({ type: DailyGateTypes.HIDE_LOADING })
        // TODO: call get activity code here
        yield put({ type: DailyGateTypes.CHECK_ACCOUNT_EXIST_SUCCESS })
        break
      case AppResponseCode.PHONE_NUMBER_EXISTS:
        requestSuccess = true
        yield put({ type: DailyGateTypes.HIDE_LOADING })
        // TODO: call get activity code here
        yield put({ type: DailyGateTypes.CHECK_ACCOUNT_EXIST_SUCCESS })
        break
      default:
        yield put({ type: DailyGateTypes.HIDE_LOADING })
        yield put({ type: DailyGateTypes.CHECK_ACCOUNT_EXIST_FAIL, payload: { message } })
        break
    }

    if (requestSuccess) {
      if (signupState === AppConstants.SignupType.SIGN_UP && code !== AppResponseCode.PHONE_NUMBER_EXISTS) {
        // AppAPI.DailyGateAPI.sendActivationCode()
        navigator.navToSignupPassword()
      } else if (signupState === AppConstants.SignupType.FORGOT_PASSWORD) {
        if (code === AppResponseCode.PHONE_NUMBER_VALID) {
          navigator.navToSignup()
        } else {
          // AppAPI.DailyGateAPI.sendActivationCode()
          navigator.navToSignupPassword()
        }
      }
    }
  } catch (error) {
    yield put({ type: DailyGateTypes.HIDE_LOADING })
  }
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.CHECK_ACCOUNT_EXIST, doAction)
}

import { takeLatest, call, put } from 'redux-saga/effects'
import DailyGateTypes from '../../redux/daily-gate/daily-gate.types'
import AppResponseCode from '../../configs/response-code.config'
import AppAPI from '../../api'
import { apiActions } from '../../redux/api'

// import { accountAPI, docdacAPI } from '../../api'

function* doAction(action) {
  const { newsId } = action.payload

  yield put({
    type: DailyGateTypes.LOADING_NEWS_DETAIL,
    payload: {
      newsId
    }
  })
  const { code, data, message } = yield call(AppAPI.DailyGateAPI.getNewsDetail, { newsId })
  yield put(apiActions.storeResponseMessage({ code, message }))

  switch (code) {
    case AppResponseCode.SUCCESS: {
      yield put({
        type: DailyGateTypes.GET_NEWS_DETAIL_SUCCESS,
        payload: {
          data
        }
      })

      break
    }
    case AppResponseCode.GET_NEWS_DETAIL_FAIL: {
      yield put({
        type: DailyGateTypes.GET_NEWS_DETAIL_FAIL,
        payload: {
          code,
          message
        }
      })
      break
    }
    default: {
      return null
    }
  }
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.GET_NEWS_DETAIL, doAction)
}

import { takeLatest, call, put } from 'redux-saga/effects'
import { DailyGateTypes } from '../../redux/daily-gate'
import AppResponseCode from '../../configs/response-code.config'
import AppAPI from '../../api'
import { apiActions } from '../../redux/api'
import AppHelper from '../../helpers'

function* doAction(action) {
  try {
    yield put({ type: DailyGateTypes.SHOW_LOADING })
    console.log('saga::createNewAccount', action.payload)
    const {
      phoneNumber, password, activationCode, navigator 
    } = action.payload
    // check activationCode/otp first if passed then hash the password
    // after that create new account
    const { code, message } = yield call(AppAPI.DailyGateAPI.checkOtp, { otp: activationCode })
    let otpPassed = false
    yield put(apiActions.storeResponseMessage({ code, message }))
    switch (code) {
      case AppResponseCode.SUCCESS: {
        // { code, message }
        otpPassed = true
        break
      }
      default:
        yield put({ type: DailyGateTypes.HIDE_LOADING })
        yield put({ type: DailyGateTypes.CHECK_OTP_FAIL })
        break
    }

    if (otpPassed) {
      const encryptedPassword = AppHelper.RSAService.Encrypt(password)
      const createNewAccountPayload = yield call(AppAPI.DailyGateAPI.createNewAccount, { phoneNumber, encryptedPassword })
      // yield put(apiActions.storeResponseMessage({ createNewAccountPayload.code, createNewAccountPayload.message }))
      switch (createNewAccountPayload.code) {
        case AppResponseCode.SUCCESS: {
          // { code, message }
          yield put({ type: DailyGateTypes.HIDE_LOADING })
          yield put({ type: DailyGateTypes.CREATE_NEW_ACCOUNT_SUCCESS })
          navigator.LatoBold()
          break
        }
        default:
          yield put({ type: DailyGateTypes.HIDE_LOADING })
          yield put({ type: DailyGateTypes.CREATE_NEW_ACCOUNT_FAIL })
          break
      }
    }
    // const { code, message } = yield call(AppAPI.DailyGateAPI.createNewAccount, { phoneNumber, password })
  } catch (error) {
    yield put({ type: DailyGateTypes.HIDE_LOADING })
    yield put({ type: DailyGateTypes.CREATE_NEW_ACCOUNT_FAIL })
  }
}

export default function* watchAction() {
  yield takeLatest(DailyGateTypes.CREATE_NEW_ACCOUNT, doAction)
}

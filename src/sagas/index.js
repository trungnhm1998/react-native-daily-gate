import { fork, all } from 'redux-saga/effects'
import DailyGateSagas from './daily-gate'
// import MockupSagas from './mockup';

function* rootSagas() {
  yield all([
    // fork(MockupSagas.Mockup),
    fork(DailyGateSagas.GetBanners),
    fork(DailyGateSagas.GetServicePlaces),
    fork(DailyGateSagas.RenewAccessToken),
    fork(DailyGateSagas.GetInitConfig),
    fork(DailyGateSagas.AuthorizeUser),
    fork(DailyGateSagas.CheckAccessToken),
    fork(DailyGateSagas.GetInforOption),
    fork(DailyGateSagas.AuthorizeFingerprint),
    fork(DailyGateSagas.VerifyInforRegister),
    fork(DailyGateSagas.checkAccountExist),
    fork(DailyGateSagas.GetListNewsByCategory),
    fork(DailyGateSagas.GetListAddress),
    fork(DailyGateSagas.CreateNewAccount),
    fork(DailyGateSagas.UpdateAccountPassword),
    fork(DailyGateSagas.GetNewsDetail),
    fork(DailyGateSagas.GetListCashDeposit),
    fork(DailyGateSagas.GetBankList)
  ])
}

export default rootSagas

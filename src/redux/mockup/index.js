import MockupActions from './mockup.actions'
import MockupTypes from './mockup.types'

export {
  MockupActions,
  MockupTypes,
}

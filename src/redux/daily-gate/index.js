import gateActions from './daily-gate.actions'
import DailyGateTypes from './daily-gate.types'

export {
  gateActions,
  DailyGateTypes,
}

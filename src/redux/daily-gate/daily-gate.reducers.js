import DailyGateTypes from './daily-gate.types'

const initState = {
  isLoading: false,
  adsBannerList: [],
  accessToken: '',
  userInfo: {
    avatarUrl: '',
    name: '',
    phoneNumber: '',
    email: ''
  },
  report: {
    maGD: '', // '25452522',
    time: '', // '26/06/2018 12:47:43',
    nameAccount: '', // '09738131341',
    content: '', // 'Nap tien vao so 0124563124583',
    money: 0, // -10000,
    lastMoney: 0, // 145452,
    timeStart: '', // ’06/07/2018’
    timeEnd: '', // ’06/07/2018’
    sodudauki: 0, // ‘100000’
    soducuoiki: 0 // ‘1000000’
  },
  renewAccessTokenInterval: 5000,
  servicePlaceList: [],
  APIRespond: {},
  loginFailCount: 0,
  isLoadingListNew: true,
  currentListNew: [],
  isLoadingNewsDetail: true,
  currentNewsDetail: null,
  bankList: []
}

export default function gateReducer(state = initState, action) {
  switch (action.type) {
    case DailyGateTypes.GET_BANNER_SUCCESS:
      return {
        ...state,
        payload: action.payload
      }
    case DailyGateTypes.GET_BANNER_FAIL:
      return {
        ...state
      }
    case DailyGateTypes.RENEW_ACCESS_TOKEN_SUCCESS: {
      const { accessToken } = action.payload
      return {
        ...state,
        accessToken
      }
    }
    case DailyGateTypes.RENEW_ACCESS_TOKEN_FAIL: {
      return {
        ...state,
        accessToken: ''
      }
    }
    case DailyGateTypes.GET_INIT_CONFIG_SUCCESS: {
      return {
        ...state,
        ...action.payload
      }
    }
    case DailyGateTypes.GET_INIT_CONFIG_FAIL: {
      return {
        ...state
      }
    }
    case DailyGateTypes.GET_SERVICE_PLACE_SUCCESS: {
      return {
        ...state,
        servicePlaceList: action.payload
      }
    }
    case DailyGateTypes.GET_SERVICE_PLACE_FAIL: {
      return {
        ...state
      }
    }
    case DailyGateTypes.AUTHORIZE_USER_SUCCESS: {
      return {
        ...state,
        userInfo: action.payload
      }
    }
    case DailyGateTypes.AUTHORIZE_USER_FAIL: {
      return {
        ...state,
        APIRespond: { code: action.payload.code, message: action.payload.message },
        loginFailCount: state.loginFailCount + 1
      }
    }
    case DailyGateTypes.CHECK_CMND_NUMBER_SUCCESS: {
      return {
        ...state,
        cmndList: action.payload.cmndNumber
      }
    }
    case DailyGateTypes.CHECK_CMND_NUMBER_FAIL: {
      return {
        ...state,
        APIRespond: { code: action.payload.code, message: action.payload.message }
      }
    }
    case DailyGateTypes.CHECK_TOKEN_VALIDATE: {
      return {
        ...state,
        userInfo: action.payload.userInfo
      }
    }
    case DailyGateTypes.CHECK_TOKEN_INVALIDATE: {
      return {
        ...state,
        APIRespond: { code: action.payload.code, message: action.payload.message }
      }
    }
    case DailyGateTypes.GET_INFOR_OPTION: {
      return {
        ...state
      }
    }
    case DailyGateTypes.GET_INFOR_OPTION_SUCCESS: {
      return {
        ...state,
        ...action.payload
      }
    }
    case DailyGateTypes.CHECK_ACCOUNT_EXIST_SUCCESS: {
      return {
        ...state
      }
    }
    case DailyGateTypes.CHECK_ACCOUNT_EXIST_FAIL: {
      return {
        ...state
      }
    }
    case DailyGateTypes.CREATE_NEW_ACCOUNT_SUCCESS: {
      return {
        ...state
      }
    }
    case DailyGateTypes.CREATE_NEW_ACCOUNT_FAIL: {
      return {
        ...state
      }
    }
    case DailyGateTypes.UPDATE_ACCOUNT_PASSWORD_SUCCESS: {
      return {
        ...state
      }
    }
    case DailyGateTypes.UPDATE_ACCOUNT_PASSWORD_FAIL: {
      return {
        ...state
      }
    }
    case DailyGateTypes.SHOW_LOADING: {
      return {
        ...state,
        isLoading: true
      }
    }
    case DailyGateTypes.HIDE_LOADING: {
      return {
        ...state,
        isLoading: false
      }
    }
    // if load new list then remove old list
    case DailyGateTypes.LOADING_LIST_NEW: {
      return {
        ...state,
        isLoadingListNew: true,
        currentListNew: action.payload.page === 1 ? [] : state.currentListNew
      }
    }
    // if user's viewing page is 1 , then this is first time their request list new for this category,
    // just replace currentListNew by a new list, else then concat it with the old list
    case DailyGateTypes.GET_LIST_NEWS_BY_CATEGORY_SUCCESS: {
      return {
        ...state,
        currentListNew: action.payload.page === 1 ? action.payload.listNews : state.currentListNew.concat(action.payload.listNews),
        isLoadingListNew: false
      }
    }
    // if user load more fail, then return the old list, else return an empty list
    case DailyGateTypes.GET_LIST_NEWS_BY_CATEGORY_FAIL: {
      return {
        ...state,
        isLoadingListNew: false,
        currentListNew: action.payload.page === 1 ? [] : state.currentListNew
      }
    }

    case DailyGateTypes.VERIFY_INFO_REGISTER_SUCCESS: {
      return {
        ...state,
        ...action.payload
      }
    }
    case DailyGateTypes.VERIFY_INFO_REGISTER_FAIL: {
      return {
        ...state,
        APIRespond: { code: action.payload.code, message: action.payload.message }
      }
    }

    case DailyGateTypes.GET_LIST_ADDRESS_SUCCESS: {
      return {
        ...state,
        ...action.payload
      }
    }
    case DailyGateTypes.CLEAR_LIST_NEWS: {
      return {
        ...state,
        currentListNew: []
      }
    }
    case DailyGateTypes.LOADING_NEWS_DETAIL: {
      return {
        ...state,
        isLoadingNewsDetail: true
      }
    }
    case DailyGateTypes.GET_NEWS_DETAIL_SUCCESS: {
      return {
        ...state,
        isLoadingNewsDetail: false,
        currentNewsDetail: action.payload.data
      }
    }
    case DailyGateTypes.GET_NEWS_DETAIL_FAIL: {
      return {
        ...state,
        isLoadingNewsDetail: false
      }
    }
    case DailyGateTypes.CLEAR_NEWS_DETAIL: {
      return {
        ...state,
        currentNewsDetail: null
      }
    }
    case DailyGateTypes.GET_LIST_CASH_DEPOSIT_SUCCESS: {
      return {
        ...state,
        ...action.payload
      }
    }
    case DailyGateTypes.GET_BANK_LIST_SUCCESS: {
      return {
        ...state,
        bankList: action.payload
      }
    }
    default:
      return state
  }
}

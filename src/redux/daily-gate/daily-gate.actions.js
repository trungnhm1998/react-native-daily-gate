import DailyGateTypes from './daily-gate.types'

const getBanners = () => {
  return {
    type: DailyGateTypes.GET_BANNER
  }
}
const authorizeUser = (phone, password, navigator) => {
  return {
    type: DailyGateTypes.AUTHORIZE_USER,
    payload: { phone, password, navigator }
  }
}
const authorizeFingerprint = (phone, password, navigator) => {
  return {
    type: DailyGateTypes.AUTHORIZE_USER,
    payload: { phone, password, navigator }
  }
}

const checkAccessToken = (token, navigator) => {
  return {
    type: DailyGateTypes.CHECK_TOKEN,
    payload: { navigator }
  }
}

const getServicePlaces = () => {
  return {
    type: DailyGateTypes.GET_SERVICE_PLACE
  }
}

const renewAccessToken = (currentAccessToken) => {
  return {
    type: DailyGateTypes.RENEW_ACCESS_TOKEN,
    payload: { currentAccessToken }
  }
}

const getInitConfig = () => {
  return {
    type: DailyGateTypes.GET_INIT_CONFIG
  }
}

const getInforOption = () => {
  return {
    type: DailyGateTypes.GET_INFOR_OPTION
  }
}

const checkAccountExist = (phoneNumber, signupState, navigator) => {
  return {
    type: DailyGateTypes.CHECK_ACCOUNT_EXIST,
    payload: { phoneNumber, signupState, navigator }
  }
}

const createNewAccount = (phoneNumber, password, activationCode, navigator) => {
  return {
    type: DailyGateTypes.CREATE_NEW_ACCOUNT,
    payload: {
      phoneNumber, password, activationCode, navigator 
    }
  }
}

const updateAccountPassword = (phoneNumber, password, activationCode) => {
  return {
    type: DailyGateTypes.UPDATE_ACCOUNT_PASSWORD,
    payload: { phoneNumber, password, activationCode }
  }
}

const getListNewsByCategory = (id, page) => {
  return {
    type: DailyGateTypes.GET_LIST_NEWS_BY_CATEGORY,
    payload: { id, page }
  }
}

const checkCMNDNumber = (cmndNumber) => {
  return {
    type: DailyGateTypes.CHECK_CMND_NUMBER,
    payload: { cmndNumber }
  }
}

const verifyInfoRegister = (dataList, navigator) => {
  return {
    type: DailyGateTypes.VERIFY_INFO_REGISTER,
    payload: { dataList, navigator }
  }
}

const getListAddress = () => {
  return {
    type: DailyGateTypes.GET_LIST_ADDRESS
  }
}

const clearListNews = () => {
  return {
    type: DailyGateTypes.CLEAR_LIST_NEWS
  }
}

const getNewsDetail = (newsId) => {
  return {
    type: DailyGateTypes.GET_NEWS_DETAIL,
    payload: { newsId }
  }
}
const clearNewsDetail = () => {
  return {
    type: DailyGateTypes.CLEAR_NEWS_DETAIL
  }
}

const getListCashDeposit = () => {
  return {
    type: DailyGateTypes.GET_LIST_CASH_DEPOSIT
  }
}
const getBankList = () => {
  return {
    type: DailyGateTypes.GET_BANK_LIST
  }
}

const gateActions = {
  getBanners,
  renewAccessToken,
  getServicePlaces,
  getInitConfig,
  authorizeUser,
  checkAccessToken,
  getInforOption,
  checkCMNDNumber,
  checkAccountExist,
  authorizeFingerprint,
  getListNewsByCategory,
  verifyInfoRegister,
  getListAddress,
  createNewAccount,
  updateAccountPassword,
  clearListNews,
  getNewsDetail,
  clearNewsDetail,
  getListCashDeposit,
  getBankList
}

export default gateActions

const DailyGateTypes = {
  HIDE_LOADING: 'HIDE_LOADING',
  SHOW_LOADING: 'SHOW_LOADING',

  GET_BANNER: 'GET_BANNER',
  GET_BANNER_SUCCESS: 'GET_BANNER_SUCCESS',
  GET_BANNER_FAIL: 'GET_BANNER_FAIL',
  GET_SERVICE_PLACE: 'GET_SERVICE_PLACE',
  GET_SERVICE_PLACE_SUCCESS: 'GET_SERVICE_PLACE_SUCCESS',
  GET_SERVICE_PLACE_FAIL: 'GET_SERVICE_PLACE_FAIL',

  RENEW_ACCESS_TOKEN: 'RENEW_ACCESS_TOKEN',
  RENEW_ACCESS_TOKEN_SUCCESS: 'RENEW_ACCESS_TOKEN_SUCCESS',
  RENEW_ACCESS_TOKEN_FAIL: 'RENEW_ACCESS_TOKEN_FAIL',

  CREATE_NEW_ACCOUNT: 'CREATE_NEW_ACCOUNT',
  CREATE_NEW_ACCOUNT_SUCCESS: 'CREATE_NEW_ACCOUNT_SUCCESS',
  CREATE_NEW_ACCOUNT_FAIL: 'CREATE_NEW_ACCOUNT_FAIL',

  UPDATE_ACCOUNT_PASSWORD: 'UPDATE_ACCOUNT_PASSWORD',
  UPDATE_ACCOUNT_PASSWORD_SUCCESS: 'UPDATE_ACCOUNT_PASSWORD_SUCCESS',
  UPDATE_ACCOUNT_PASSWORD_FAIL: 'UPDATE_ACCOUNT_PASSWORD_FAIL',

  CHECK_OTP: 'CHECK_OTP',
  CHECK_OTP_SUCCESS: 'CHECK_OTP_SUCCESS',
  CHECK_OTP_FAIL: 'CHECK_OTP_FAIL',

  GET_INIT_CONFIG: 'GET_INIT_CONFIG',
  GET_INIT_CONFIG_SUCCESS: 'GET_INIT_CONFIG_SUCCESS',
  GET_INIT_CONFIG_FAIL: 'GET_INIT_CONFIG_FAIL',
  GET_INFOR_OPTION: 'GET_INFOR_OPTION',
  GET_INFOR_OPTION_SUCCESS: 'GET_INFOR_OPTION_SUCCESS',
  AUTHORIZE_USER: 'AUTHORIZE_USER',
  AUTHORIZE_USER_SUCCESS: 'AUTHORIZE_USER_SUCCESS',
  AUTHORIZE_USER_FAIL: 'AUTHORIZE_USER_FAIL',

  AUTHORIZE_FINGERPRINT: 'AUTHORIZE_FINGERPRINT',

  CHECK_TOKEN_VALIDATE: 'CHECK_TOKEN_VALIDATE',
  CHECK_TOKEN_INVALIDATE: 'CHECK_TOKEN_INVALIDATE',
  CHECK_TOKEN: 'CHECK_TOKEN',

  CHECK_ACCOUNT_EXIST: 'CHECK_ACCOUNT_EXIST',
  CHECK_ACCOUNT_EXIST_SUCCESS: 'CHECK_ACCOUNT_EXIST_SUCCESS',
  CHECK_ACCOUNT_EXIST_FAIL: 'CHECK_ACCOUNT_EXIST_FAIL',
  CHECK_PHONE_NUMBER: 'CHECK_PHONE_NUMBER',
  CHECK_PHONE_NUMBER_SUCCESS: 'CHECK_PHONE_NUMBER_SUCCESS',
  CHECK_PHONE_NUMBER_FAIL: 'CHECK_PHONE_NUMBER_FAIL',

  CHECK_CMND_NUMBER: 'CHECK_CMND_NUMBER',
  CHECK_CMND_NUMBER_SUCCESS: 'CHECK_CMND_NUMBER_SUCCESS',
  CHECK_CMND_NUMBER_FAIL: 'CHECK_CMND_NUMBER_FAIL',

  GET_LIST_NEWS_BY_CATEGORY: 'GET_LIST_NEWS_BY_CATEGORY',
  LOADING_LIST_NEW: 'LOADING_LIST_NEW',
  GET_LIST_NEWS_BY_CATEGORY_SUCCESS: 'GET_LIST_NEWS_BY_CATEGORY_SUCCESS',
  GET_LIST_NEWS_BY_CATEGORY_FAIL: 'GET_LIST_NEWS_BY_CATEGORY_FAIL',

  VERIFY_INFO_REGISTER: 'VERIFY_INFO_REGISTER',
  VERIFY_INFO_REGISTER_SUCCESS: 'VERIFY_INFO_REGISTER_SUCCESS',
  VERIFY_INFO_REGISTER_FAIL: 'VERIFY_INFO_REGISTER_FAIL',

  GET_LIST_ADDRESS: 'GET_LIST_ADDRESS',
  GET_LIST_ADDRESS_SUCCESS: 'GET_LIST_ADDRESS_SUCCESS',
  GET_LIST_ADDRESS_FAIL: 'GET_LIST_ADDRESS_FAIL',

  CLEAR_LIST_NEWS: 'CLEAR_LIST_NEWS',

  GET_NEWS_DETAIL: 'GET_NEWS_DETAIL',
  LOADING_NEWS_DETAIL: 'LOADING_NEWS_DETAIL',
  GET_NEWS_DETAIL_SUCCESS: 'GET_NEWS_DETAIL_SUCCESS',
  GET_NEWS_DETAIL_FAIL: 'GET_NEWS_DETAIL_FAIL',
  CLEAR_NEWS_DETAIL: 'CLEAR_NEWS_DETAIL',

  GET_LIST_CASH_DEPOSIT: 'GET_LIST_CASH_DEPOSIT',
  GET_LIST_CASH_DEPOSIT_SUCCESS: 'GET_LIST_CASH_DEPOSIT_SUCCESS',
  GET_LIST_CASH_DEPOSIT_FAIL: 'GET_LIST_CASH_DEPOSIT_FAIL',
  GET_BANK_LIST: 'GET_BANK_LIST',
  GET_BANK_LIST_SUCCESS: 'GET_BANK_LIST_SUCCESS',
  GET_BANK_LIST_FAIL: 'GET_BANK_LIST_FAIL'
}

export default DailyGateTypes

// import mockupReducer from './mockup/mockup.reducers'
import gateReducer from './daily-gate/daily-gate.reducers'
import apiReducer from './api/api.reducers'

export {
  // mockupReducer,
  gateReducer,
  apiReducer
}

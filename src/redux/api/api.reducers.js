import ApiTypes from './api.types'

const initialState = {
  type: null,
  data: {}
}

export default function apiReducer(state = initialState, action) {
  switch (action.type) {
    case ApiTypes.STORE_API_MESSAGE: {
      const {
        message, code
      } = action.payload
      return {
        message,
        code,
      }
    }
    case ApiTypes.STORE_API_PAYLOAD: {
      const { type, payload } = action.payload
      return {
        type,
        data: { ...payload }
      }
    }
    case ApiTypes.CLEAR_API_RESPONSE:
      return {}
    default:
      return state
  }
}

import ApiTypes from './api.types'

const storeResponseMessage = (response) => {
  return {
    type: ApiTypes.STORE_API_MESSAGE,
    payload: { ...response }
  }
}

const storeResponsePayload = (response) => {
  return {
    type: ApiTypes.STORE_API_PAYLOAD,
    payload: { ...response }
  }
}

const clearResponse = () => {
  return {
    type: ApiTypes.CLEAR_API_RESPONSE
  }
}

const apiActions = {
  storeResponseMessage,
  storeResponsePayload,
  clearResponse
}

export default apiActions

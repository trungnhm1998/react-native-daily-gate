import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import I18n from 'redux-i18n'
import { PersistGate } from 'redux-persist/integration/react'
import translations from '../locales'
import { AppStoreProvider, AppPersistStore } from './store.config'

let store

class StoreProvider extends PureComponent {
  getChildContext() {
    return {
      store,
    }
  }

  static childContextTypes = {
    store: PropTypes.shape({}),
  };

  render() {
    const { children } = this.props

    store = store || AppStoreProvider

    return (
      <Provider store={store}>
        <I18n translations={translations} initialLang="vi" fallbackLang="vi">
          <PersistGate persistor={AppPersistStore}>
            {children}
          </PersistGate>
        </I18n>
      </Provider>
    )
  }
}

export default StoreProvider

import { combineReducers } from 'redux'
import { i18nState } from 'redux-i18n'
import {
  // mockupReducer,
  gateReducer,
  apiReducer
} from '../redux'


const rootReducer = combineReducers({
  // mockupReducer,
  gateReducer,
  apiReducer,
  i18nState
})

export default rootReducer

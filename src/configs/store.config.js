import { createStore, applyMiddleware } from 'redux'
import { AsyncStorage } from 'react-native'
import createSagaMiddleware from 'redux-saga'
import { persistReducer, persistStore } from 'redux-persist'
import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'remote-redux-devtools'
import rootReducers from './reducers.config'
import rootSaga from '../sagas'

/* eslint-disable no-underscore-dangle */
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const composeEnhancers = composeWithDevTools({ realtime: true, port: 8000 })
/* eslint-enable */
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['gateReducer']
}

const configureStore = (initialState) => {
  const sagaMiddleware = createSagaMiddleware()
  const persistRootReducers = persistReducer(persistConfig, rootReducers)
  if (__DEV__) {
    const store = createStore(
      persistRootReducers,
      initialState,
      composeEnhancers(
        applyMiddleware(
          sagaMiddleware,
          createLogger(),
        ),
      ),
    )
    sagaMiddleware.run(rootSaga)
    return store
  }
  const store = createStore(
    persistRootReducers,
    initialState,
    composeEnhancers(
      applyMiddleware(
        sagaMiddleware,
      ),
    ),
  )
  sagaMiddleware.run(rootSaga)
  return store
}

const AppStoreProvider = configureStore()
const AppPersistStore = persistStore(AppStoreProvider)

// export default AppStoreProvider
export { AppStoreProvider, AppPersistStore }

import InputFlagLogin from './input/InputFlagLogin'
import InputTextNormal from './input/InputTextNormal'
import InputRightIcon from './input/InputRightIcon'
import InputLeftIcon from './input/InputLeftIcon'
import InputModalNormal from './input/InputModalNormal'
import InputTextFormatMoney from './input/InputTextFormatMoney'
import InputModalRightIcon from './input/InputModalRightIcon'
import InputModalRightIconTicket from './input/InputModalRightIconTicket'
import InputModalLeftIcon from './input/InputModalLeftIcon'
import InputFLoatingLabel_tcomb from './input/input-floating-label'
import styles from './tcomb.styles'

export default {
  styles,
  InputFLoatingLabel_tcomb,
  InputFlagLogin,
  InputTextNormal,
  InputTextFormatMoney,
  InputRightIcon,
  InputLeftIcon,
  InputModalNormal,
  InputModalRightIcon,
  InputModalRightIconTicket,
  InputModalLeftIcon
}

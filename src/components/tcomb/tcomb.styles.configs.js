/*

 a bootstrap like style

 */
'use strict';

import { Platform, Dimensions, PixelRatio } from 'react-native';
const { width,height } = Dimensions.get('window');
const LABEL_COLOR = '#B0B0B0';
const INPUT_COLOR = '#333';
const ERROR_COLOR = '#c00';
const HELP_COLOR = '#999999';
const BORDER_COLOR = '#e7e7e7';
const DISABLED_COLOR = '#777777';
const DISABLED_BACKGROUND_COLOR = '#eeeeee';
const FONT_SIZE = 16;
const FONT_SIZE_LABEL = 12;
const FONT_WEIGHT = '500';
const MARGIN_LEFT = 15;
const INPUT_HEIGHT = 50;
const INPUT_PADDING = 0;
const INPUT_PADDING_TOP = 10;
const BORDER_WIDTH = 0.5 * PixelRatio.get();
// const INPUT_HEIGHT = height * 0.08;
// const INPUT_PADDING = height * 0.02;
// const INPUT_PADDING_TOP = height * 0.018;


var stylesheet = Object.freeze({
  fieldset: {},
  // the style applied to the container of all inputs
  formGroup: {
    normal: {
      marginBottom: 10
    },
    error: {
      marginBottom: 10
    }
  },
  controlLabel: {
    normal: {
      color: LABEL_COLOR,
      fontSize: FONT_SIZE_LABEL,
      marginBottom: 7,
      fontWeight: FONT_WEIGHT,
      marginLeft: MARGIN_LEFT,
    },
    // the style applied when a validation error occours
    error: {
      color: ERROR_COLOR,
      fontSize: FONT_SIZE_LABEL,
      marginBottom: 7,
      fontWeight: FONT_WEIGHT,
      marginLeft: MARGIN_LEFT,
    }
  },
  helpBlock: {
    normal: {
      color: HELP_COLOR,
      fontSize: FONT_SIZE,
      marginBottom: 2
    },
    // the style applied when a validation error occours
    error: {
      color: HELP_COLOR,
      fontSize: FONT_SIZE,
      marginBottom: 2
    }
  },
  errorBlock: {
    fontSize: FONT_SIZE,
    marginBottom: 2,
    color: ERROR_COLOR
  },
  textboxView: {
    normal: {
      borderTopWidth: BORDER_WIDTH,
      borderColor: BORDER_COLOR,
      borderBottomWidth: BORDER_WIDTH,
      marginBottom: 5,
    },
    error: {
      borderColor: ERROR_COLOR,
      borderTopWidth: BORDER_WIDTH,
      borderBottomWidth: BORDER_WIDTH,
      marginBottom: 5,
    },
    notEditable: {
    }
  },
  textbox: {
    normal: {
      color: INPUT_COLOR,
      fontSize: FONT_SIZE,
      height: INPUT_HEIGHT,
      padding: INPUT_PADDING,
      borderRadius: 0,
      backgroundColor :'#fff',
      paddingLeft: 15,
      paddingTop: INPUT_PADDING_TOP,
    },
    // the style applied when a validation error occours
    error: {
      color: INPUT_COLOR,
      fontSize: FONT_SIZE,
      height: INPUT_HEIGHT,
      padding: INPUT_PADDING,
      borderRadius: 0,
      backgroundColor :'#fff',
      paddingLeft: 15,
    },
    // the style applied when the textbox is not editable
    notEditable: {
      fontSize: FONT_SIZE,
      height: INPUT_HEIGHT,
      padding: 7,
      borderRadius: 4,
      borderColor: BORDER_COLOR,
      borderWidth: 1,
      color: DISABLED_COLOR,
      backgroundColor: DISABLED_BACKGROUND_COLOR
    }
  },
  checkbox: {
    normal: {
      marginBottom: 4
    },
    // the style applied when a validation error occours
    error: {
      marginBottom: 4
    }
  },
  pickerContainer: {
    normal: {
      marginBottom: 4,
      borderRadius: 4,
      borderColor: BORDER_COLOR,
      borderWidth: 1
    },
    error: {
      borderColor: ERROR_COLOR
    },
    open: {
      // Alter styles when select container is open
    }
  },
  select: {
    normal: Platform.select({
      android: {
        paddingLeft: 7,
        color: INPUT_COLOR
      },
      ios: {

      }
    }),
    // the style applied when a validation error occours
    error: Platform.select({
      android: {
        paddingLeft: 7,
        color: ERROR_COLOR
      },
      ios: {

      }
    })
  },
  pickerTouchable: {
    normal: {
      height: 44,
      flexDirection: 'row',
      alignItems: 'center'
    },
    error: {
      height: 44,
      flexDirection: 'row',
      alignItems: 'center'
    },
    active: {
      borderBottomWidth: 1,
      borderColor: BORDER_COLOR
    }
  },
  pickerValue: {
    normal: {
      fontSize: FONT_SIZE,
      paddingLeft: 7
    },
    error: {
      fontSize: FONT_SIZE,
      paddingLeft: 7
    }
  },
  datepicker: {
    normal: {
      marginBottom: 4
    },
    // the style applied when a validation error occours
    error: {
      marginBottom: 4
    }
  },
  dateTouchable: {
    normal: {},
    error: {}
  },
  dateValue: {
    normal: {
      color: INPUT_COLOR,
      fontSize: FONT_SIZE,
      padding: 7,
      marginBottom: 5
    },
    error: {
      color: ERROR_COLOR,
      fontSize: FONT_SIZE,
      padding: 7,
      marginBottom: 5
    }
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});

module.exports = stylesheet;
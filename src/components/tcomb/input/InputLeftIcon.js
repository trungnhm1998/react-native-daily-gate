var React = require('react')
var { View, Text, TextInput, Image, StyleSheet, PixelRatio } = require('react-native')
import { Themes, Icon, TextFont} from '../../../ui'

const styles = StyleSheet.create({
  inputStyle : {
    flex: 1,
  }
})

function InputLeftIcon(locals) {
  if (locals.hidden) {
    return null;
  }

  var stylesheet = locals.stylesheet;
  var formGroupStyle = stylesheet.formGroup.normal;
  var controlLabelStyle = stylesheet.controlLabel.normal;
  var textboxStyle = stylesheet.textbox.normal;
  var textboxViewStyle = stylesheet.textboxView.normal;
  var helpBlockStyle = stylesheet.helpBlock.normal;
  var errorBlockStyle = stylesheet.errorBlock;

  if (locals.hasError) {
    formGroupStyle = stylesheet.formGroup.error;
    controlLabelStyle = stylesheet.controlLabel.error;
    textboxStyle = stylesheet.textbox.error;
    textboxViewStyle = stylesheet.textboxView.error;
    helpBlockStyle = stylesheet.helpBlock.error;
  }

  if (locals.editable === false) {
     textboxStyle = stylesheet.textbox.normal;
     textboxViewStyle = stylesheet.textboxView.normal;
  }

  var label = locals.config.label ? <Text style={[controlLabelStyle,{color:'#000'},locals.config.labelStyle]}>{locals.config.label}</Text> : null;
  var help = locals.help ? <Text style={helpBlockStyle}>{locals.help}</Text> : null;
  var error = locals.hasError && locals.error ? <Text accessibilityLiveRegion="polite" style={errorBlockStyle}>{locals.error}</Text> : null;

  return (
    <View style={[formGroupStyle, { marginBottom: 0 }]}>
      {label}
      <View style={[textboxViewStyle, { flexDirection: 'row', paddingVertical: 5, backgroundColor: '#fff' }, locals.config.inputStyle]}>
        <View style={{ width: 40, justifyContent: 'center', borderRadius: 20 }}>
          <Icon
            name={locals.config.leftIcon}
            size={locals.config.leftIconSize}
            color={locals.config.leftIconColor}
            style={{alignSelf: 'center', paddingLeft: 5}}
          />
        </View>
        <TextInput
          accessibilityLabel={locals.label}
          ref="input"
          autoCapitalize={locals.autoCapitalize}
          autoCorrect={false}
          autoFocus={locals.autoFocus}
          blurOnSubmit={locals.blurOnSubmit}
          editable={locals.editable}
          keyboardType={locals.keyboardType}
          maxLength={locals.maxLength}
          multiline={locals.multiline}
          onBlur={locals.onBlur}
          onEndEditing={locals.onEndEditing}
          onFocus={locals.onFocus}
          onLayout={locals.onLayout}
          onSelectionChange={locals.onSelectionChange}
          onSubmitEditing={locals.onSubmitEditing}
          onContentSizeChange={locals.onContentSizeChange}
          placeholderTextColor={locals.placeholderTextColor}
          secureTextEntry={locals.secureTextEntry}
          selectTextOnFocus={locals.selectTextOnFocus}
          selectionColor={locals.selectionColor}
          numberOfLines={locals.numberOfLines}
          underlineColorAndroid={locals.underlineColorAndroid}
          clearButtonMode={locals.clearButtonMode}
          clearTextOnFocus={locals.clearTextOnFocus}
          enablesReturnKeyAutomatically={locals.enablesReturnKeyAutomatically}
          keyboardAppearance={locals.keyboardAppearance}
          onKeyPress={locals.onKeyPress}
          returnKeyType={locals.returnKeyType}
          selectionState={locals.selectionState}
          onChangeText={(value) => locals.onChange(value)}
          onChange={locals.onChangeNative}
          placeholder={locals.placeholder}
          style={[textboxStyle, styles.inputStyle, { borderRadius: 24, paddingLeft: 2 }]}
          value={locals.config.value}
        />
      </View>
      {help}
      {error}
    </View>
  );
}


InputLeftIcon.defaultProps = {
  //rightIconColor: 'red'
}


module.exports = InputLeftIcon;
import { Themes, Icon, TextFont } from '../../../ui'
const React = require('react')
const {
  View, Text, TextInput, Image, StyleSheet, PixelRatio, StatusBar, Animated, TouchableOpacity 
} = require('react-native')

const styles = StyleSheet.create({
  inputStyle: {
    flex: 1
  }
})

class FloatingLabelInput extends React.Component {
  state = {
    isFocused: false
  }

  componentWillMount() {
    this._animatedIsFocused = new Animated.Value(this.props.value === '' ? 0 : 1)
  }

  handleFocus = () => {
    this.input.focus()
    this.setState({ isFocused: true })
  }

  handleBlur = () => this.setState({ isFocused: false })

  componentDidUpdate() {
    Animated.timing(this._animatedIsFocused, {
      toValue: this.state.isFocused || this.props.value !== '' ? 1 : 0,
      duration: 200
    }).start()
  }

  // only render the mask if input is read only
  renderLabelMask(editable) {
    const labelMaskStyle = {
      backgroundColor: '#fff',
      position: 'absolute',
      left: 10,
      paddingLeft: 5,
      paddingRight: 5,
      top: -8,
      width: 80,
      height: 10
    }

    if (editable === false) {
      return (
        <View style={labelMaskStyle}>
        </View>
      )
    }
    return null
  }

  render() {
    const locals = this.props

    const rightIconStyle = {
      position: 'absolute',
      right: 15,
      top: 20
    }
    const textInputStyle = {
      height: 50,
      fontSize: 20,
      color: '#000',
      paddingLeft: 10,
      borderWidth: 1,
      borderRadius: 8,
      borderColor: locals.hasError ? '#E02729' : '#D1D1D1',
      backgroundColor: locals.editable === false ? '#F4F5FA' : '#fff'
    }
    // If there is right icon
    // const rightIcon = locals.config.iconRightSrc ? (
    //   <TouchableOpacity style={rightIconStyle} onPress={locals.config.onPressIconRight}>
    //     <Image source={locals.config.iconRightSrc} />
    //   </TouchableOpacity>
    // ) : null

    // truyền rightAddon vào, rightAddon nhận vào 1 hàm hoặc 1 component
    const rightAddon = locals.config.rightAddon ? (
      <TouchableOpacity style={rightIconStyle} onPress={locals.config.onPressTextRight}>
        {locals.config.rightAddon}
      </TouchableOpacity>
    ) : null

    const animatedStyle = () => {
      if (locals.editable === false) {
        return {
          top: -5,
          fontSize: 12,
          color: '#9B9B9B'
        }
      }
      return {
        top: this._animatedIsFocused.interpolate({
          inputRange: [0, 1],
          outputRange: [15, -5]
        }),
        fontSize: this._animatedIsFocused.interpolate({
          inputRange: [0, 1],
          outputRange: [16, 12]
        }),
        color: this._animatedIsFocused.interpolate({
          inputRange: [0, 1],
          outputRange: ['#aaa', '#9B9B9B']
        })
      }
    }

    // console.log('icon', locals.config.iconRightSrc)
    const labelStyle = {
      backgroundColor: locals.editable === false ? '#ffffff00' : '#fff',
      fontFamily: 'lato-light',
      position: 'absolute',
      left: 10,
      paddingLeft: 5,
      paddingRight: 5,
      ...animatedStyle()
    }

    return (
      <View style={{ flex: 1 }}>
        <TextInput
          accessibilityLabel={locals.label}
          ref={(input) => { this.input = input }}
          autoCapitalize={locals.autoCapitalize}
          autoCorrect={false}
          autoFocus={locals.autoFocus}
          blurOnSubmit={locals.blurOnSubmit}
          editable={locals.editable}
          keyboardType={locals.keyboardType}
          maxLength={locals.maxLength}
          multiline={locals.multiline}
          onEndEditing={locals.onEndEditing}
          onLayout={locals.onLayout}
          onSelectionChange={locals.onSelectionChange}
          onSubmitEditing={locals.onSubmitEditing}
          onContentSizeChange={locals.onContentSizeChange}
          placeholderTextColor={locals.placeholderTextColor}
          secureTextEntry={locals.secureTextEntry}
          selectTextOnFocus={locals.selectTextOnFocus}
          selectionColor={locals.selectionColor}
          numberOfLines={locals.numberOfLines}
          underlineColorAndroid={locals.underlineColorAndroid}
          clearButtonMode={locals.clearButtonMode}
          clearTextOnFocus={locals.clearTextOnFocus}
          enablesReturnKeyAutomatically={locals.enablesReturnKeyAutomatically}
          keyboardAppearance={locals.keyboardAppearance}
          onKeyPress={locals.onKeyPress}
          returnKeyType={locals.returnKeyType}
          selectionState={locals.selectionState}
          onChangeText={value => locals.onChange(value)}
          onChange={locals.onChangeNative}
          value={locals.value}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          style={textInputStyle}
        />
        {this.renderLabelMask(locals.editable)}
        <Animated.Text onPress={this.handleFocus} style={labelStyle}>
          {locals.label}
        </Animated.Text>
        {rightAddon}
      </View>
    )
  }
}

function InputFLoatingLabel_tcomb(locals) {
  if (locals.hidden) {
    return null
  }

  const stylesheet = locals.stylesheet
  let formGroupStyle = stylesheet.formGroup.normal
  let controlLabelStyle = stylesheet.controlLabel.normal
  let textboxStyle = stylesheet.textbox.normal
  let textboxViewStyle = stylesheet.textboxView.normal
  let helpBlockStyle = stylesheet.helpBlock.normal
  const errorBlockStyle = stylesheet.errorBlock

  if (locals.hasError) {
    formGroupStyle = stylesheet.formGroup.error
    controlLabelStyle = stylesheet.controlLabel.error
    textboxStyle = stylesheet.textbox.error
    textboxViewStyle = stylesheet.textboxView.error
    helpBlockStyle = stylesheet.helpBlock.error
  }

  if (locals.editable === false) {
    textboxStyle = stylesheet.textbox.notEditable
    textboxViewStyle = stylesheet.textboxView.notEditable
  }

  // TODO: Update text color to be more accurate
  const tips = locals.config.tips
    ? <TextFont color={Themes.Colors.greenText} size={14} style={{ textAlign: 'left' }}>{locals.config.tips}</TextFont>
    : null

  const help = locals.help ? <Text style={helpBlockStyle}>{locals.help}</Text> : null
  const error = locals.hasError && locals.error ? (
    <TextFont accessibilityLiveRegion="polite" style={errorBlockStyle}>
      {locals.error}
    </TextFont>
  ) : null
  const containerStyle = locals.config.containerStyle ? locals.config.containerStyle : {}

  return (
    <View style={formGroupStyle}>
      <View
        style={[
          textboxViewStyle,
          {
            flexDirection: 'row',
            paddingVertical: 1,
            backgroundColor: '#fff',
            borderWidth: 0
          },
          containerStyle
        ]}
      >
        {/*
          <View
            style={{ backgroundColor: '#fff', justifyContent:'center', alignItems:'center', width: 60, borderRightWidth: 1 / PixelRatio.get(), borderColor:'#000'}}>
            <Image source={Themes.Images.vnFlag} style={{ width: 30, height: 19 }}></Image>
            <Text style={{marginTop: 3, fontSize: 11}}>(+84)</Text>
          </View>
          */}
        <FloatingLabelInput {...locals} />
      </View>
      {tips}
      {help}
      {error}
    </View>
  )
}

module.exports = InputFLoatingLabel_tcomb

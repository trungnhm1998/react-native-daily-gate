import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native'
import { Icon } from '../../../ui'
import _ from 'lodash'
import numeral from 'numeral'
import t from 'tcomb-form-native'

const Textbox = t.form.Textbox
class InputModalRightIcon extends Textbox {
  constructor(props) {
    super(props)

    const initState = this.props.initState || false

    if(initState) {
      this.state = {
        value: (props.value) ? String(props.value) : undefined
      }
    }
  }

  getTemplate = () => {
    let self = this
    return (locals) => {
      if (locals.hidden) {
        return null
      }
      const stylesheet = locals.stylesheet
      let formGroupStyle = stylesheet.formGroup.normal
      let controlLabelStyle = stylesheet.controlLabel.normal
      let textboxStyle = stylesheet.modal.normal
      let helpBlockStyle = stylesheet.helpBlock.normal
      let errorBlockStyle = stylesheet.errorBlock
      if (locals.hasError) {
        controlLabelStyle = stylesheet.controlLabel.error
        formGroupStyle = stylesheet.formGroup.error
        textboxStyle = stylesheet.modal.error
        helpBlockStyle = stylesheet.helpBlock.error
      }
      if (locals.editable === false) {
        textboxStyle = stylesheet.textbox.notEditable
      }
      const help = locals.help ? <Text style={helpBlockStyle}>{locals.help}</Text> : null
      const error = locals.hasError && locals.error ? <Text style={errorBlockStyle}>{locals.error}</Text> : null
      // var label = locals.config.label ? <Text style={controlLabelStyle}>{locals.config.label}</Text> : null
      var label = locals.config.label ? <Text style={[controlLabelStyle,{color:'#000'},locals.config.labelStyle]}>{locals.config.label}</Text> : null;

      let valueModal = ''
      if(locals.config.formatMoney === true){
        valueModal = (_.isNil(locals.value)) || (locals.value === '') ? <Text style={{ color: locals.placeholderTextColor }}>{locals.placeholder}</Text> : <Text>{numeral(locals.value).format('0,0')} {locals.config.currency}</Text>
      }else{
        valueModal = (_.isNil(locals.value)) || (locals.value === '') ? <Text style={{ color: locals.placeholderTextColor }}>{locals.placeholder}</Text> : <Text>{locals.value}</Text>
      }


      return (
        <View style={[formGroupStyle, { marginBottom:0 }]}>
          {label}
          <TouchableOpacity activeOpacity={0.8} style={[textboxStyle, { height: 47, justifyContent: 'center', flexDirection:'row', alignItems:'center' }]} onPress={locals.onFocus}>
            <Text style={{flex: 1}} ref="input">{valueModal}</Text>
            <View style={{width: 35, height: 35, justifyContent: 'center', alignItems:'center'}}>
              <Icon
                name={locals.config.nameIcon}
                size={locals.config.IconSize}
                color={locals.config.IconColor}
              />
            </View>
          </TouchableOpacity>
          {help}
          {error}
        </View>
      )
    }
  }

}

const styles = StyleSheet.create({
  textInput: {
    height: 40
  }
})

export default InputModalRightIcon

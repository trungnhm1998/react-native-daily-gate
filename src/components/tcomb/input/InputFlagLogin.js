var React = require('react')
var { View, Text, TextInput, Image, StyleSheet, PixelRatio } = require('react-native')
import { Themes, Icon, TextFont} from '../../../ui'

const styles = StyleSheet.create({
  inputStyle : {
    flex: 1,
  }
})

function InputFlagLogin(locals) {
  if (locals.hidden) {
    return null;
  }

  let stylesheet = locals.stylesheet;
  let formGroupStyle = stylesheet.formGroup.normal;
  let controlLabelStyle = stylesheet.controlLabel.normal;
  let textboxStyle = stylesheet.textbox.normal;
  let textboxViewStyle = stylesheet.textboxView.normal;
  let helpBlockStyle = stylesheet.helpBlock.normal;
  let errorBlockStyle = stylesheet.errorBlock;

  if (locals.hasError) {
    formGroupStyle = stylesheet.formGroup.error;
    controlLabelStyle = stylesheet.controlLabel.error;
    textboxStyle = stylesheet.textbox.error;
    textboxViewStyle = stylesheet.textboxView.error;
    helpBlockStyle = stylesheet.helpBlock.error;
  }

  if (locals.editable === false) {
    textboxStyle = stylesheet.textbox.notEditable;
    textboxViewStyle = stylesheet.textboxView.notEditable;
  }

  const labelStyle = locals.config.labelStyle ? locals.config.labelStyle : {}
  let label = locals.config.label
    ? <Text style={{...controlLabelStyle, ...{ textAlign: 'center', color: '#000', marginVertical: 5 }, ...labelStyle}}>{locals.config.label}</Text>
    : null
  let help = locals.help ? <Text style={helpBlockStyle}>{locals.help}</Text> : null;
  let error = locals.hasError && locals.error ? <Text accessibilityLiveRegion="polite" style={errorBlockStyle}>{locals.error}</Text> : null;
  const containerStyle = locals.config.containerStyle ? locals.config.containerStyle : {}

  return (
    <View style={formGroupStyle}>
      {label}
      <View style={[textboxViewStyle, { flexDirection: 'row', paddingVertical: 5, backgroundColor: '#fff' }, containerStyle]}>
        {
          /*
          <View
            style={{ backgroundColor: '#fff', justifyContent:'center', alignItems:'center', width: 60, borderRightWidth: 1 / PixelRatio.get(), borderColor:'#000'}}>
            <Image source={Themes.Images.vnFlag} style={{ width: 30, height: 19 }}></Image>
            <Text style={{marginTop: 3, fontSize: 11}}>(+84)</Text>
          </View>
          */
        }
        <TextInput
          accessibilityLabel={locals.label}
          ref="input"
          autoCapitalize={locals.autoCapitalize}
          autoCorrect={false}
          autoFocus={locals.autoFocus}
          blurOnSubmit={locals.blurOnSubmit}
          editable={locals.editable}
          keyboardType={locals.keyboardType}
          maxLength={locals.maxLength}
          multiline={locals.multiline}
          onBlur={locals.onBlur}
          onEndEditing={locals.onEndEditing}
          onFocus={locals.onFocus}
          onLayout={locals.onLayout}
          onSelectionChange={locals.onSelectionChange}
          onSubmitEditing={locals.onSubmitEditing}
          onContentSizeChange={locals.onContentSizeChange}
          placeholderTextColor={locals.placeholderTextColor}
          secureTextEntry={locals.secureTextEntry}
          selectTextOnFocus={locals.selectTextOnFocus}
          selectionColor={locals.selectionColor}
          numberOfLines={locals.numberOfLines}
          underlineColorAndroid={locals.underlineColorAndroid}
          clearButtonMode={locals.clearButtonMode}
          clearTextOnFocus={locals.clearTextOnFocus}
          enablesReturnKeyAutomatically={locals.enablesReturnKeyAutomatically}
          keyboardAppearance={locals.keyboardAppearance}
          onKeyPress={locals.onKeyPress}
          returnKeyType={locals.returnKeyType}
          selectionState={locals.selectionState}
          onChangeText={(value) => locals.onChange(value)}
          onChange={locals.onChangeNative}
          placeholder={locals.placeholder}
          style={[textboxStyle, styles.inputStyle]}
          value={locals.value}
        />
      </View>
      {help}
      {error}
    </View>
  );
}

module.exports = InputFlagLogin;

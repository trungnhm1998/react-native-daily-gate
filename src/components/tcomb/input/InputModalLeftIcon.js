import React from 'react'
import _ from 'lodash'
var { View, Text, TextInput, Image, TouchableOpacity, StyleSheet, PixelRatio } = require('react-native')
import { Themes, Icon, TextFont } from '../../../ui'

const styles = StyleSheet.create({
  inputStyle: {
    flex: 1
  }
})

function InputModalLeftIcon(locals) {
  if (locals.hidden) {
    return null
  }

  var stylesheet = locals.stylesheet
  var formGroupStyle = stylesheet.formGroup.normal
  var controlLabelStyle = stylesheet.controlLabel.normal
  var textboxStyle = stylesheet.modal.normal
  var textboxViewStyle = stylesheet.textboxView.normal
  var helpBlockStyle = stylesheet.helpBlock.normal
  var errorBlockStyle = stylesheet.errorBlock

  if (locals.hasError) {
    formGroupStyle = stylesheet.formGroup.error
    controlLabelStyle = stylesheet.controlLabel.error
    textboxStyle = stylesheet.modal.error
    textboxViewStyle = stylesheet.textboxView.error
    helpBlockStyle = stylesheet.helpBlock.error
  }

  if (locals.editable === false) {
    textboxStyle = stylesheet.textbox.normal
    textboxViewStyle = stylesheet.textboxView.normal
  }

  const label = locals.config.label ? <Text style={[controlLabelStyle, { color: '#000' }, locals.config.labelStyle]}>{locals.config.label}</Text> : null
  const help = locals.help ? <Text style={helpBlockStyle}>{locals.help}</Text> : null
  const error = locals.hasError && locals.error ? <Text accessibilityLiveRegion="polite" style={errorBlockStyle}>{locals.error}</Text> : null
  const valueModal = _.isNil(locals.config.value) ? <Text style={{color:locals.placeholderTextColor}}>{locals.placeholder}</Text> : <Text>{locals.config.value}</Text>

  return (
    <View style={[formGroupStyle, { marginBottom: 0 }]}>
      {label}
      <TouchableOpacity
        ref="input"
        activeOpacity={0.8}
        style={[textboxStyle, { height: 47, flexDirection: 'row', alignItems: 'center', paddingLeft: 0, borderRadius: 24 }]}
        onPress={locals.onFocus}
      >
        <View style={{ width: 35, height: 35, justifyContent: 'center', alignItems: 'center', borderRadius: 17 }}>
          <Icon
            name={locals.config.nameIcon}
            size={locals.config.IconSize}
            color={locals.config.IconColor}
          />
        </View>
        <Text>{valueModal}</Text>
      </TouchableOpacity>
      {help}
      {error}
    </View>
  )
}


InputModalLeftIcon.defaultProps = {
  //rightIconColor: 'red'
}
//
//
// <View style={[textboxViewStyle, { flexDirection: 'row', paddingVertical: 5, backgroundColor: '#fff' }, locals.config.inputStyle]}>
//   <View style={{ width: 40, justifyContent: 'center', borderRadius: 20 }}>
//     <Icon
//       name={locals.config.leftIcon}
//       size={locals.config.leftIconSize}
//       color={locals.config.leftIconColor}
//       style={{ alignSelf: 'center', paddingLeft: 5 }}
//     />
//   </View>
//
// </View>

//
// <TextInput
//   accessibilityLabel={locals.label}
//   ref="input"
//   autoCapitalize={locals.autoCapitalize}
//   autoCorrect={false}
//   autoFocus={locals.autoFocus}
//   blurOnSubmit={locals.blurOnSubmit}
//   editable={locals.editable}
//   keyboardType={locals.keyboardType}
//   maxLength={locals.maxLength}
//   multiline={locals.multiline}
//   onBlur={locals.onBlur}
//   onEndEditing={locals.onEndEditing}
//   onFocus={locals.onFocus}
//   onLayout={locals.onLayout}
//   onSelectionChange={locals.onSelectionChange}
//   onSubmitEditing={locals.onSubmitEditing}
//   onContentSizeChange={locals.onContentSizeChange}
//   placeholderTextColor={locals.placeholderTextColor}
//   secureTextEntry={locals.secureTextEntry}
//   selectTextOnFocus={locals.selectTextOnFocus}
//   selectionColor={locals.selectionColor}
//   numberOfLines={locals.numberOfLines}
//   underlineColorAndroid={locals.underlineColorAndroid}
//   clearButtonMode={locals.clearButtonMode}
//   clearTextOnFocus={locals.clearTextOnFocus}
//   enablesReturnKeyAutomatically={locals.enablesReturnKeyAutomatically}
//   keyboardAppearance={locals.keyboardAppearance}
//   onKeyPress={locals.onKeyPress}
//   returnKeyType={locals.returnKeyType}
//   selectionState={locals.selectionState}
//   onChangeText={(value) => locals.onChange(value)}
//   onChange={locals.onChangeNative}
//   placeholder={locals.placeholder}
//   style={[textboxStyle, styles.inputStyle, { borderRadius: 24, paddingLeft: 2 }]}
//   value={locals.config.value}
// />


module.exports = InputModalLeftIcon
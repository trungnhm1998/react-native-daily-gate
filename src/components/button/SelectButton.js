import React from 'react'
import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import PropTypes from 'prop-types'

const deviceHeight = Dimensions.get('window').height
const styles = StyleSheet.create({
  normalLoginBt: {
    backgroundColor: 'red',
    height: deviceHeight * 0.09,
    borderRadius: 8,
    marginBottom: 15,
    paddingBottom: 2
  },
  innerButton: {
    backgroundColor: 'blue',
    borderRadius: 8,
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row'
  },
  textButton: {
    flex: 1,
    justifyContent: 'center'
  },
  textLabelLogin: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '400'
  },
  buttonGroupItem: {
    width: Dimensions.get('window').width * 0.1,
    height: Dimensions.get('window').width * 0.1,
    marginLeft: Dimensions.get('window').width * 0.01,
    marginRight: Dimensions.get('window').width * 0.01
  },
  buttonGroupItemTouch: {
    width: Dimensions.get('window').width * 0.1,
    height: Dimensions.get('window').width * 0.1,
    borderRadius: 3,
    borderColor: '#c92033',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonGroupItemText: {
    color: '#c92033',
    fontSize: 16
  }
})
const SelectButton = (props) => {
  return (
    <View class="group-bt" style={styles.buttonGroupItem}>
      <TouchableOpacity
        onPress={props.onPress}
        style={[styles.buttonGroupItemTouch, { backgroundColor: props.consecutiveBuy === props.buttonNum ? props.buttonColor : '#fff', borderColor: props.consecutiveBuy === props.buttonNum ? '#fff' : props.buttonColor }]}
      >
        <Text
          style={[styles.buttonGroupItemText, { color: props.consecutiveBuy === props.buttonNum ? '#fff' : props.buttonColor }]}
        >{props.buttonNum.toString()}</Text >
      </TouchableOpacity>
    </View>
  )
}
SelectButton.propTypes = {
  buttonNum: PropTypes.number.isRequired,
  consecutiveBuy: PropTypes.number.isRequired,
  onPress: PropTypes.func.isRequired,
  buttonColor: PropTypes.string
}
SelectButton.defaultProps = {
  buttonColor: '#c92033'
}
export default SelectButton

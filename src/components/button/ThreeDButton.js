import React from 'react'
import {
  StyleSheet, TouchableOpacity, View, Dimensions 
} from 'react-native'
import PropTypes from 'prop-types'
import { TextFont } from '../../ui'
const { height } = Dimensions.get('window')
const styles = StyleSheet.create({
  normalLoginBt: {
    backgroundColor: 'red',
    height: height * 0.082,
    borderRadius: 8,
    paddingBottom: 2
  },
  innerButton: {
    backgroundColor: 'blue',
    borderRadius: 8,
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row'
  },
  textButton: {
    flex: 1,
    justifyContent: 'center'
  },
  textLabelLogin: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center'
  }
})
const ThreeDButton = (props) => {
  return (
    <View>
      <TouchableOpacity
        onPress={props.onPress}
        activeOpacity={0.7}
        style={[styles.normalLoginBt, { backgroundColor: props.borderedColor, height: props.height }]}
      >
        <View style={[styles.innerButton, { backgroundColor: props.backgroundColor }]}>
          <View style={styles.textButton}>
            <TextFont style={styles.textLabelLogin}>{props.text}</TextFont>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  )
}
ThreeDButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  height: PropTypes.number,
  text: PropTypes.string,
  borderedColor: PropTypes.string,
  backgroundColor: PropTypes.string
}

ThreeDButton.defaultProps = {
  // onPress: () => {
  // },
  height: height * 0.082,
  text: 'Tiếp tục',
  borderedColor: '#8c0c1a',
  backgroundColor: '#c92033'
}

export default ThreeDButton

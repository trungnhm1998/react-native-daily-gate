import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  TouchableOpacity, View, Platform, TouchableNativeFeedback, ViewPropTypes 
} from 'react-native'
import _ from 'lodash'
import { TextFont, Themes } from '../../ui'

class MaterialButton extends Component {
  render() {
    const {
      style, textStyle, btnColor, children, textFont, textSize, onPress
    } = this.props

    if (Platform.OS === 'android') {
      return (
        <TouchableNativeFeedback background={TouchableNativeFeedback.SelectableBackground()} onPress={onPress}>
          <View
            style={[
              {
                padding: 15,
                backgroundColor: btnColor,
                borderRadius: Themes.Metrics.buttonRadius,
                shadowColor: '#20282F',
                elevation: 5,
                shadowOffset: {
                  width: 0,

                  height: 3
                },

                shadowRadius: 2,

                shadowOpacity: 0.08
              },
              style
            ]}
          >
            <TextFont font={textFont} size={textSize} style={textStyle}>
              {children}
            </TextFont>
          </View>
        </TouchableNativeFeedback>
      )
    }
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={[
          {
            padding: 15,
            backgroundColor: btnColor,
            borderRadius: Themes.Metrics.buttonRadius,
            shadowColor: '#20282F',
            elevation: 5,
            shadowOffset: {
              width: 0,

              height: 3
            },

            shadowRadius: 2,

            shadowOpacity: 0.08
          },
          style
        ]}
        onPress={onPress}
      >
        <TextFont font={textFont} size={textSize} style={textStyle}>
          {children}
        </TextFont>
      </TouchableOpacity>
    )
  }
}

MaterialButton.propTypes = {
  textStyle: PropTypes.any,
  btnColor: PropTypes.any,
  style: ViewPropTypes.style,
  textFont: PropTypes.string,
}

MaterialButton.defaultProps = {
  textStyle: {
    color: '#fff',
    textAlign: 'center'
  },
  btnColor: Themes.Colors.red,
  textFont: 'LatoRegular',
}

export default MaterialButton

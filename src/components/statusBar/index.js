import React from 'react'
import { StatusBar, View, Platform } from 'react-native'
import { Colors } from '../../ui/themes'

const StatusBarAndroid = ({ barStyle, navColor, translucent }) => {
  if (Platform.OS === 'android') {
    return (
      <StatusBar
        barStyle="light-content"
        backgroundColor={navColor}
        translucent={translucent}
      />
    )
  }
  return null
}

export default StatusBarAndroid

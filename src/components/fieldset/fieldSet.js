import React from 'react'
import { StyleSheet, View, ViewPropTypes } from 'react-native'
import PropTypes from 'prop-types'
import { TextFont } from '../../ui'

const styles = StyleSheet.create({
  fieldSetContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column'
  },
  fieldSetContentContainer: {
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth: 0.5,
    borderRadius: 10,
    flex: 1,
    width: null,
    alignSelf: 'stretch',
    borderStyle: 'dashed',
    marginTop: -10
  },
  labelContainer: {
    backgroundColor: '#fff',
    paddingLeft: 5,
    paddingRight: 5,
    zIndex: 2
  }
})
const FieldSet = (props) => {
  return (
    <View style={[styles.fieldSetContainer, { borderColor: props.borderColor }]}>
      <View style={[styles.labelContainer, props.labelStyle]}>
        <TextFont font="UTMHel" style={{ fontSize: 14, color: props.color }}>
          {props.fieldSetName}
        </TextFont>
      </View>
      <View style={styles.fieldSetContentContainer}>
        <View style={{ flex: 1 }}>{props.children}</View>
      </View>
    </View>
  )
}
FieldSet.propTypes = {
  fieldSetName: PropTypes.string.isRequired,
  labelStyle: ViewPropTypes.style,
  color: PropTypes.string,
  borderColor: PropTypes.string
}
FieldSet.defaultProps = {
  fieldSetName: 'FieldSet setName',
  labelStyle: {
    backgroundColor: '#fff'
  },
  color: '#004053',
  borderColor: '#c92033'
}
export default FieldSet

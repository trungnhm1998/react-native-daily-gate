import { Themes } from '../../ui'

export default {
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

  },
  contentContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff'
  },
  logo: {
    marginVertical: 10
  },
  heading: {
    textAlign: 'center',
    color: '#00a4de',
    fontSize: 21
  },

  buttonContainer: {
    marginTop: 20 * Themes.Metrics.ratioScreen,
    height: 40 * Themes.Metrics.ratioScreen,
    justifyContent: 'center'
  },
  buttonText: {
    color: '#8fbc5a',
    fontSize: 15,
    fontWeight: 'bold'
  }
}

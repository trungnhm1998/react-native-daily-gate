import React, { Component } from 'react'
import {
  Alert, Image, Text, TouchableOpacity, View 
} from 'react-native'
import PropTypes from 'prop-types'
import FingerprintScanner from 'react-native-fingerprint-scanner'
import { Modal, MaterialButton, Icon } from '..'
import styles from './FingerprintPopup.component.styles'
import { TextFont, Themes } from '../../ui'

class FingerprintPopup extends Component {
  constructor(props) {
    super(props)
    this.state = { errorMessage: undefined }
  }

  componentDidMount() {
    FingerprintScanner.authenticate({ onAttempt: this.handleAuthenticationAttempted })
      .then(() => {
        this.props.handlePopupDismissed()
        this.props.onMatchFingerPrint()
      })
      .catch((error) => {
        this.setState({ errorMessage: error.message })
      })
  }

  componentWillUnmount() {
    FingerprintScanner.release()
  }

  handleAuthenticationAttempted = (error) => {
    this.setState({ errorMessage: error.message })
  }

  render() {
    const { errorMessage } = this.state
    const { handlePopupDismissed } = this.props
    const { t: translate } = this.context
    return (
      <Modal height={0.3} title="Fingerprint" visible={this.props.visible} onClose={handlePopupDismissed}>
        <View style={styles.container}>
          <TextFont font="LatoBold" size={15} color={Themes.Colors.red}>
            {errorMessage || translate('Mời quét vân tay để tiếp tục')}
          </TextFont>
          <Icon name="icon_fingerprint" color={Themes.Colors.red} size={45} style={styles.logo} />
          <MaterialButton style={styles.buttonContainer} onPress={handlePopupDismissed}>
            TRỞ VỀ
          </MaterialButton>
        </View>
      </Modal>
    )
  }
}

FingerprintPopup.propTypes = {
  handlePopupDismissed: PropTypes.func.isRequired,
  onMatchFingerPrint: PropTypes.func.isRequired
}
FingerprintPopup.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}
export default FingerprintPopup

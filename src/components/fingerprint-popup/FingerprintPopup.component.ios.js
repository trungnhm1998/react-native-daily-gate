import React, { Component } from 'react'
import { AlertIOS } from 'react-native'
import FingerprintScanner from 'react-native-fingerprint-scanner'
import PropTypes from 'prop-types'
class FingerprintPopup extends Component {
  componentDidMount() {
    const { t: translate } = this.context
    FingerprintScanner.authenticate({ description: translate('Mời quét vân tay để tiếp tục'), fallbackEnabled: false })
      .then(() => {
        this.props.handlePopupDismissed()
        this.props.onMatchFingerPrint()
      })
      .catch((error) => {
        this.props.handlePopupDismissed()
        AlertIOS.alert(error.message)
      })
  }

  render() {
    return false
  }
}

FingerprintPopup.propTypes = {
  handlePopupDismissed: PropTypes.func.isRequired,
  onMatchFingerPrint: PropTypes.func.isRequired
}
FingerprintPopup.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}

export default FingerprintPopup

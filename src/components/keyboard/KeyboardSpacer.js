import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Keyboard,
  LayoutAnimation,
  View,
  Platform,
  StyleSheet,
  Text,
  ViewPropTypes
} from 'react-native'
const styles = StyleSheet.create({
  container: {
    left: 0,
    right: 0,
    bottom: 0
  },
  textButton: {
    height: 40,
    paddingTop: 10,
    color: '#007aff',
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16
  }
})
export default class KeyboardSpacer extends Component {
  static propTypes = {
    topSpacing: PropTypes.number,
    onToggle: PropTypes.func,
    onPressDone: PropTypes.func,
    doneLabel: PropTypes.string,
    style: ViewPropTypes.style,
    animationConfig: PropTypes.object
  }

  static defaultProps = {
    topSpacing: 0,
    doneLabel: 'Xong',
    // From: https://medium.com/man-moon/writing-modern-react-native-ui-e317ff956f02
    animationConfig: {
      duration: 500,
      create: {
        duration: 300,
        type: LayoutAnimation.Types.spring,
        property: LayoutAnimation.Properties.opacity
      },
      update: {
        type: LayoutAnimation.Types.spring,
        springDamping: 200
      },
    },
    onToggle: () => null
  }

  constructor(props, context) {
    super(props, context)
    this.state = {
      keyboardSpace: 0,
      isKeyboardOpened: false
    }
    this._listeners = null
    this.updateKeyboardSpace = this.updateKeyboardSpace.bind(this)
    this.resetKeyboardSpace = this.resetKeyboardSpace.bind(this)
  }

  componentDidMount() {
    const updateListener = Platform.OS === 'android' ? 'keyboardDidShow' : 'keyboardWillShow'
    const resetListener = Platform.OS === 'android' ? 'keyboardDidHide' : 'keyboardWillHide'
    this._listeners = [
      Keyboard.addListener(updateListener, this.updateKeyboardSpace),
      Keyboard.addListener(resetListener, this.resetKeyboardSpace)
    ]
  }

  componentWillUpdate(props, state) {
    if (state.isKeyboardOpened !== this.state.isKeyboardOpened) {
      LayoutAnimation.configureNext(props.animationConfig)
    }
  }

  componentWillUnmount() {
    this._listeners.forEach(listener => listener.remove())
  }

  updateKeyboardSpace(frames) {
    if (!frames.endCoordinates) {
      return
    }
    const keyboardSpace = frames.endCoordinates.height + this.props.topSpacing
    this.setState({
      keyboardSpace,
      isKeyboardOpened: true,
    }, this.props.onToggle(true, keyboardSpace))
  }

  resetKeyboardSpace() {
    this.setState({
      keyboardSpace: 0,
      isKeyboardOpened: false,
    }, this.props.onToggle(false, 0))
  }

  render() {
    if (Platform.OS === 'android') {
      return null
    }

    const doneButtonHeight = this.state.keyboardSpace > 40 ? 40 : 0
    const doneButton = this.props.onPressDone ?
      <View style={{ height: doneButtonHeight, flex: 1, backgroundColor: '#ddd', justifyContent: 'flex-end', flexDirection: 'row' }}>
        <Text style={styles.textButton} onPress={() => {
          this.props.onPressDone()
        }}>{this.props.doneLabel}</Text>
      </View> : null

    return (
      <View style={[styles.container, this.props.style]}>
        {doneButton}
        <View style={[styles.container, { height: this.state.keyboardSpace }]}/>
      </View>
    )
  }
}

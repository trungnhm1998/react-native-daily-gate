// truyen vao data, vs moi data có thể gắn thêm highlightValue=true de bôi đậm đỏ value

import React, { Component } from 'react'
import { Text, View } from 'react-native'
import PropTypes from 'prop-types'
import styles from './bill.styles'
import { TextFont, Themes } from '../../ui'
export default class Bill extends Component {
  renderContent = () => {
    console.log(this.props.data)
    return this.props.data.map((item, index) => (
      <View key={index} style={index === this.props.data.length - 1 ? styles.sectionContainerlast : styles.sectionContainer}>
        <View style={styles.topMask} />
        <View style={styles.rightMask} />
        <View style={styles.leftMask} />

        {item.map((el, elIndex) => (
          <View key={elIndex} style={styles.sectionContent}>
            <View
              style={{
                flex: 2,
                justifyContent: 'space-between',
                flexDirection: 'row',
                alignItems: 'center'
              }}
            >
              <TextFont font="LatoRegular" size={12}>
                {el.name}
              </TextFont>
              <TextFont font="LatoRegular">:</TextFont>
            </View>
            <View style={{ flex: 3, alignItems: 'flex-end' }}>
              <TextFont
                font={el.highlightValue ? 'LatoBold' : 'LatoRegular'}
                color={el.highlightValue ? Themes.Colors.red : Themes.Colors.black}
                size={12}
              >
                {el.value}
              </TextFont>
            </View>
          </View>
        ))}
      </View>
    ))
  }

  render() {
    return <View style={this.props.border ? styles.containerWithBorder : styles.containerWithoutBorder}>{this.renderContent()}</View>
  }
}

Bill.propTypes = {
  border: PropTypes.bool,
  data: PropTypes.array
}

Bill.defaultProps = {
  border: true
}

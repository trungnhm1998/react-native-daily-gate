import { StyleSheet } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  containerWithBorder: {
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    borderStyle: 'dashed',
    borderRadius: Themes.Metrics.buttonRadius,
    borderWidth: 1,
    borderColor: Themes.Colors.grey,

    backgroundColor: Themes.Colors.white,
    padding: 10 * Themes.Metrics.ratioScreen
  },
  containerWithoutBorder: {
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',

    backgroundColor: Themes.Colors.white,
    padding: 10 * Themes.Metrics.ratioScreen
  },
  sectionContainer: {
    width: '100%',
    borderStyle: 'dashed',
    borderRadius: 1,
    borderWidth: 1,
    borderColor: Themes.Colors.grey,
    flexDirection: 'column',
    backgroundColor: Themes.Colors.white,
    marginBottom: 10
  },
  topMask: {
    height: 3,
    width: '100%',
    backgroundColor: 'white',
    position: 'absolute',
    top: -3,
    left: 0
  },
  rightMask: {
    height: '100%',
    width: 3,
    backgroundColor: 'white',
    position: 'absolute',
    top: 0,
    right: -3
  },
  leftMask: {
    height: '100%',
    width: 3,
    backgroundColor: 'white',
    position: 'absolute',
    top: 0,
    left: -3
  },
  sectionContainerlast: {
    flexDirection: 'column',
    width: '100%'
  },
  sectionContent: {
    flexDirection: 'row',
    alignItems: 'center'
  }
})

export default styles

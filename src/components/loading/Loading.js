import React, { Component } from 'react'
import { View, ActivityIndicator, Modal } from 'react-native'
import { connect } from 'react-redux'
import { Themes } from '../../ui'

class Loading extends Component {
  render() {
    const { gateReducer } = this.props
    const { isLoading } = gateReducer
    return (
      <Modal
        animationType="none"
        transparent
        visible={isLoading}
        // presentationStyle="fullScreen"
      >
        <View
          style={{
            justifyContent: 'center',
            flex: 1,
            backgroundColor: 'rgba(0, 0, 0, 0.6)',
          }}
        >
          <ActivityIndicator size="small" color={Themes.Colors.red} />
        </View>
      </Modal>
    )
  }
}

// export default Loading
const mapStateToProps = (state) => {
  return {
    gateReducer: state.gateReducer,
  }
}

export default connect(
  mapStateToProps,
  null
)(Loading)
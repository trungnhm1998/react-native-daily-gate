// truyen vao footer,children,onClose
// height truyen vao la ti le chiem bao nhieu % so voi chieu dai man hinh
import React, { Component } from 'react'
import {
  TouchableOpacity, View, Alert, Modal as RNModal
} from 'react-native'
import PropTypes from 'prop-types'
import styles from './styles'
import { Icon } from '..'
import { TextFont, Themes } from '../../ui'

export default class Modal extends Component {
  render() {
    const paddingValue = ((1 - this.props.height) * Themes.Metrics.screenHeight) / 2

    return (
      <RNModal
        animationType="fade"
        transparent
        visible={this.props.visible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.')
        }}
      >
        <TouchableOpacity activeOpacity={1} onPress={this.props.onClose} style={{ ...styles.modalWrapper, paddingVertical: paddingValue }}>
          <TouchableOpacity activeOpacity={1} style={styles.modalContainer}>
            <View style={styles.title}>
              <TextFont style={{ alignSelf: 'center' }} font="LatoRegular" color={Themes.Colors.red}>
                {this.props.title}
              </TextFont>
              <TouchableOpacity
                style={{ marginLeft: 'auto' }}
                onPress={() => {
                  this.props.onClose()
                }}
              >
                <Icon name="icon_close" />
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1 }}>{this.props.children}</View>
            <View style={styles.footer}>{this.props.footer}</View>
          </TouchableOpacity>
        </TouchableOpacity>
      </RNModal>
    )
  }
}

Modal.propTypes = {
  visible: PropTypes.bool,
  title: PropTypes.string,
  height: PropTypes.number,
}

Modal.defaultProps = {
  visible: false,
  title: 'MODAL',
  height: 0.6,
}

import { StyleSheet } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  modalWrapper: {
    flex: 1,

    paddingHorizontal: Themes.Metrics.paddingHorizontalModal * Themes.Metrics.ratioScreen,
    paddingVertical: Themes.Metrics.paddingVerticalModal * Themes.Metrics.ratioScreen,
    height: 'auto',
    backgroundColor: 'rgba(0, 0, 0, 0.5)'
  },
  modalContainer: {
    flex: 1,
    borderRadius: Themes.Metrics.borderRadiusModal,
    backgroundColor: Themes.Colors.white,
    flexDirection: 'column',
    overflow: 'hidden'
  },
  title: {
    flexDirection: 'row',

    alignItems: 'center',
    padding: 10 * Themes.Metrics.ratioScreen,
    borderBottomColor: Themes.Colors.lightGrey,
    borderBottomWidth: 1,
    height: 50 * Themes.Metrics.ratioScreen
  },
  footer: {
    marginVertical: 10 * Themes.Metrics.ratioScreen,
    marginHorizontal: Themes.Metrics.paddingHorizontalModal * Themes.Metrics.ratioScreen
  }
})

export default styles

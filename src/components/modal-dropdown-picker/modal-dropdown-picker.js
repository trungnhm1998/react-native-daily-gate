// Truyền vào props data, onChangeValue,iconLeft,height, selectedValue,forceUpdate,addonRight
import React, { Component } from 'react'
import {
  Text,
  TouchableOpacity,
  View,
  Alert,
  Button,
  Image,
  ScrollView,
  TextInput,
  KeyboardAvoidingView,
  FlatList,
  TouchableWithoutFeedback
} from 'react-native'
import styles from './modal-dropdown-picker.styles'
import { Modal } from '..'
import { TextFont, Themes } from '../../ui'
import Icon from '../icon'
export default class ModalDropdownPicker extends Component {
  state = {
    modalVisible: false,
    query: '',
    resData: null
  }


  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.data !== prevProps.data) {
      this.setState({ resData: this.props.data })
    }
  }

  componentDidMount() {
    this.setState({ resData: this.props.data })
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible })
  }

  closeModal = () => {
    this.setState({ modalVisible: false })
  }

  renderList = () => {
    const { data } = this.props
    if (!data) {
      return (
        <TextFont font="LatoRegular" size={14}>
          No data
        </TextFont>
      )
    }
    // const resData = data.filter((el) => {
    //   return el.toLowerCase().indexOf(this.state.query.toLowerCase()) > -1
    // })
    //   console.log(data, resData)
    return this.state.resData.map(item => (
      <View style={styles.item}>
        <TextFont font="LatoRegular" size={14}>
          {item}
        </TextFont>
      </View>
    ))
  }

  onChangeValue = (item) => {
    this.props.onChangeValue(item)
    this.closeModal()
  }

  renderItem = ({ item }) => (
    <TouchableWithoutFeedback onPress={() => this.onChangeValue(item)}>
      <View style={styles.item}>
        <TextFont font="LatoRegular" size={14}>
          {item}
        </TextFont>
      </View>
    </TouchableWithoutFeedback>
  )

  changeQueryText = (query) => {
    const { data } = this.props
    if (!data) {
      return null
    }
    const resData = data.filter((el) => {
      return el.toLowerCase().indexOf(query.toLowerCase()) > -1
    })

    this.setState({ resData, query })
  }

  renderSeachBar = () => {
    return (
      <View style={styles.searchContainer}>
        <View style={styles.searchWrapper}>
          <Icon name="icon_search" />
          <TextInput style={styles.searchInput} onChangeText={this.changeQueryText} value={this.state.query} />
        </View>
      </View>
    )
  }

  keyExtractor = (item, index) => {
    return index
  }

  render() {
    return (
      <TouchableOpacity
        style={{ ...this.props.style, ...styles.container }}
        onPress={() => {
          this.setModalVisible(true)
        }}
      >
        <Modal height={0.8} visible={this.state.modalVisible} onClose={this.closeModal}>
          <View style={{ flex: 1, marginBottom: 10 * Themes.Metrics.ratioScreen }}>
            {this.renderSeachBar()}
            <FlatList
              keyboardDismissMode="on-drag"
              style={{ flex: 1 }}
              data={this.state.resData}
              extraData={this.state}
              renderItem={this.renderItem}
              keyExtractor={this.keyExtractor}
              showsVerticalScrollIndicator={false}
            />
          </View>
        </Modal>
        <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
          {this.props.iconLeft ? <Image style={styles.iconLeft} source={this.props.iconLeft} /> : null}
          <TextFont font="LatoRegular" size={14}>
            {this.props.selectedValue ? this.props.selectedValue : this.props.placeholderText}
          </TextFont>
          <View style={{ flexDirection: 'row', marginLeft: 'auto' }}>
            {this.props.addonRight && <TextFont style={{ paddingRight: Themes.Metrics.paddingHorizontal }}>{this.props.addonRight}</TextFont>}
            <Icon name="icon_dropdown" style={this.props.addonRight && { marginTop: Themes.Metrics.baseMargin }} size={7} />
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

ModalDropdownPicker.defaultProps = {
  placeholderText: ' Choose value'
}

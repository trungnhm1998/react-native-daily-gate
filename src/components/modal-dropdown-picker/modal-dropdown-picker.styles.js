import { StyleSheet } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    ...Themes.styleGB.materialStyle,
    borderRadius: Themes.Metrics.buttonRadius,
    height: 50 * Themes.Metrics.ratioScreen,
    backgroundColor: Themes.Colors.white,
    padding: 10 * Themes.Metrics.ratioScreen
  },
  modalContainer: {
    flex: 1,
    backgroundColor: Themes.Colors.white,
    opacity: 1
  },
  item: {
    borderTopColor: Themes.Colors.lightGrey,
    borderTopWidth: 1,

    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: Themes.Metrics.paddingHorizontal * Themes.Metrics.ratioScreen,
    paddingVertical: Themes.Metrics.paddingVertical * Themes.Metrics.ratioScreen
  },
  searchContainer: {
    height: 50 * Themes.Metrics.ratioScreen,
    backgroundColor: Themes.Colors.lightGrey,
    paddingHorizontal: 10,
    paddingVertical: 5
  },
  searchWrapper: {
    borderRadius: Themes.Metrics.buttonRadius,
    backgroundColor: Themes.Colors.white,
    flex: 1,

    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10
  },
  searchInput: {
    flex: 1,
    marginLeft: 5 * Themes.Metrics.ratioScreen,
    color: Themes.Colors.black,
    fontSize: 15
  },
  iconLeft: {
    marginRight: 10 * Themes.Metrics.ratioScreen
  }
})

export default styles

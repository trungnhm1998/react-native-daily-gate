import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Navigation } from 'react-native-navigation'
import ImagePicker from 'react-native-image-picker'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'
import _ from 'lodash'
import RegisterInfo from './register-info'
import { gateActions } from '../../redux/daily-gate'
import { ScreenConfigs, AppNavigation } from '../../navigation'
import { Themes } from '../../ui'

const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
class RegisterInfoContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      avatarSourceBefor: '',
      avatarSourceAfter: ''
    }
  }

  componentWillMount() {
    this.props.gateAction.getListAddress()
  }

  onSelectImageBefor = () => {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response)
    
      if (response.didCancel) {
        console.log('User cancelled image picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton)
      } else {
        const source = { uri: response.uri }
        this.setState({
          avatarSourceBefor: source,
        })
      }
    })
  }

  onSelectImageAfter = () => {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response)
    
      if (response.didCancel) {
        console.log('User cancelled image picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton)
      } else {
        const source = { uri: response.uri }
        this.setState({
          avatarSourceAfter: source,
        })
      }
    })
  }

  onPressSecurity = () => {
    const { t: translate } = this.context
    Navigation.push(this.props.componentId, {
      component: {
        name: ScreenConfigs.SECURITY_PAGE,
        options: {
          topBar: {
            visible: true,
            drawBehind: false,
            // animate: false,
            backButton: {
              color: Themes.Colors.white
            },
            background: {
              color: Themes.Colors.topBarColor,
              translucent: false
            },
            title: {
              text: translate('Phương thức bảo mật GD'),
              color: Themes.Colors.white
            }
          }
        },
        passProps: {
          translate: translate
        }
      }
    })
  }

  renderNumberValidate = (number) => {
    if (_.filter(this.props.gateReducer.cmndList, number) !== []) {
      return true
    }
    return false
  }

  onSubmitForm = (data) => {
    this.props.gateAction.verifyInfoRegister(data, {
      nextScreen: () => this.onPressSecurity()
    })
  }

  render() {
    const { t: translate } = this.context
    const { i18nState, gateReducer } = this.props
    return (
      <RegisterInfo
        onPressSecurity={this.onPressSecurity}
        translate={translate}
        i18nState={i18nState}
        onSelectImageBefor={this.onSelectImageBefor}
        onSelectImageAfter={this.onSelectImageAfter}
        avatarSourceBefor={this.state.avatarSourceBefor ? this.state.avatarSourceBefor : null}
        avatarSourceAfter={this.state.avatarSourceAfter ? this.state.avatarSourceAfter : null}
        gateReducer={gateReducer}
        renderNumberValidate={this.renderNumberValidate}
        onSubmitForm={this.onSubmitForm}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState,
    gateReducer: state.gateReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    gateAction: bindActionCreators(gateActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterInfoContainer)

RegisterInfoContainer.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}

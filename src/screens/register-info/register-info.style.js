import { StyleSheet, Dimensions } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  registerContainer: {
    flex: 1,
    backgroundColor: Themes.Colors.backgroundColor,
    paddingHorizontal: Themes.Metrics.basePadding
  },
  lable: {
    textAlign: 'left'
  },
  imageContainer: {
    flexDirection: 'row',
    marginVertical: Themes.Metrics.baseMargin,
    width: '100%',
  },
  imageLeft: {
    ...Themes.styleGB.materialStyle,
    borderRadius: 10,
    borderColor: Themes.Colors.gray,
    borderWidth: 0.7,
    marginRight: Themes.Metrics.baseMargin,
    width: (Dimensions.get('window').width / 2 - 15) 
  },
  imageRight: {
    width: (Dimensions.get('window').width / 2 - 15),
    ...Themes.styleGB.materialStyle,
    borderRadius: 10,
    borderColor: Themes.Colors.lightGrey,
    borderWidth: 0.7,
  },
  borderLeft: {
    margin: Themes.Metrics.baseMargin,  
  },
  borderContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  borderImage: {
    width: 80 * Themes.Metrics.ratioScreen,
    height: 80 * Themes.Metrics.ratioScreen,
    
  },
  imageStyle: {
    width: '100%',
    height: '100%',
  },
  iconImageStyle: {
    marginTop: Themes.Metrics.baseMargin,
    marginLeft: Themes.Metrics.baseMargin
  },
  selecImage: {
    width: '100%',
    backgroundColor: Themes.Colors.red,
    ...Themes.styleGB.materialStyle,
    borderRadius: 10,
    marginTop: Themes.Metrics.smallMargin,
    paddingVertical: Themes.Metrics.smallPadding
  },
  submitButton: {
    borderRadius: 10,
    width: '100%',
    paddingVertical: Themes.Metrics.basePadding,
    backgroundColor: Themes.Colors.red,
    marginVertical: Themes.Metrics.baseMargin
  },
  lableGuide: {
    color: Themes.Colors.red,
    textDecorationLine: 'underline'
  }
})

export default styles

import { StyleSheet, Dimensions } from 'react-native'
import { Themes } from '../../../ui'

const styles = StyleSheet.create({
  securityContainer: {
    flex: 1,
    backgroundColor: Themes.Colors.backgroundColor,
    paddingHorizontal: Themes.Metrics.basePadding
  },
  borderButton: {
    width: '100%',
    borderRadius: 10,
    backgroundColor: Themes.Colors.white,
    borderWidth: 0.5,
    ...Themes.styleGB.materialStyle,
    marginTop: Themes.Metrics.baseMargin
  },
  customBorder: {
    padding: 15,
    flexDirection: 'row',
  },
  textRadio: {
    paddingLeft: Themes.Metrics.basePadding
  },
  submitButton: {
    width: '100%',
    borderRadius: 10,
    backgroundColor: Themes.Colors.red,
    ...Themes.styleGB.materialStyle,
    marginTop: Themes.Metrics.marginHorizontal
  },
  textSubmit: {
    textAlign: 'center', 
    color: Themes.Colors.white,
    padding: 15
  },
  textNote: {
    textAlign: 'center',
    color: Themes.Colors.gray
  }
})

export default styles

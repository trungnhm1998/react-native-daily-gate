import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Navigation } from 'react-native-navigation'
import SecurityPage from './security-page'
import { ScreenConfigs, AppNavigation } from '../../../navigation'
import { Themes } from '../../../ui'

class SecurityPageContainer extends Component {
  
  componentWillMount() {
    // alert('mounted')
  }

  onPressPasswordPage = () => {
    const { t: translate } = this.context
    Navigation.push(this.props.componentId, {
      component: {
        name: ScreenConfigs.PASSWORD_PAGE,
        options: {
          topBar: {
            visible: true,
            drawBehind: false,
            // animate: false,
            backButton: {
              color: Themes.Colors.white
            },
            background: {
              color: Themes.Colors.topBarColor,
              translucent: false
            },
            title: {
              text: translate('Tạo mật khẩu giao dịch'),
              color: Themes.Colors.white
            }
          }
        }
      }
    })
  }

  render() {
    const { t: translate } = this.context
    const { i18nState } = this.props
    return (
      <SecurityPage
        translate={translate}
        i18nState={i18nState}
        onPressPasswordPage={this.onPressPasswordPage}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SecurityPageContainer)

SecurityPageContainer.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}

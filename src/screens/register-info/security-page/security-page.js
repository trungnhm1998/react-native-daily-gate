import React, { Component } from 'react'
import {
  View, Image, TouchableOpacity, ScrollView,
} from 'react-native'
import { Themes, TextFont } from '../../../ui'
import styles from './security-page.style'
import appSymbol from '../../../assets/symbols'
import AppImages from '../../../assets/images'
import { Icon, TcombForm, ModalDropdownPicker } from '../../../components'

export default class SecirityPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isPassCheck: false,
      isOTPCheck: false,
      isDontPass: false,
      valuePassCheck: 'Mật khẩu giao dịch',
      valueOTP: 'OTP',
      valueDontPass: 'Không sử dụng MKGD',
      valueSubmit: '',
    }
  }

  onSubmit() {
    const { isPassCheck, isOTPCheck, isDontPass, valuePassCheck, valueOTP, valueDontPass } = this.state
    if (isPassCheck) {
      this.setState({ valueSubmit: valuePassCheck }, () =>  console.log('value:', this.state.valueSubmit))
    }
    if (isOTPCheck) {
      this.setState({ valueSubmit: valueOTP }, () =>  console.log('value:', this.state.valueSubmit))
    }
    if (isDontPass) {
      this.setState({ valueSubmit: valueDontPass }, () =>  console.log('value:', this.state.valueSubmit))
    }
    this.props.onPressPasswordPage()
  }

  render() {
    const { translate } = this.props
    const { isPassCheck, isOTPCheck, isDontPass } = this.state
    return (
      <View style={styles.securityContainer}>
        <TouchableOpacity style={[styles.borderButton, isPassCheck ? { borderColor: Themes.Colors.red } : { borderColor: Themes.Colors.lightGrey }]} onPress={() => this.setState({ isPassCheck: !isPassCheck,isOTPCheck: false, isDontPass: false })}>
          <View style={styles.customBorder}>
            <Image source={isPassCheck ? appSymbol.icon_input_check_act : appSymbol.icon_input_check_nor} />
            <TextFont font="LatoLight" size={12} style={styles.textRadio}>{translate('Mật khẩu giao dịch')}</TextFont>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.borderButton, isOTPCheck ? { borderColor: Themes.Colors.red } : { borderColor: Themes.Colors.lightGrey }]} onPress={() => this.setState({ isOTPCheck: !isOTPCheck, isPassCheck: false, isDontPass: false })}>
          <View style={styles.customBorder}>
            <Image source={isOTPCheck ? appSymbol.icon_input_check_act : appSymbol.icon_input_check_nor} />
            <TextFont font="LatoLight" size={12} style={styles.textRadio}>{translate('OTP')}</TextFont>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.borderButton, isDontPass ? { borderColor: Themes.Colors.red } : { borderColor: Themes.Colors.lightGrey }]} onPress={() => this.setState({ isDontPass: !isDontPass, isPassCheck: false, isOTPCheck: false })}>
          <View style={styles.customBorder}>
            <Image source={isDontPass ? appSymbol.icon_input_check_act : appSymbol.icon_input_check_nor} />
            <TextFont font="LatoLight" size={12} style={styles.textRadio}>{translate('Không sử dụng MKGD')}</TextFont>
          </View>
        </TouchableOpacity>
        { isDontPass && <TextFont font="LatoLight" size={12} style={{ color: Themes.Colors.red, paddingTop: Themes.Metrics.smallPadding, paddingLeft: Themes.Metrics.smallPadding }}>{translate('Để an toàn giao dịch, quý khách vui lòng thiết lập MKGD')}</TextFont>}
        <TouchableOpacity style={styles.submitButton} onPress={() => this.onSubmit()}>
          <TextFont font="LatoLight" size={12} style={styles.textSubmit}>{translate('Tiếp tục')}</TextFont>
        </TouchableOpacity>
        <View style={{ paddingTop: 15 }}>
          <TextFont font="LatoLight" size={12} style={styles.textNote}>
            {translate('(*) Cấu hình phương thức bảo mật GD là bạn đã đồng ý với các điều khoản bảo mật giao dịch của chúng tôi.')}
          </TextFont>
        </View>
      </View>
    )
  }
}

import React, { Component } from 'react'
import {
  View, Image, TouchableOpacity, ScrollView
} from 'react-native'
import _ from 'lodash'
import { Themes, TextFont } from '../../ui'
import styles from './register-info.style'
import appSymbol from '../../assets/symbols'
import AppImages from '../../assets/images'
import { Icon, TcombForm, ModalDropdownPicker } from '../../components'

const tcomb = require('tcomb-form-native')
const { Form } = tcomb.form

const userAccount = tcomb.struct({
  nameUser: tcomb.String,
  emailUser: tcomb.String,
  cmndNumber: tcomb.String,
  address: tcomb.String
})

const dataCity = []
export default class RegisterInfo extends Component {
  constructor(props) {
    super(props)

    // form value to validate phoneNumber
    this.value = {
      cmndNumberCorrect: false
    }
    this.dataProvine = null


    this.state = {
      value: {
        nameUser: '',
        emailUser: '',
        cmndNumber: '',
        address: ''
      },
      selectedCity: '',
      selectedProvine: '',
      valueProvine: '',
      errorValue: false,
      codeValue: ''
    }
  }

  onChange(value) {
    this.setState({
      value
    })
  }

  componentDidMount() {
    _.forEach(this.props.gateReducer.listCity, function(value, key){
        dataCity.push(value.name)
    })
  }

  static getDerivedStateFromProps(props, state) {
    if (props.gateReducer.APIRespond.code === 900) {
      return {
        codeValue: props.gateReducer.APIRespond.code
      }
    }
    return {}
  }

  renderCMNDNumberValidate = () => {
    if (this.state.codeValue === 900) {
      return <Image source={appSymbol.icon_input_error} />
    }
  }

  renderForm(translate) {
    const layout = (locals) => {
      return (
        <View style={{ marginTop: 15 }}>
          <TextFont font="LatoLight" size={12} style={styles.lable}>{translate('THÔNG TIN CÁ NHÂN')}</TextFont>
          <View>{locals.inputs.nameUser}</View>
          <View>{locals.inputs.emailUser}</View>
          <TextFont font="LatoLight" size={12} style={styles.lable}>{translate('CMND')}</TextFont>
          <View>{locals.inputs.cmndNumber}</View>
          <View style={{ flexDirection: 'row' }}>
            <TextFont font="LatoLight" size={12} style={styles.lable}>{translate('Chụp ảnh cùng CMND/GPKD(')}</TextFont>
            <TouchableOpacity>
              <TextFont font="LatoLight" size={12} style={styles.lableGuide}>{translate('Xem hướng dẫn')}</TextFont>
            </TouchableOpacity>
            <TextFont font="LatoLight" size={12} style={styles.lable}>)</TextFont>
          </View>
          {this.renderImage(translate)}
          <TextFont font="LatoLight" size={12} style={styles.lable}>{translate('Địa chỉ')}</TextFont>
          <View>{locals.inputs.address}</View>
          {this.renderFormAddress(translate)}
        </View>
      )
    }
    

    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        nameUser: {
          template: TcombForm.InputFLoatingLabel_tcomb,
          label: 'Ho ten/ cong ty',
          config: {
          }
        },
        emailUser: {
          template: TcombForm.InputFLoatingLabel_tcomb,
          label: 'Email',
          keyboardType: 'email-address',
          config: {
          }
        },
        cmndNumber: {
          template: TcombForm.InputFLoatingLabel_tcomb,
          label: 'Số CMND/MST',
          keyboardType: 'phone-pad',
          hasError: this.state.codeValue === 900 ? true : false,
          error: translate('CMND đã tồn tại. Xin vui lòng thử lại hoặc kiểm tra lại'),
          config: {
            rightAddon: this.renderCMNDNumberValidate()
          }
        },
        address: {
          template: TcombForm.InputFLoatingLabel_tcomb,
          label: 'Dia chi',
          config: {
          }
        },
      }
    }

    return (
      <Form
        ref={(form) => {
          this.form = form
        }}
        type={userAccount}
        options={options}
        onChange={value => this.onChange(value)}
        value={this.state.value}
      />
    )
  }

  renderImage = (translate) => {
    return (
      <View style={styles.imageContainer}>
        <View style={styles.imageLeft}>
          <View style={styles.borderLeft}>
            <View style={styles.borderContainer}>
              <TextFont font="LatoLight" size={12}>{translate('Mặt trước')}</TextFont>
              <View style={styles.borderImage}>
                {this.props.avatarSourceBefor ? <Image source={this.props.avatarSourceBefor} style={styles.imageStyle} /> : <Icon name="icon_photo_frame" size={50} color={Themes.Colors.lightGrey} style={styles.iconImageStyle} />}
              </View>
            </View>
            <TouchableOpacity style={[styles.selecImage, this.props.avatarSourceBefor ? { backgroundColor: '#243549' } : { backgroundColor: Themes.Colors.red }]} onPress={this.props.onSelectImageBefor}>
              <TextFont font="LatoLight" style={{ color: Themes.Colors.white }} size={15}>{this.props.avatarSourceBefor ? translate('CHỌN LẠI') : translate('CHỌN ẢNH')}</TextFont>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.imageRight}>
          <View style={styles.borderLeft}>
            <View style={styles.borderContainer}>
              <TextFont font="LatoLight" size={12}>{translate('Mặt sau')}</TextFont>
              <View style={styles.borderImage}>
                {this.props.avatarSourceAfter ? <Image source={this.props.avatarSourceAfter} style={styles.imageStyle} /> : <Icon name="icon_photo_frame" size={50} color={Themes.Colors.lightGrey} style={styles.iconImageStyle} />}
              </View>
            </View>
            <TouchableOpacity style={[styles.selecImage, this.props.avatarSourceAfter ? { backgroundColor: '#243549' } : { backgroundColor: Themes.Colors.red }]} onPress={this.props.onSelectImageAfter}>
              <TextFont font="LatoLight" size={15} style={{ color: Themes.Colors.white }}>{this.props.avatarSourceAfter ? translate('CHỌN LẠI') : translate('CHỌN ẢNH')}</TextFont>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }

  onSetDataProvine = (item) => {
    this.dataProvine = _.filter(this.props.gateReducer.listCity, ['name', item])
    this.setState({ valueProvine: this.dataProvine[0].provine, selectedCity: item, selectedProvine: this.dataProvine[0].provine[0] })
   
  }

  renderFormAddress = (translate) => {
    if (this.props.gateReducer.listCity) {
      return (
        <View>
          <ModalDropdownPicker
            onChangeValue={item => this.onSetDataProvine(item)}
            selectedValue={this.state.selectedCity}
            data={dataCity}
            style={{ width: '100%' }}
            placeholderText={translate('Tỉnh/thành')}
          />
          <ModalDropdownPicker
            onChangeValue={item => this.setState({ selectedProvine: item })}
            selectedValue={this.state.selectedProvine}
            data={this.state.valueProvine ? this.state.valueProvine : null}
            style={{ width: '100%', marginTop: Themes.Metrics.baseMargin }}
            placeholderText={translate('Quận/huyện')}
          />
        </View>
      )
    }
  }

  checkData = () => {
    const { value, selectedCity, selectedProvine  } = this.state
    const { avatarSourceBefor, avatarSourceAfter } = this.props
    const data = {
      userName: this.state.value.nameUser,
      cmndNumber: this.state.value.cmndNumber
    }
    if ((value.nameUser === null || value.emailUser === null || value.cmndNumber === null || value.address === null ||
       selectedCity === null || selectedProvine === null || avatarSourceBefor === null || avatarSourceAfter === null)) {
      this.setState({ valueNull : true})
    } else {
      this.props.onSubmitForm(data)
    }
  }

  renderButton = (translate) => {
    return (
      <TouchableOpacity style={styles.submitButton} onPress={() => this.checkData()}>
        <TextFont font="LatoLight" size={12} style={{ color: Themes.Colors.white }}>{translate('Tiếp tục')}</TextFont>
      </TouchableOpacity>
    )
  }

  render() {
    const { translate } = this.props
    return (
      <ScrollView style={styles.registerContainer}>
        {this.renderForm(translate)}
        {this.renderButton(translate)}
      </ScrollView>
    )
  }
}

import { StyleSheet, Dimensions } from 'react-native'
import { Themes } from '../../../ui'

const styles = StyleSheet.create({
  passwordContainer: {
    flex: 1,
    backgroundColor: Themes.Colors.backgroundColor,
    paddingHorizontal: Themes.Metrics.basePadding
  },
  submitButton: {
    width: '100%',
    borderRadius: 10,
    backgroundColor: Themes.Colors.red,
    ...Themes.styleGB.materialStyle,
    marginTop: Themes.Metrics.baseMargin
  },
  textSubmit: {
    textAlign: 'center', 
    color: Themes.Colors.white,
    padding: 15
  },
  borderInput: {
    width: '100%',
    borderRadius: 10,
    backgroundColor: Themes.Colors.white,
    ...Themes.styleGB.materialStyle,
    marginTop: Themes.Metrics.smallMargin,
    marginBottom: Themes.Metrics.baseMargin
  },
  inputStyle: {
    padding: 15
  }
})

export default styles

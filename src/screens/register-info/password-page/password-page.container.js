import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Navigation } from 'react-native-navigation'
import PropTypes from 'prop-types'
import PasswordPage from './password-page'
import { ScreenConfigs, AppNavigation } from '../../../navigation'
import { Themes } from '../../../ui'

class PasswordPageContainer extends Component {
  
  componentWillMount() {
    // alert('mounted')
  }

  render() {
    const { t: translate } = this.context
    const { i18nState } = this.props
    return (
      <PasswordPage
        translate={translate}
        i18nState={i18nState}
        
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PasswordPageContainer)

PasswordPageContainer.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}

import React, { Component } from 'react'
import {
  View, Image, TouchableOpacity, TextInput,
} from 'react-native'
import { Themes, TextFont } from '../../../ui'
import styles from './password-page.style'
import appSymbol from '../../../assets/symbols'
import AppImages from '../../../assets/images'
import { Icon, TcombForm, ModalDropdownPicker } from '../../../components'

export default class PasswordPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      password: '',
      rePassword: '',
    }
  }

  onChange(value) {
    this.setState({
      value
    })
  }

  renderForm(translate) {
    return (
      <View style={{ marginTop: 15 }}>
        <TextFont font="LatoLight" size={12} style={{ textAlign: 'left' }}>{translate('MẬT KHẨU GIAO DỊCH')}</TextFont>
        <View style={styles.borderInput}>
          <TextInput 
            style={styles.inputStyle}
            onChangeText={(password) => { this.setState({ password }) }}
            value={this.state.password}
            underlineColorAndroid="transparent"
            placeholderTextColor="#fff"
          />
        </View>
        <TextFont font="LatoLight" size={12} style={{ textAlign: 'left' }}>{translate('NHẬP LẠI MẬT KHẨU GIAO DỊCH')}</TextFont>
        <View style={styles.borderInput}>
          <TextInput 
            style={styles.inputStyle}
            onChangeText={(rePassword) => { this.setState({ rePassword }) }}
            value={this.state.rePassword}
            underlineColorAndroid="transparent"
            placeholderTextColor="#fff"
          />
        </View>
      </View>
    )
  }

  render() {
    const { translate } = this.props
    return (
      <View style={styles.passwordContainer}>
        {this.renderForm(translate)}
        <TouchableOpacity style={styles.submitButton} onPress={() => this.onSubmit()}>
          <TextFont font="LatoLight" size={12} style={styles.textSubmit}>{translate('Tiếp tục')}</TextFont>
        </TouchableOpacity>
      </View>
    )
  }
}

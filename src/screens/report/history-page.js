import React, { Component } from 'react'
import {
  View, TouchableOpacity, Image, FlatList
} from 'react-native'
import { Calendar } from 'react-native-calendars'

import { Themes, TextFont } from '../../ui'
import styles from './report.style'
import styless from '../setting/setting.style'
import appSymbol from '../../assets/symbols'
import AppImages from '../../assets/images'
import { Icon } from '../../components'

export default class HistoryPage extends Component {
  constructor(props) {
    super(props)
  }
  
  render() {
    const { translate } = this.props
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.historyContainer}>
          <View style={styles.everyItem}>
            <View style={styles.itemLeft}>
              <View style={styles.iconLeftHistory}></View>
              <TextFont font="LatoRegular" size={12} style={styles.itemHistory}>{translate('Lịch sử truy cập')}</TextFont>
            </View>
            <TouchableOpacity style={styles.itemRight}>
              <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
            </TouchableOpacity>
          </View>
          <View style={styles.everyItem}>
            <View style={styles.itemLeft}>
              <View style={styles.iconLeftHistory}></View>
              <TextFont font="LatoRegular" size={12} style={styles.itemHistory}>{translate('Lịch sử nạp tiền ngân hàng')}</TextFont>
            </View>
            <TouchableOpacity style={styles.itemRight}>
              <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
            </TouchableOpacity>
          </View>
          <View style={styles.everyItem}>
            <View style={styles.itemLeft}>
              <View style={styles.iconLeftHistory}></View>
              <TextFont font="LatoRegular" size={12} style={styles.itemHistory}>{translate('Lịch sử nạp thẻ cào')}</TextFont>
            </View>
            <TouchableOpacity style={styles.itemRight}>
              <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
            </TouchableOpacity>
          </View>
          <View style={styles.everyItem}>
            <View style={styles.itemLeft}>
              <View style={styles.iconLeftHistory}></View>
              <TextFont font="LatoRegular" size={12} style={styles.itemHistory}>{translate('Lịch sử chuyển tiền')}</TextFont>
            </View>
            <TouchableOpacity style={styles.itemRight}>
              <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
            </TouchableOpacity>
          </View>
          <View style={styles.everyItem}>
            <View style={styles.itemLeft}>
              <View style={styles.iconLeftHistory}></View>
              <TextFont font="LatoRegular" size={12} style={styles.itemHistory}>{translate('Lịch sử hoàn tiền')}</TextFont>
            </View>
            <TouchableOpacity style={styles.itemRight}>
              <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
            </TouchableOpacity>
          </View>
          <View style={styles.everyItem}>
            <View style={styles.itemLeft}>
              <View style={styles.iconLeftHistory}></View>
              <TextFont font="LatoRegular" size={12} style={styles.itemHistory}>{translate('Lịch sử SMS')}</TextFont>
            </View>
            <TouchableOpacity style={styles.itemRight}>
              <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
            </TouchableOpacity>
          </View>
        </View>
      </View>

    )
  }
}
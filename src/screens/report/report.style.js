import { StyleSheet, Dimensions } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  reportContainer: {
    flex: 1,
  },
  topBarStyle: {
    flex: 1,
    backgroundColor: Themes.Colors.white,
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    ...Themes.styleGB.materialStyle
  },
  itemTopBar: {
    paddingHorizontal: Themes.Metrics.basePadding
  },
  topButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50 * Themes.Metrics.ratioScreen
  },
  topButton_selected: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50 * Themes.Metrics.ratioScreen,
    borderBottomColor: Themes.Colors.red,
    borderBottomWidth: 3
  },
  revenue: {
    flex: 1,
    backgroundColor: Themes.Colors.backgroundColor,
  },
  calendarContainer: {
    flexDirection: 'row'
  },
  calendarStyle: {
    width: '100%',
    flexDirection: 'row',
    borderRadius: 10,
    backgroundColor: Themes.Colors.white,
    paddingVertical: Themes.Metrics.basePadding,
    marginLeft: Themes.Metrics.baseMargin,
    paddingLeft: Themes.Metrics.baseMargin,
    ...Themes.styleGB.materialStyle
  },
  startDay: {
    alignItems: 'flex-start',
    marginRight: Themes.Metrics.baseMargin
  },
  endDay: {
    marginLeft: Themes.Metrics.baseMargin
  },
  lableDay: {
    textAlign: 'left',
    paddingVertical: Themes.Metrics.basePadding
  },
  calendar: {
    marginLeft: Themes.Metrics.marginHorizontal
  },
  valueDay: {
    paddingRight: Themes.Metrics.basePadding * 2,
  },
  customCalendar: {
    position: 'absolute',
    width: 190,
    top: 90,
    borderRadius: 10,
    backgroundColor: Themes.Colors.gray,
    opacity: 9999
  },
  surplus: {
    borderWidth: 0.5,
    borderRadius: 10,
    borderColor: Themes.Colors.gray,
    marginVertical: Themes.Metrics.baseMargin,
    borderStyle: 'dashed',
  },
  surplusValue: {
    padding: Themes.Metrics.basePadding,
    flexDirection: 'row',
    width: '100%'
  },
  surplusLeft: {
    flex: 1,
    textAlign: 'left'
  },
  surplusCenter: {
    flex: 1, 
    alignItems: 'center'
  },
  surplusRight: {
    alignItems: 'flex-end'
  },
  lableStyle: {
    backgroundColor: Themes.Colors.lightGrey,
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: Themes.Metrics.basePadding,
  },
  submitButton: {
    borderRadius: 10,
    width: '100%',
    paddingVertical: Themes.Metrics.basePadding,
    backgroundColor: Themes.Colors.red,
    marginVertical: Themes.Metrics.baseMargin
  },
  everyItem: {
    padding: Themes.Metrics.basePadding,
    borderBottomWidth: 0.3,
    borderColor: Themes.Colors.whiteOpacity,
    flexDirection: 'row',
    width: '100%',
  },
  itemLeft: {
    flexDirection: 'row',
  },
  itemRight: {
    flexDirection: 'row',
    marginLeft: 'auto'
  },
  item: {
    paddingLeft: Themes.Metrics.basePadding,
  },
  time: {
    textAlign: 'left',
    paddingLeft: Themes.Metrics.basePadding
  },
  mkStyle: {
    marginRight: Themes.Metrics.smallMargin,
  },
  historyContainer: {
    backgroundColor: Themes.Colors.white,
    paddingLeft: Themes.Metrics.paddingHorizontal,
    paddingRight: Themes.Metrics.paddingHorizontal,
    ...Themes.styleGB.materialStyle,
    marginVertical: Themes.Metrics.baseMargin
  },
  borderIcon: {
    padding: Themes.Metrics.smallPadding
  },
  iconDate: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconLeft: {
    borderRadius: 30,
    backgroundColor: Themes.Colors.red,
    marginVertical: 20,
    padding: 5
  },
  iconLeftHistory: {
    borderRadius: 30,
    backgroundColor: Themes.Colors.red,
    marginVertical: 10,
    padding: 5
  },
  itemHistory: {
    paddingTop: Themes.Metrics.smallMargin,
    paddingLeft: Themes.Metrics.paddingHorizontal
  }
})

export default styles

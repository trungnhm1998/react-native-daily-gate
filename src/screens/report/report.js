import React, { Component } from 'react'
import {
  View, TouchableOpacity,Text
} from 'react-native'
import ScrollableTabView, { ScrollableTabBar, } from 'react-native-scrollable-tab-view'
import { Themes, TextFont } from '../../ui'
import styles from './report.style'
import appSymbol from '../../assets/symbols'
import AppImages from '../../assets/images'
import { Icon } from '../../components'
import SummaryPage from './summary-page'
import DetailRevenue from './detail_revenue'
import AgencyPage from './agency-page'
import History from './history-page'

export default class Report extends Component {
  constructor(props) {
    super(props)
  }

  renderTopButton = (translate) => {
    return (
      <ScrollableTabView
        renderTabBar={() => <ScrollableTabBar style={{ paddingHorizontal: Themes.Metrics.basePadding }} />}
      >
        <View style={styles.revenue} tabLabel={translate('Tổng hợp')}><SummaryPage translate={translate} /></View>
        <View style={styles.revenue} tabLabel={translate('Chi tiết doanh thu')}><DetailRevenue translate={translate} /></View>
        <View style={styles.revenue} tabLabel={translate('Đại lý cấp 2')}><AgencyPage translate={translate} /></View>
        <View style={styles.revenue} tabLabel={translate('Lich sử')}><History translate={translate} /></View>
      </ScrollableTabView>
    )
  }

  render() {
    const { translate } = this.props
    return (
      <View style={styles.reportContainer}>
        <View style={styles.topBarStyle}>
          {this.renderTopButton(translate)}
        </View>
      </View>
    )
  }
}
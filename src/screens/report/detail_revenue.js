import React, { Component } from 'react'
import {
  View, TouchableOpacity, Image, FlatList 
} from 'react-native'
import { Calendar } from 'react-native-calendars'

import { Themes, TextFont } from '../../ui'
import styles from './report.style'
import styless from '../setting/setting.style'
import appSymbol from '../../assets/symbols'
import AppImages from '../../assets/images'
import { Icon, ModalDropdownPicker } from '../../components'

export default class DetailRevenue extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showCalendarStartDay: false,
      showCalendarEndDay: false,
      valueStartDay: '',
      valueEndDay: '',
      selectedValue: '',
      jsonData: [
        {
          maGD: '25452522',
          time: '26/06/2018 12:47:43',
          nameAccount: '09738131341',
          content: 'Nap tien vao so 0124563124583',
          money: -10000,
          lastMoney: 145452
        },
        {
          maGD: '25452522',
          time: '26/06/2018 12:47:43',
          nameAccount: '09738131341',
          content: 'Nap tien vao so 0124563124583',
          money: 10000,
          lastMoney: 145452
        },
        {
          maGD: '25452522',
          time: '26/06/2018 12:47:43',
          nameAccount: '09738131341',
          content: 'Nap tien vao so 0124563124583',
          money: -10000,
          lastMoney: 145452
        },
        {
          maGD: '25452522',
          time: '26/06/2018 12:47:43',
          nameAccount: '09738131341',
          content: 'Nap tien vao so 0124563124583',
          money: 10000,
          lastMoney: 145452
        }
      ],
      dataKind: ['basic', 'advance', 'super advance']
    }
    this.day = new Date().getDay()
    this.month = new Date().getMonth() + 1
    this.year = new Date().getFullYear()
  }

  renderDay = (translate) => {
    const {
      showCalendarStartDay, showCalendarEndDay, valueStartDay, valueEndDay 
    } = this.state
    return (
      <View style={{ paddingHorizontal: Themes.Metrics.basePadding }}>
        <View style={styles.calendarContainer}>
          <View style={styles.startDay}>
            <TextFont style={styles.lableDay} font="LatoRegular" size={12}>
              {translate('TỪ NGÀY')}
            </TextFont>
            <View style={styles.calendarStyle}>
              <TextFont style={styles.valueDay}>{valueStartDay !== '' ? valueStartDay.dateString : `${this.day}/${this.month}/${this.year}`}</TextFont>
              <TouchableOpacity
                style={styles.calendar}
                onPress={() => {
                  this.setState({ showCalendarStartDay: !showCalendarStartDay, showCalendarEndDay: false })
                }}
              >
                <Icon name="icon_date" size={10} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.endDay}>
            <TextFont style={styles.lableDay} font="LatoRegular" size={12}>
              {translate('ĐẾN NGÀY')}
            </TextFont>
            <View style={styles.calendarStyle}>
              <TextFont style={styles.valueDay}>{valueEndDay !== '' ? valueEndDay.dateString : `${this.day}/${this.month}/${this.year}`}</TextFont>
              <TouchableOpacity
                style={styles.calendar}
                onPress={() => {
                  this.setState({ showCalendarEndDay: !showCalendarEndDay, showCalendarStartDay: false })
                }}
              >
                <Icon name="icon_date" size={10} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <ModalDropdownPicker
          onChangeValue={item => this.setState({ selectedValue: item })}
          selectedValue={this.state.selectedValue}
          data={this.state.dataKind ? this.state.dataKind : null}
          style={{ width: '100%', marginTop: Themes.Metrics.baseMargin }}
          placeholderText={translate('Loại :')}
          addonRight={translate('Tất cả')}
        />
        {this.renderButton(translate)}
      </View>
    )
  }

  onShowCalendar = () => {
    const { showCalendarStartDay, showCalendarEndDay } = this.state
    return (
      <Calendar
        current="2019-01-01"
        onDayPress={(day) => {
          if (showCalendarStartDay) {
            this.setState({ valueStartDay: day })
          }
          if (showCalendarEndDay) {
            this.setState({ valueEndDay: day })
          }
        }}
        style={[styles.customCalendar, showCalendarStartDay ? { left: 10 } : { right: 10 }]}
      />
    )
  }

  renderButton = (translate) => {
    return (
      <TouchableOpacity style={styles.submitButton}>
        <TextFont font="LatoRegular" size={12} color={Themes.Colors.white}>
          {translate('Xác nhận')}
        </TextFont>
      </TouchableOpacity>
    )
  }

  onDetail = (item) => {
    console.log(item.content)
  }

  renderListData = () => {
    return (
      <FlatList
        data={this.state.jsonData}
        renderItem={({ item }) => {
          return (
            <View style={styles.everyItem}>
              <View style={styles.itemLeft}>
                <View>
                  <TextFont font="LatoRegular" size={12} style={styles.item}>
                    {item.content}
                  </TextFont>
                  <TextFont font="LatoRegular" size={12} style={styles.time}>
                    {item.time}
                  </TextFont>
                </View>
              </View>
              <View style={styless.itemRight}>
                <TextFont font="LatoRegular" size={12} style={[styles.mkStyle]}>
                  {item.money}
                </TextFont>
                <TouchableOpacity onPress={this.onDetail(item)}>
                  <Icon name="icon_arrow" size={10} style={styles.iconArrow} />
                </TouchableOpacity>
              </View>
            </View>
          )
        }}
      />
    )
  }

  render() {
    const { translate } = this.props
    const {
      showCalendarStartDay, showCalendarEndDay, valueStartDay, valueEndDay 
    } = this.state
    return (
      <View style={{ flex: 1 }}>
        <View style={[styles.reportContainer, showCalendarStartDay || showCalendarEndDay ? { opacity: 0.5 } : { opacity: 1 }]}>
          {this.renderDay(translate)}
          <View style={styles.lableStyle}>
            <TextFont font="LatoRegular" size={15}>
              {translate('Tên sản phẩm')}
            </TextFont>
            <TextFont font="LatoRegular" size={15}>
              {translate('Doanh thu')}
            </TextFont>
          </View>
          <View style={{ flex: 1 }}>{this.renderListData()}</View>
        </View>
        {showCalendarStartDay || showCalendarEndDay ? this.onShowCalendar() : null}
      </View>
    )
  }
}

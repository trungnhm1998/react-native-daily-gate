import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Report from './report'

class ReportContainer extends Component {
  componentWillMount() {
    // alert('mounted')
  }

  render() {
    const { t: translate } = this.context
    const { i18nState } = this.props
    return (
      <Report
        translate={translate}
        i18nState={i18nState}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReportContainer)

ReportContainer.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}

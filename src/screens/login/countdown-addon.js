import React, { Component } from 'react'
import { Text, View } from 'react-native'

export default class CountDownText extends Component {
  constructor(props) {
    super(props)
    this.state = { time: 60 }
  }

  componentDidMount() {
    let countdown
    countdown = setInterval(() => {
      this.setState((prevState) => {
        if (prevState.time === 0) {
          clearInterval(countdown)
          return prevState
        }
        return { time: prevState.time - 1 }
      })
    }, 1000)
  }

  render() {
    return (
      <Text> 
   
        {this.state.time}
    
      </Text>
    )
  }
}

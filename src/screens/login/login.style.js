import { StyleSheet } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  loginBg: {
    flex: 1,
    flexDirection: 'column',
    width: Themes.Metrics.screenWidth,
    backgroundColor: 'transparent',
    paddingHorizontal: Themes.Metrics.paddingHorizontal,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 35 * Themes.Metrics.ratioScreen
  },
  loginLogo: {
    marginBottom: 40
  },
  greatingTextContainer: {
    alignSelf: 'center',
    height: 70,
    marginBottom: 20
  },
  greatingText_1: {
    opacity: 0.4,

    alignSelf: 'center'
  },
  greatingText_2: {
    alignSelf: 'center'
  },
  formContainer: {
    alignSelf: 'center',
    flexDirection: 'column',
    backgroundColor: Themes.Colors.white,
    height: 350,
    width: '100%',
    alignItems: 'center',
    padding: 25,
    borderRadius: Themes.Metrics.borderRadius
  },
  formContainer_loged_before: {
    alignSelf: 'center',
    flexDirection: 'column',
    backgroundColor: Themes.Colors.white,
    height: 400,
    width: '100%',
    alignItems: 'center',
    padding: 25,
    borderRadius: Themes.Metrics.borderRadius
  },

  formOption: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%'
  },
  loginButton: {
    width: '100%',
    marginTop: 20
  },
  fingerprintButton: {
    width: '100%',
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  footerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: 5,
    marginTop: 15,
    width: '100%'
  },
  btnTouch: {
    backgroundColor: 'red',
    height: 100,
    width: Themes.Metrics.screenWidth
  },
  loginContentTop: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  txtForgetPass: {
    textDecorationLine: 'underline',
    alignSelf: 'center',
    marginTop: 20
  },
  txtBottom: {
    position: 'absolute',
    bottom: 10,
    left: 0,
    right: 0,
    textAlign: 'center',
    color: '#8b8b8b'
  }
})

export default styles

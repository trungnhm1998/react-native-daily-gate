import React, { Component } from 'react'
import {
  Text, View, Keyboard, Animated, Image, KeyboardAvoidingView, TouchableOpacity, ScrollView, Platform 
} from 'react-native'

import FingerprintScanner from 'react-native-fingerprint-scanner'

import {
  TcombForm, Button, Icon, MaterialButton, FingerprintPopup 
} from '../../components'
import CountDownText from './countdown-addon'
import { TextFont, Themes } from '../../ui'
import appSymbol from '../../assets/symbols'

import styles from './login.style'

const t = require('tcomb-form-native')
const Form = t.form.Form
const userAccount = t.struct({
  phone: t.String,
  password: t.String
})

export default class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: {
        phone: '',
        password: ''
      },
      hidePassword: true,
      rememberPassword: true,
      showWelcome: true,
      showFingerPrintButton: true,
      fingerPopupShowed: false
    }
    this.onChange = this.onChange.bind(this)
  }

  componentWillMount() {
    if (Platform.OS === 'ios') {
      this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow)
      this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide)
    } else {
      this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow)
      this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide)
    }
  }

  componentDidMount() {
    FingerprintScanner.isSensorAvailable().catch(error => this.setState({ showFingerPrintButton: false }))
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove()
    this.keyboardWillHideSub.remove()
  }

  handleFingerprintShowed = () => {
    this.setState({ fingerPopupShowed: true })
  }

  handleFingerprintDismissed = () => {
    this.setState({ fingerPopupShowed: false })
  }

  keyboardWillShow = (event) => {

    this.setState({ showWelcome: false })
  }

  keyboardWillHide = (event) => {
    this.setState({ showWelcome: true })
  }

  onChange(value) {
    this.setState({ value })
  }

  toogleRememberPassword = () => {
    this.setState(prevState => ({
      rememberPassword: !prevState.rememberPassword
    }))
  }

  renderRightAddonPasswordInput = () => {
    return (
      <TouchableOpacity
        onPress={() => this.setState((prevState) => {
          return { hidePassword: !prevState.hidePassword }
        })
        }
      >
        <Image source={appSymbol.icon_input_eye_green} />
      </TouchableOpacity>
    )
  }

  renderForm = () => {
    const layout = (locals) => {
      return (
        <View style={{ marginTop: 10, width: '100%' }}>
          {locals.inputs.phone}
          {locals.inputs.password}
        </View>
      )
    }

    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        phone: {
          label: 'Số điện thoại',
          keyboardType: 'phone-pad',
          template: TcombForm.InputFLoatingLabel_tcomb,
          placeholder: '0904143147',
          placeholderTextColor: '#E7E7E7',
          keyboardType: 'phone-pad',
          config: {}
        },
        password: {
          label: 'Mật khẩu',
          template: TcombForm.InputFLoatingLabel_tcomb,
          placeholderTextColor: '#E7E7E7',
          secureTextEntry: this.state.hidePassword,
          config: {
            rightAddon: this.renderRightAddonPasswordInput(),
            secureTextEntry: true,
            labelStyle: {
              marginLeft: 0,
              color: '#222424',
              textAlign: 'left'
            },
            containerStyle: {
              marginTop: 5
            }
          }
        }
      }
    }
    return (
      <Form
        ref={(form) => {
          this.form = form
        }}
        onChange={this.onChange}
        value={this.state.value}
        type={userAccount}
        options={options}
      />
    )
  }

  renderFormOption = () => {
    const { translate } = this.props
    return (
      <View style={styles.formOption}>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity onPress={this.toogleRememberPassword}>
            <Image source={this.state.rememberPassword ? appSymbol.icon_check_act : appSymbol.icon_check_nor} />
          </TouchableOpacity>

          <TextFont font="LatoLight" size={12} color={Themes.Colors.grey}>
            {translate(' Nhớ mật khẩu')}
          </TextFont>
        </View>
        <View>
          <TouchableOpacity onPress={this.props.onPressForgotPassword}>
            <TextFont font="LatoLight" size={12} color={Themes.Colors.lightBlue}>
              {translate('Quên mật khẩu?')}
            </TextFont>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderWelcomeText = (userLoggedBefore) => {
    if (!this.state.showWelcome) return
    const { translate } = this.props
    const { gateReducer } = this.props
    const defaultWelcome = (
      <View style={styles.greatingTextContainer}>
        <TextFont font="LatoRegular" color={Themes.Colors.lightGrey} style={styles.greatingText_1} size={18}>
          {translate('PAYMENT GATE')}
        </TextFont>
        <TextFont font="LatoRegular" color={Themes.Colors.white} style={styles.greatingText_2} size={20}>
          {translate('Xin Chào!')}
        </TextFont>
      </View>
    )

    if (!userLoggedBefore) return defaultWelcome
    return (
      <View style={styles.greatingTextContainer}>
        <TextFont font="LatoRegular" color={Themes.Colors.lightGrey} style={styles.greatingText_1} size={18}>
          {translate('Xin Chào!')}
        </TextFont>
        <TextFont font="LatoRegular" color={Themes.Colors.white} style={styles.greatingText_2} size={20}>
          {gateReducer.userInfo.displayName ? gateReducer.userInfo.displayName : gateReducer.userInfo.phone}
        </TextFont>
      </View>
    )
  }

  renderFingerPrintPopup = () => {
    if (this.state.fingerPopupShowed) {
      return (
        <FingerprintPopup
          onMatchFingerPrint={this.props.onFingerprintLogin}
          handlePopupDismissed={this.handleFingerprintDismissed}
          visible={this.state.fingerPopupShowed}
        />
      )
    }
    return null
  }

  onPressLogin = () => {
    const { value } = this.state
    if (value) this.props.onPressLogin(value.phone, value.password)
  }

  render() {
    const { translate } = this.props
    const { gateReducer } = this.props
    let userLoggedBefore = false
    if (gateReducer.userInfo) {
      if (gateReducer.userInfo.displayName || gateReducer.userInfo.phone) {
        userLoggedBefore = true
      }
    }
  

    // vi heigh của form cố định, nên sẽ có 2 tuỳ chọn render, nó như user đã login trc đây và thiết bị có sử dụng fingerprint thì render form vs chiều dài đài hơn để chưa được button fingerprint
    return (
      <ScrollView style={{ flex: 1 }} scrollEnabled={false}>
        <View style={styles.loginBg}>
          <Icon name="logo-white-1" color="#fff" size={25} style={styles.loginLogo} />

          {this.renderWelcomeText(userLoggedBefore)}

          <View style={userLoggedBefore && this.state.showFingerPrintButton ? styles.formContainer_loged_before : styles.formContainer}>
            <TextFont font="LatoBold" color={Themes.Colors.grey} size={15}>
              {translate('ĐĂNG NHẬP')}
            </TextFont>
            {this.renderForm()}
            {this.renderFormOption()}
            {this.renderFingerPrintPopup()}
            <MaterialButton textFont="LatoBold" textSize={15} style={styles.loginButton} onPress={this.onPressLogin}>
              {translate('ĐĂNG NHẬP')}
            </MaterialButton>

            {userLoggedBefore && this.state.showFingerPrintButton ? (
              <MaterialButton
                textFont="LatoBold"
                textSize={15}
                style={styles.fingerprintButton}
                textStyle={{ color: Themes.Colors.red }}
                btnColor={Themes.Colors.white}
                onPress={this.handleFingerprintShowed}
              >
                <Icon name="icon_fingerprint" color={Themes.Colors.red} size={25} />

                {translate('  MỞ BẰNG KHOÁ VÂN TAY')}
              </MaterialButton>
            ) : null}
          </View>
          <View style={styles.footerContainer}>
            <View
              style={{
                flexDirection: 'row',
                height: 30,
                justifyContent: 'center',
                alignItem: 'center',
                width: '100%'
              }}
            >
              <TextFont style={{ alignSelf: 'center' }} font="LatoRegular" size={12} color={Themes.Colors.white}>
                {translate('Nếu bạn chưa có tài khoản ')}
              </TextFont>
              <TouchableOpacity style={{ alignSelf: 'center' }} onPress={this.props.onPressSignUp}>
                <TextFont font="LatoBold" size={14} color={Themes.Colors.white} style={{ textDecorationLine: 'underline' }}>
                  {translate('ĐĂNG KÝ NGAY')}
                </TextFont>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    )
  }
}

// Login.defaultProps = {
//   payload: {
//     accountInfo: {
//       username: ' NGUYEN VAN DAU'
//     }
//   }
// }

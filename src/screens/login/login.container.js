import React, { Component } from 'react'
import { View, Button } from 'react-native'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import DeviceInfo from 'react-native-device-info'
import { Navigation } from 'react-native-navigation'
import Semver from 'semver'
import { bindActionCreators } from 'redux'
import Login from './login'
import { ScreenConfigs, AppNavigation } from '../../navigation'
import { Themes } from '../../ui'
import StatusBarAndroid from '../../components/statusBar'
import { Modal } from '../../components'
import { gateActions } from '../../redux/daily-gate'
import AppConstants from '../../configs/constant.config';
class LoginContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modalVisible: false,
      modalPriority: ''
    }
  }

  componentDidMount() {
    // console.log()
    try {
      const { latestVersion, forceUpdate, update } = this.props.gateReducer
      const appVersion = DeviceInfo.getVersion()
      if (appVersion && latestVersion) {
        if (Semver.gt(latestVersion, appVersion)) {
          if (update || forceUpdate) {
            // TODO: check and set state to show modal here
            this.setState({
              modalVisible: true,
              modalPriority: update ? 'update' : 'forceUpdate'
            })
          }
        }
      }
    } catch (error) {
      alert(`Something went wrong ${error}`)
    }
  }

  onPressSignUp = () => {
    const { t: translate } = this.context
    const passProps = {
      signupState: AppConstants.SignupType.SIGN_UP
    }
    Navigation.push(this.props.componentId, AppNavigation.screen.signup(passProps, translate))
  }

  onPressForgotPassword = () => {
    // nav to forgot pass screen
    const { t: translate } = this.context
    const passProps = {
      signupState: AppConstants.SignupType.FORGOT_PASSWORD,
      navToSignup: () => this.onPressSignUp()
    }
    Navigation.push(this.props.componentId, AppNavigation.screen.signup(passProps, translate))
  }

  onPressLogin = (phone, pass) => {
    this.props.gateAction.authorizeUser(phone, pass, {
      nextScreen: () => AppNavigation.navToHome()
    })
  }

  // if user MATCH FINGERPRINT, then call this function to login
  onFingerprintLogin = () => {
    this.props.gateAction.authorizeFingerprint('00', 'st', {
      nextScreen: () => AppNavigation.navToHome()
    })
  }

  closeModal = () => {
    this.setState({ modalVisible: false })
  }

  renderModalFooter = () => {
    return (
      <View style={{}}>
        <Button title="Update" onPress={() => this.closeModal()} />
      </View>
    )
  }

  render() {
    const { t: translate } = this.context
    const { i18nState, gateReducer } = this.props
    return (
      <View style={{ flex: 1 }}>
        <StatusBarAndroid translucent navColor="rgba(0, 0, 0, 0)" />
        <Login
          onPressForgotPassword={this.onPressForgotPassword}
          onPressSignUp={this.onPressSignUp}
          onPressLogin={this.onPressLogin}
          onFingerprintLogin={this.onFingerprintLogin}
          translate={translate}
          i18nState={i18nState}
          gateReducer={gateReducer}
          APIRespond={gateReducer.APIRespond}
        />
        <Modal
          title={this.state.modalPriority === 'update' ? 'You need to update' : 'You must update'}
          visible={this.state.modalVisible}
          onClose={() => this.closeModal()}
          footer={this.renderModalFooter()}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState,
    gateReducer: state.gateReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    gateAction: bindActionCreators(gateActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginContainer)

LoginContainer.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}

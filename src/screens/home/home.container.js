import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import TimerMixin from 'react-timer-mixin'
import { Navigation } from 'react-native-navigation'
import Home from './home'
import { gateActions } from '../../redux/daily-gate'
import { ScreenConfigs } from '../../navigation'
import { Themes } from '../../ui'
class HomeContainer extends Component {
  constructor(props) {
    super(props)
    this.bottomTabEventListener = Navigation.events().registerBottomTabSelectedListener(({ selectedTabIndex, unselectedTabIndex }) => Navigation.popToRoot(this.props.componentId))
  }

  onTransfers = () => {
    console.log('onTransfers')
  }

  getBannerList = () => {
    const { getBanners } = this.props.gateAction
    getBanners()
  }

  getInforOptionList = () => {
    const { getInforOption } = this.props.gateAction
    getInforOption()
  }

  renewAccessTokenTimer = () => {
    const { content } = this.props.gateReducer
    const { renewAccessTokenInterval } = content
    TimerMixin.setInterval(() => {
      const { renewAccessToken } = this.props.gateAction
      renewAccessToken()
    }, renewAccessTokenInterval)
  }

  componentDidMount() {
    this.getBannerList()
    this.renewAccessTokenTimer()
    this.getInforOptionList()
  }

  onNoticeButtonPress = () => {
    const { t: translate } = this.context
    Navigation.push(this.props.componentId, {
      component: {
        name: ScreenConfigs.NOTICE,
        options: {
          topBar: {
            visible: true,
            drawBehind: false,
            // animate: false,
            backButton: {
              color: Themes.Colors.white
            },
            background: {
              color: Themes.Colors.topBarColor,
              translucent: false
            },
            title: {
              text: translate('Thông báo'),
              color: Themes.Colors.white
            }
          }
        }
      }
    })
  }

  onPressDepositButton = () => {
    const { t: translate } = this.context
    Navigation.push(this.props.componentId, {
      component: {
        name: ScreenConfigs.LIST_DEPOSIT,
        options: {
          topBar: {
            visible: true,
            drawBehind: false,
            // animate: false,
            backButton: {
              color: Themes.Colors.white
            },
            background: {
              color: Themes.Colors.topBarColor,
              translucent: false
            },
            title: {
              text: translate('Nạp tiền mặt'),
              color: Themes.Colors.white
            }
          }
        }
      }
    })
  }

  render() {
    const { t: translate } = this.context
    const { i18nState, gateReducer } = this.props
    return (
      <Home
        onRecharge={this.onRecharge}
        onTransfers={this.onTransfers}
        translate={translate}
        i18nState={i18nState}
        gateReducer={gateReducer}
        onNoticeButtonPress={this.onNoticeButtonPress}
        onPressDepositButton={this.onPressDepositButton}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState,
    gateReducer: state.gateReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    gateAction: bindActionCreators(gateActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeContainer)

HomeContainer.contextTypes = {
  t: PropTypes.func
}

import React, { Component } from 'react'
import {
  View, TouchableOpacity, Text, Image, ImageBackground, Dimensions, FlatList, SafeAreaView
} from 'react-native'
import numeral from 'numeral'
import { Themes, TextFont } from '../../ui'
import styles from './home.style'
import appSymbol from '../../assets/symbols'
import AppImages from '../../assets/images'
import { Icon } from '../../components'

export default class Home extends Component {  
  renderHeaderImage() {
    const imageContainerStyle = {
      position: 'absolute',
      top: -3,
      left: 0,
      // backgroundColor: 'red',
      height: '40%',
      width: Dimensions.get('window').width
    }
    return (
      <View style={imageContainerStyle}>
        <Image
          source={AppImages.bgHeader}
          resizeMethod="resize"
          resizeMode="stretch"
          style={{
            flex: 1,
            width: undefined,
            height: undefined
          }}
        />
      </View>
    )
  }

  renderBannerItem = (data) => {
    const { adsBannerList } = this.props.gateReducer.payload
    const { item, index } = data
    const paddingRatio = (Themes.Metrics.paddingHorizontal - (3 * Themes.Metrics.ratioScreen)) * Themes.Metrics.ratioScreen
    const bannerContainer = {
      paddingLeft: index === 0 ? paddingRatio : 0,
      paddingRight: index === adsBannerList.length - 1 ? paddingRatio : Themes.Metrics.smallMargin,
      // width: '100%',
      height: '100%'
    }
    return (
      <View
        key={item.index}
        style={[
          Themes.styleGB.materialStyle,
          bannerContainer
        ]}
      >
        <Image
          resizeMode="stretch"
          source={item.bannerSrc}
          style={styles.Banner}
        />
      </View>
    )
  }

  renderAdsBannerList = () => {
    if (!this.props.gateReducer.payload) return null
    const { adsBannerList } = this.props.gateReducer.payload

    return (
      <View style={styles.BannerListContainer}>
        <FlatList
          // style={{ marginLeft: 8 }}
          showsHorizontalScrollIndicator={false}
          data={adsBannerList}
          horizontal
          keyExtractor={(item, index) => item.id}
          renderItem={data => this.renderBannerItem(data)}
        />
      </View>
    )
  }

  renderButton = (translate) => {
    return (
      <View style={styles.ButtonHeader}>
        <View style={styles.RowButton}>
          <TouchableOpacity style={styles.Buttonstyle} onPress={this.props.onPressDepositButton}>
            <Image source={appSymbol.icon_home_nap} style={styles.ImageStyle} />
            <Text>{translate('NẠP TIỀN')}</Text>
          </TouchableOpacity>
          <View style={styles.Hr} />
          <TouchableOpacity style={styles.Buttonstyle} onPress={this.props.onTransfers}>
            <Image source={appSymbol.icon_home_chuyen} style={styles.ImageStyle} />
            <Text>{translate('CHUYỂN TIỀN')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }


  renderBalanceStatus = (translate) => {
    return (
      <View style={styles.BalanceContainer}>
        <View style={styles.BalanceTitle}>
          <TextFont
            font="LatoRegular"
            style={{
              color: 'rgba(255, 255, 255, 0.4)'
            }}
          >
            {translate('SỐ DƯ')}
          </TextFont>
        </View>
        <View style={styles.BalanceStatusContainer}>
          <View style={styles.StatusContainer}>
            <TextFont
              font="LatoBold"
              style={styles.TextMoney}
            >
              {numeral(121212122).format('0,0')}
            </TextFont>
            <TextFont
              font="LatoRegular"
              style={styles.VND}
            >
              {translate('VNĐ')}
            </TextFont>
          </View>
        </View>
        
      </View>
    )
  }

  renderTopBar = () => {
    return (
      <View style={styles.topbar}>
        <View style={styles.topbarItemLeft}>
          <Image source={AppImages.avatarDemo} />
        </View>
        <View style={styles.topbarItemCenter}>
          <Icon name="logo-white-1" color="#fff" size={20} />
        </View>

        <View style={styles.topbarItemRight}>
          <TouchableOpacity onPress={this.props.onNoticeButtonPress}>
            <Image source={appSymbol.icon_noti_act} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  formatData = (data) => {
    const numColumns = 3
    const numberOfFullRows = Math.floor(data.length / numColumns)

    let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns)
    while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
      data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true })
      numberOfElementsLastRow += 1
    }
    return data
  }

  renderServiceItem = ({ item, index }) => {
    // check if the current service item is the center item of 3 col
    // get row first
    const rowNum = Math.round(index / 3) // return 1 if index [3, 4, 5]
    let margin = 0
    if (index === (rowNum * 3) + 1) {
      // is middle item
      margin = 20
    }
    
    if (item.empty === true) {
      return <View style={[styles.tempEmpty, { backgroundColor: 'transparent', marginLeft: ((index % 3) === 1) ? 20 : 0 }]} />
    }
    return (
      <TouchableOpacity
        style={[styles.temp, { marginHorizontal: margin }]}
      >
        <View style={styles.iconStyle}>
          <Image 
            source={item.nameIcon}
            resizeMethod="resize"
          />
        </View>
        <TextFont font="LatoRegular" size={12} style={styles.textTemp}>{item.title}</TextFont>
      </TouchableOpacity>
    )
  }

  renderServices() {
    const { someItem } = this.props.gateReducer
    if (someItem) {
      return (
        <FlatList
          data={this.formatData(someItem)}
          renderItem={this.renderServiceItem}
          numColumns={3}
          keyExtractor={(item, index) => item.title}
          showsVerticalScrollIndicator={false}
          style={{
            width: '100%',
            height: '100%',
            paddingHorizontal: Themes.Metrics.paddingHorizontal * Themes.Metrics.ratioScreen
          }}
        />
      )
    }
    return <View />
  }

  render() {
    const { translate } = this.props
    return (
      <View style={styles.HomeLogo}>
        {this.renderHeaderImage()}
        <View style={styles.HotSectionContainer}>
          {this.renderTopBar()}
          {this.renderBalanceStatus(translate)}
          {this.renderButton(translate)}
        </View>
        {this.renderAdsBannerList()}
        {this.renderServices()}
      </View>
    )
  }
}
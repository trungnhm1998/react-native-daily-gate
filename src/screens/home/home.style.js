import { StyleSheet, Dimensions } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  HomeLogo: {
    flex: 1,
    backgroundColor: '#F4F5FA',
    alignItems: 'center',
    paddingTop: Themes.Metrics.paddingStatusBar * Themes.Metrics.ratioScreen,
    // paddingHorizontal: Themes.Metrics.paddingHorizontal * Themes.Metrics.ratioScreen
  },
  HotSectionContainer: {
    paddingHorizontal: Themes.Metrics.paddingHorizontal * Themes.Metrics.ratioScreen
  },
  ButtonHeader: {
    height: Themes.Metrics.buttonHeightHome,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Themes.Colors.white,
    borderRadius: Themes.Metrics.buttonRadius,
    borderWidth: 1,
    borderColor: Themes.Colors.white,
    ...Themes.styleGB.materialStyle,
    marginBottom: Themes.Metrics.baseMargin,
  },
  RowButton: {
    flexDirection: 'row'
  },
  Buttonstyle: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  Hr: {
    borderLeftWidth: 0.6,
    borderColor: Themes.Colors.gray
  },
  ImageStyle: {
    marginBottom: Themes.Metrics.baseMargin
  },
  BalanceContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: Themes.Metrics.marginVertical
  },
  BalanceTitle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    color: Themes.Colors.lightGrey
  },
  TextMoney: {
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: Themes.Fonts.size.h4,
    color: Themes.Colors.white
  },
  BalanceStatusContainer: {
    flex: 1,
    // flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  StatusContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  VND: {
    fontSize: Themes.Fonts.size.h5,
    color: 'rgba(255, 255, 255, 0.6)',
    paddingLeft: 10
  },
  Banner: {
    // height: 90 * Themes.Metrics.ratioScreen,
    width: ((Dimensions.get('window').width - Themes.Metrics.paddingHorizontal * 2) * Themes.Metrics.ratioScreen) + 6,
  },
  BannerListContainer: {
    height: 100 * Themes.Metrics.ratioScreen,
    // paddingLeft: Themes.Metrics.marginVertical
  },
  topbar: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginBottom: Themes.Metrics.baseMargin
  },
  topbarItemLeft: {
    flex: 1,
    alignItems: 'flex-start'
  },
  topbarItemRight: {
    flex: 1,
    alignItems: 'flex-end'
  },
  topbarItemCenter: {
    flex: 1,
    alignItems: 'center'
  },
  temp: {
    backgroundColor: Themes.Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    height: 100, // approximate a square,
    marginBottom: 20,
    ...Themes.styleGB.materialStyle,
    borderColor: Themes.Colors.gray,
    borderRadius: 10
  },
  tempEmpty: {
    backgroundColor: Themes.Colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    height: 100, // approximate a square,
    marginBottom: 20,
  },
  textTemp: {
    color: Themes.Colors.gray,
    padding: Themes.Metrics.smallPadding
  },
  iconStyle: {
    
    paddingBottom: Themes.Metrics.smallPadding
  }
})

export default styles

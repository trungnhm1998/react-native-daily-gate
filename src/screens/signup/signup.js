import React, { Component } from 'react'
import {
  View, StyleSheet,
  Image,
  ScrollView,
} from 'react-native'
import { Themes, TextFont } from '../../ui'
import { TcombForm, MaterialButton } from '../../components'
import appSymbols from '../../assets/symbols'
import AppResponseCode from '../../configs/response-code.config'
import AppConstants from '../../configs/constant.config'
import AppHelper from '../../helpers';

const tcomb = require('tcomb-form-native')
const { Form } = tcomb.form

const userAccount = tcomb.struct({
  phoneNumber: tcomb.String
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: Themes.Metrics.paddingHorizontal,
    paddingTop: Themes.Metrics.paddingVertical * Themes.Metrics.ratioScreen,
  },
  title: {
    textTransform: 'uppercase',
    marginTop: 20
    // marginLeft: 10
  },
  alreadyHaveAccount: {
    marginTop: 20,
    textDecorationLine: 'underline'
  }
})

/**
 * regex
 * (09|05|03|01|07|08|[1|2|3|4|5|6|7|8|9])+([0-9]{8})\b
 */

export default class Signup extends Component {
  constructor(props) {
    super(props)
    // form value to validate phoneNumber
    this.value = {
      phoneNumberCorrect: false
    }

    this.state = {
      value: {}
    }
  }

  onChange(value) {
    // console.log('value', this.value)
    const { phoneNumber } = value

    // default value will always false
    this.value.phoneNumberCorrect = false
    if (phoneNumber.length === 10) {
      const regex = AppHelper.Regex.phone
      const result = phoneNumber.match(regex)
      if (result) {
        this.value.phoneNumberCorrect = true
      }
    }

    this.setState({
      value
    })
  }

  renderPhoneNumberValidate = (hasError) => {
    // check phoneNumber format then render the tick icon
    if (this.value.phoneNumberCorrect) {
      // return <Text>yes</Text>
      if (hasError) {
        return <Image source={appSymbols.icon_input_error} />
      }
      return <Image source={appSymbols.icon_input_check} />
    }
    return null
  }

  renderForm(translate) {
    const layout = (locals) => {
      return (
        <View style={{ marginTop: 15 }}>
          <View>{locals.inputs.phoneNumber}</View>
        </View>
      )
    }

    const { apiReducer, signupState } = this.props
    let hasError = false
    let error = ''
    if (signupState === AppConstants.SignupType.SIGN_UP
        && apiReducer.code === AppResponseCode.PHONE_NUMBER_EXISTS) {
      hasError = true
      error = translate('Số điện thoại đã tồn tại.')
    } else if (signupState === AppConstants.SignupType.FORGOT_PASSWORD
        && apiReducer.code === AppResponseCode.PHONE_NUMBER_VALID) {
      hasError = true
      const errorStyle = {
        fontSize: Themes.Fonts.size.extraSmall,
        marginLeft: Themes.Metrics.baseMargin,
        textAlign: 'left'
      }
      error = () => {
        return (
          <TextFont style={errorStyle} color={Themes.Colors.redText}>
            {translate('Số điện thoại không tồn tại. Sẽ chuyển qua đăng ký mới trong vòng ')}
            <TextFont font="LatoBold" style={errorStyle} color={Themes.Colors.greenText}>
              5s
            </TextFont>
            !
          </TextFont>
        )
      }
    }

    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        phoneNumber: {
          template: TcombForm.InputFLoatingLabel_tcomb,
          label: translate('Số điện thoại'),
          keyboardType: 'phone-pad',
          config: {
            rightAddon: this.renderPhoneNumberValidate(hasError)
          },
          hasError,
          error
          // hasError: true,
          // error: 'test'
        }
      }
    }

    return (
      <Form
        ref={(form) => {
          this.form = form
        }}
        type={userAccount}
        options={options}
        onChange={value => this.onChange(value)}
        value={this.state.value}
      />
    )
  }

  submitPhoneNumber = () => {
    const { phoneNumber } = this.state.value
    const { phoneNumberCorrect } = this.value
    const { submitPhoneNumber } = this.props
    if (phoneNumberCorrect) {
      submitPhoneNumber(phoneNumber)
    }
  }

  renderSubmitButton(translate) {
    return (
      <MaterialButton onPress={() => this.submitPhoneNumber()}>
        {translate('TIẾP TỤC')}
      </MaterialButton>
    )
  }

  renderCaptcha() {
    // TODO: Implement captcha here
    return (
      <View style={{ height: 30, backgroundColor: 'red' }}>
      </View>
    )
  }

  render() {
    const { translate } = this.props
    return (
      <ScrollView style={styles.container} scrollEnabled={false}>
        {this.renderForm(translate)}
        {this.renderSubmitButton(translate)}
      </ScrollView>
    )
  }
}

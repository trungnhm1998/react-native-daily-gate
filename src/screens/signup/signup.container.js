import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'
import { Navigation } from 'react-native-navigation'
import TimerMixin from 'react-timer-mixin'
import Signup from './signup'
import { gateActions } from '../../redux/daily-gate'
import { ScreenConfigs, AppNavigation } from '../../navigation'
import { Themes } from '../../ui'
import AppConstants from '../../configs/constant.config'
import { apiActions } from '../../redux/api'

class SignupContainer extends Component {
  constructor(props) {
    super(props)
    // use this for reuse UI for forgot password and signup
  }

  componentWillMount() {
    // alert('mounted')
  }

  onHaveAccountPressed() {
    Navigation.popToRoot(this.props.componentId)
  }

  navToSignupPassword = (phoneNumber, signupState) => {
    const { t: translate } = this.context
    Navigation.push(this.props.componentId, {
      component: {
        name: ScreenConfigs.SIGNUP_PASSWORD,
        passProps: {
          phoneNumber,
          signupState
        },
        options: {
          statusBar: {
            drawBehind: false,
            visible: true
          },
          topBar: {
            visible: true,
            drawBehind: false,
            // animate: false,
            backButton: {
              visible: false,
              color: Themes.Colors.white,
              showTitle: false
            },
            background: {
              color: Themes.Colors.topBarColor,
              translucent: false
            },
            title: {
              text: signupState === AppConstants.SignupType.SIGN_UP ? translate('Đăng ký') : translate('Quên mật khẩu'),
              color: Themes.Colors.white
            }
          }
        }
      }
    })
  }

  submitPhoneNumber = (phoneNumber) => {
    // console.log('submitPhone', phoneNumber)
    // alert('test', phoneNumber)
    const {
      signupState, navToSignup, apiAction
    } = this.props
    const { checkAccountExist } = this.props.gateAction
    const { t: translate } = this.context

    const navigator = {
      navToSignupPassword: () => this.navToSignupPassword(phoneNumber, signupState),
      navToSignup: () => {
        TimerMixin.setTimeout(() => {
          Navigation.pop(this.props.componentId)
          navToSignup()
          apiAction.clearResponse()
        }, AppConstants.AutoNavInterval.Signup)
      }
    }
    checkAccountExist(phoneNumber, signupState, navigator)
  }

  render() {
    const { t: translate } = this.context
    const {
      i18nState, signupState, apiReducer, gateReducer 
    } = this.props
    const { isLoading } = gateReducer
    return (
      <Signup
        translate={translate}
        i18nState={i18nState}
        onHaveAccountPressed={() => this.onHaveAccountPressed()}
        submitPhoneNumber={this.submitPhoneNumber}
        isLoading={isLoading}
        signupState={signupState}
        apiReducer={apiReducer}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState,
    gateReducer: state.gateReducer,
    apiReducer: state.apiReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    gateAction: bindActionCreators(gateActions, dispatch),
    apiAction: bindActionCreators(apiActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignupContainer)

SignupContainer.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}

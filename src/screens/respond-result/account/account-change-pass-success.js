import React, { Component } from 'react'
import {
  Text, View, Image, StyleSheet, ImageBackground 
} from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import AppImages from '../../../assets/images'
import { TextFont, Themes } from '../../../ui'
import { MaterialButton } from '../../../components'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    paddingVertical: 100
  },
  image: {
    marginBottom: 25
  },
  content: {
    marginBottom: 25
  },
  button: {
    height: 50,
    width: 150 * Themes.Metrics.ratioScreen
  }
})

class AccountChangePassSuccess extends Component {
  onButtonClick = () => {}

  render() {
    const { t: translate } = this.context
    const { i18nState } = this.props
    return (
      <ImageBackground source={AppImages.bgAccountResult} style={styles.container}>
        <Image source={AppImages.iconSuccess} style={styles.image} />
        <TextFont font="LatoRegular" color={Themes.Colors.grey} style={styles.content}>
          {translate('Bạn đã đổi mật khẩu thành công')}
        </TextFont>
        <MaterialButton onPress={this.onButtonClick} style={styles.button} textFont="LatoRegular" textSize={12}>
          {translate('ĐỒNG Ý')}
        </MaterialButton>
      </ImageBackground>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountChangePassSuccess)

AccountChangePassSuccess.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}

import React, { Component } from 'react'
import {
  Text, View, WebView, ActivityIndicator 
} from 'react-native'
import styles from './news-detail.styles'
import { Themes } from '../../ui'

export default class NewsDetail extends Component {
  render() {
   
    return (
      <View style={styles.container}>
        {this.props.isLoadingNewsDetail ? (
          <ActivityIndicator size="small" color={Themes.Colors.red} />
        ) : (
          <WebView source={{ html: this.props.currentNewsDetail ? this.props.currentNewsDetail.content : '' }} />
        )}
      </View>
    )
  }
}

import React, { Component } from 'react'
import { Text, View } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import NewsDetail from './news-detail'

import { gateActions } from '../../redux/daily-gate'

class NewsDetailContainer extends Component {
  componentDidMount() {
 
    this.props.gateActions.getNewsDetail(this.props.newId)
  }

  componentWillUnmount() {
    this.props.gateActions.clearNewsDetail()
  }

  render() {
    const { isLoadingNewsDetail, currentNewsDetail } = this.props.gateReducer
    return <NewsDetail isLoadingNewsDetail={isLoadingNewsDetail} currentNewsDetail={currentNewsDetail} />
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState,
    gateReducer: state.gateReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    gateActions: bindActionCreators(gateActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewsDetailContainer)

NewsDetailContainer.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}

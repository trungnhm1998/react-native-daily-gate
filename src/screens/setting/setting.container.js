import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Setting from './setting'

class SettingContainer extends Component {

  constructor(props) {
    super(props);
    this.state = { switchValue: false }
  }
  
  componentWillMount() {
    // alert('mounted')
  }

  render() {
    const { t: translate } = this.context
    const { i18nState } = this.props
    return (
      <Setting
        switchValue={this.state.switchValue}
        translate={translate}
        i18nState={i18nState}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingContainer)

SettingContainer.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}

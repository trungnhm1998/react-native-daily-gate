import React, { Component } from 'react'
import {
  View, Image, Switch, TouchableOpacity, ScrollView
} from 'react-native'
import { Themes, TextFont } from '../../ui'
import styles from './setting.style'
import appSymbol from '../../assets/symbols'
import AppImages from '../../assets/images'
import { Icon } from '../../components'

export default class Setting extends Component {
  renderPerson = () => {
    return (
      <View style={styles.infor}>
        <TouchableOpacity style={styles.backgrImage}>
          <Icon name="icon_user" size={30} style={styles.imageAvatar} />
          <View style={styles.borderIconEdit}>
            <Image source={appSymbol.icon_edit} style={styles.iconEdit} />
          </View>
        </TouchableOpacity>
        <View style={styles.inforPerson}>
          <TextFont font="LatoRegular" style={styles.name}>Pham Duy Thai</TextFont>
          <TextFont font="LatoRegular" style={styles.phone}>098163714</TextFont>
          <TextFont font="LatoRegular" style={styles.email} size={12}>thaipdt@gmail.com</TextFont>
        </View>
        <TouchableOpacity style={styles.rightImage}>
          <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
        </TouchableOpacity>
      </View>
    )
  }

  renderService = (translate) => {
    return (
      <View style={styles.optionAccount}>
        <View style={styles.everyItem}>
          <View style={styles.itemLeft}>
            <View style={styles.borderIcon}>
              <Icon name="icon_sceurity" size={10} color={Themes.Colors.red} style={styles.iconArrow} /> 
            </View>
            <TextFont font="LatoRegular" size={12} style={styles.item}>{translate('Quản lý mã thẻ')}</TextFont>
          </View>
          <TouchableOpacity style={styles.itemRight}>
            <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderOptionForaccount = (translate) => {
    const { switchValue } = this.props
    return (
      <View style={styles.optionAccount}>
        <View style={styles.everyItem}>
          <View style={styles.itemLeft}>
            <View style={styles.borderIcon}>
              <Icon name="icon_sceurity" size={10} color={Themes.Colors.red} style={styles.iconArrow} /> 
            </View>
            <TextFont font="LatoRegular" size={12} style={styles.item}>{translate('Phương thức bảo mật GD')}</TextFont>
          </View>
          <View style={styles.itemRight}>
            <TextFont font="LatoRegular" size={12} style={styles.mkStyle}>{translate('MK Cấp 2')}</TextFont>
            <TouchableOpacity>
              <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.everyItem}>
          <View style={styles.itemLeft}>
            <View style={styles.borderIcon}>
              <Icon name="icon_lock" size={10} color={Themes.Colors.red} style={styles.iconArrow} /> 
            </View>
            <TextFont font="LatoRegular" size={12} style={styles.item}>{translate('Đổi mật khẩu đăng nhập')}</TextFont>
          </View>
          <TouchableOpacity style={styles.itemRight}>
            <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
          </TouchableOpacity>
        </View>
        <View style={styles.everyItem}>
          <View style={styles.itemLeft}>
            <View style={styles.borderIcon}>
              <Icon name="icon_lock_2" size={10} color={Themes.Colors.red} style={styles.iconArrow} /> 
            </View>
            <TextFont font="LatoRegular" size={12} style={styles.item}>{translate('Đổi mật khẩu giao dịch cấp 2')}</TextFont>
          </View>
          <TouchableOpacity style={styles.itemRight}>
            <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
          </TouchableOpacity>
        </View>
        <View style={styles.iteminfo}>
          <View style={styles.lastItemAccount}>
            <View style={styles.itemLeft}>
              <View style={styles.borderIcon}>
                <Icon name="icon_map" size={10} color={Themes.Colors.red} style={styles.iconArrow} /> 
              </View>
              <TextFont font="LatoRegular" size={12} style={styles.item}>{translate('Thông báo số dư')}</TextFont>
            </View>
          </View>
          <TouchableOpacity style={styles.itemRight}>
            <Switch value={switchValue} style={styles.switchStyle} ios_backgroundColor={Themes.Colors.red} />
          </TouchableOpacity>
        </View>

      </View>
    )
  }

  renderInfor = (translate) => {
    return (
      <View style={styles.optionAccount}>
        <View style={styles.everyItem}>
          <View style={styles.itemLeft}>
            <View style={styles.borderIcon}>
              <Icon name="icon_contract" size={10} color={Themes.Colors.red} style={styles.iconArrow} /> 
            </View>
            <TextFont font="LatoRegular" size={12} style={styles.item}>{translate('Chính sách bảo mật')}</TextFont>
          </View>
          <TouchableOpacity style={styles.itemRight}>
            <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
          </TouchableOpacity>
        </View>
        <View style={styles.everyItem}>
          <View style={styles.itemLeft}>
            <View style={styles.borderIcon}>
              <Icon name="icon_private" size={10} color={Themes.Colors.red} style={styles.iconArrow} /> 
            </View>
            <TextFont font="LatoRegular" size={12} style={styles.item}>{translate('Điều khoản sử dụng')}</TextFont>
          </View>
          <TouchableOpacity style={styles.itemRight}>
            <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
          </TouchableOpacity>
        </View>
        <View style={styles.everyItem}>
          <View style={styles.itemLeft}>
            <View style={styles.borderIcon}>
              <Icon name="icon_shopping" size={10} color={Themes.Colors.red} style={styles.iconArrow} /> 
            </View>
            <TextFont font="LatoRegular" size={12} style={styles.item}>{translate('Liên hệ hỗ trợ')}</TextFont>
          </View>
          <TouchableOpacity style={styles.itemRight}>
            <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
          </TouchableOpacity>
        </View>
        <View style={styles.everyItem}>
          <View style={styles.itemLeft}>
            <View style={styles.borderIcon}>
              <Icon name="icon_communicate" size={10} color={Themes.Colors.red} style={styles.iconArrow} /> 
            </View>
            <TextFont font="LatoRegular" size={12} style={styles.item}>{translate('Góp ý')}</TextFont>
          </View>
          <TouchableOpacity style={styles.itemRight}>
            <Icon name="icon_arrow" size={10} style={styles.iconArrow} /> 
          </TouchableOpacity>
        </View>
        <View style={styles.lastItem}>
          <View style={styles.itemLeft}>
            <View style={styles.borderIcon}>
              <Icon name="icon_info" size={10} color={Themes.Colors.red} style={styles.iconArrow} /> 
            </View>
            <TextFont font="LatoRegular" size={12} style={styles.item}>{translate('Phiên bản')}</TextFont>
          </View>
          <TouchableOpacity style={styles.itemRight}>
            <TextFont font="LatoRegular" size={12} style={styles.item}>v.1.0.0</TextFont>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderButton = (translate) => {
    return (
      <View style={styles.signOut}>
        <TextFont font="LatoRegular" style={styles.textSignOut} size={12}>{translate('Đăng xuất')}</TextFont>
      </View>
    )
  }

  render() {
    const { translate } = this.props
    return (
      <View style={styles.settingContainer}>
        {this.renderPerson(translate)}
        <ScrollView>
          <TextFont font="LatoRegular" style={styles.title} size={12}>{translate('DỊCH VỤ')}</TextFont>
          {this.renderService(translate)}
          <TextFont font="LatoRegular" style={styles.title} size={12}>{translate('BẢO MẬT TÀI KHOẢN')}</TextFont>
          {this.renderOptionForaccount(translate)}
          <TextFont font="LatoRegular" style={styles.title} size={12}>{translate('THÔNG TIN')}</TextFont>
          {this.renderInfor(translate)}
          {this.renderButton(translate)}
        </ScrollView>
      </View>
    )
  }
}
import { StyleSheet, Dimensions } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  settingContainer: {
    flex: 1,
    backgroundColor: Themes.Colors.backgroundColor,
  },
  infor: {
    flexDirection: 'row',
    backgroundColor: Themes.Colors.white,
    alignItems: 'center',
    ...Themes.styleGB.materialStyle
  }, 
  backgrImage: {
    borderRadius: Themes.Metrics.borderRadius,
    backgroundColor: Themes.Colors.backgroundColor,
    marginHorizontal: Themes.Metrics.marginHorizontal,
    marginVertical: Themes.Metrics.baseMargin
  },
  imageAvatar: {
    padding: Themes.Metrics.paddingVertical
  },
  name: {
    color: Themes.Colors.red,
    fontWeight: 'bold',
    textAlign: 'left'
  },
  phone: {
    color: Themes.Colors.green,
    textAlign: 'left'
  },
  email: {
    textAlign: 'left'
  },
  rightImage: {
    flexDirection: 'row',
    marginLeft: 'auto',
    marginRight: Themes.Metrics.marginRight
  },
  title: {
    paddingHorizontal: Themes.Metrics.basePadding,
    textAlign: 'left',
    paddingTop: Themes.Metrics.basePadding,
  },
  optionAccount: {
    backgroundColor: Themes.Colors.white,
    paddingLeft: Themes.Metrics.paddingHorizontal,
    paddingRight: Themes.Metrics.paddingHorizontal,
    ...Themes.styleGB.materialStyle

  },
  everyItem: {
    paddingVertical: Themes.Metrics.basePadding,
    borderBottomWidth: 0.3,
    borderColor: Themes.Colors.whiteOpacity,
    flexDirection: 'row',
    width: '100%',
  },
  itemLeft: {
    flexDirection: 'row',
  },
  itemRight: {
    flexDirection: 'row',
    marginLeft: 'auto'
  },
  item: {
    paddingLeft: Themes.Metrics.basePadding
  },
  lastItem: {
    paddingVertical: Themes.Metrics.basePadding,
    flexDirection: 'row',
  },
  lastItemAccount: {
    paddingVertical: Themes.Metrics.basePadding,
    flexDirection: 'row',
  },
  signOut: {
    backgroundColor: Themes.Colors.white,
    marginVertical: Themes.Metrics.baseMargin,
    paddingVertical: Themes.Metrics.basePadding,
    alignItems: 'flex-start',
    ...Themes.styleGB.materialStyle
  },
  textSignOut: {
    color: Themes.Colors.red,
    paddingLeft: Themes.Metrics.paddingVertical,
  },
  mkStyle: {
    marginRight: Themes.Metrics.smallMargin,
  },
  switchStyle: {
    transform: [{ scaleX: 0.7 }, { scaleY: 0.5 }],
  },
  borderIcon: {
    borderRadius: Themes.Metrics.borderRadius,
    backgroundColor: Themes.Colors.backgroundColor,
    padding: Themes.Metrics.smallPadding
  },
  iteminfo: {
    flexDirection: 'row'
  },
  time: {
    textAlign: 'left',
    paddingLeft: Themes.Metrics.basePadding
  },
  borderIconEdit: {
    width: '100%', 
    justifyContent: 'center', 
    alignItems: 'center', 
    marginTop: -15
  },
  iconEdit: {
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
  }
})

export default styles

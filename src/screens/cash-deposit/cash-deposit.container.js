import React, { Component } from 'react'
import { Text, View } from 'react-native'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { Navigation } from 'react-native-navigation'
import { gateActions } from '../../redux/daily-gate'
import { ScreenConfigs } from '../../navigation'
import { Themes } from '../../ui'

import CashDeposit from './cash-deposit'

class CashDepositContainer extends Component {
  componentDidMount() {
    this.props.gateActions.getBankList()
  }

  render() {
    const { t: translate } = this.context
    const { bankList } = this.props.gateReducer
    return <CashDeposit translate={translate} bankList={bankList} />
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState,
    gateReducer: state.gateReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    gateActions: bindActionCreators(gateActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CashDepositContainer)

CashDepositContainer.contextTypes = {
  t: PropTypes.func
}

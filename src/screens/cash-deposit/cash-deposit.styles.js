import { StyleSheet } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    width: Themes.Metrics.screenWidth,
    backgroundColor: Themes.Colors.backgroundColor,

    paddingTop: Themes.Metrics.paddingVertical * Themes.Metrics.ratioScreen,
    paddingHorizontal: Themes.Metrics.paddingHorizontal * Themes.Metrics.ratioScreen,
    alignItems: 'center'
  },
  bankPickerContainer: {
    width: '100%',
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginBottom: 10 * Themes.Metrics.ratioScreen
  },
  depositMethodContainer: {
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  depositMethodItem: {
    ...Themes.styleGB.materialStyle,
    borderRadius: Themes.Metrics.buttonRadius,
    backgroundColor: Themes.Colors.white,
    flexDirection: 'column',
    alignItems: 'center',
    flex: 1,
    paddingHorizontal: 5 * Themes.Metrics.ratioScreen,
    paddingVertical: 8 * Themes.Metrics.ratioScreen,
    height: 80 * Themes.Metrics.ratioScreen,
    justifyContent: 'center'
  },
  inputContainer: {
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  label: {
    textAlign: 'left',
    marginBottom: 10 * Themes.Metrics.ratioScreen
  },
  confirmButton: {
    width: '100%',
    marginTop: 20
  }
})

export default styles

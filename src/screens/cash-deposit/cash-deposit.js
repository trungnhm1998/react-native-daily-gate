import React, { Component } from 'react'
import {
  Text, View, TouchableOpacity, Image, TextInput, ScrollView, ActivityIndicator 
} from 'react-native'
import numeral from 'numeral'
import styles from './cash-deposit.styles'
import { ModalDropdownPicker, TcombForm, MaterialButton } from '../../components'
import AppImages from '../../assets/images'
import { TextFont, Themes } from '../../ui'
import appSymbols from '../../assets/symbols'
const tcomb = require('tcomb-form-native')
const { Form } = tcomb.form

const depositInfo = tcomb.struct({
  accountNumber: tcomb.String,
  moneyAmount: tcomb.String
})

const iconSrc = {
  TPBank: AppImages.iconBankTpb,
  Viettinbank: AppImages.iconBankTpb,
  Agribank: AppImages.iconBankTpb,
  Vietcombank: AppImages.iconBankTpb,
  DongAbank: AppImages.iconBankTpb
}

const depositMethodList = [
  {
    content: 'Nạp tiền mặt/UNC Ngân hàng',
    id: 'bank'
  },
  {
    content: 'Chuyển khoản tại cây ATM',
    id: 'atm'
  },
  {
    content: 'Chuyển khoản Internet Banking',
    id: 'ebank'
  }
]

export default class CashDeposit extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedBank: this.props.bankList && this.props.bankList[0] && (this.props.bankList[0].name ? this.props.bankList[0].name : null),
      selectedDepositMethodId: depositMethodList[0].id,
      inputValue: {
        accountNumber: '',
        moneyAmount: ''
      }
    }
  }

  rendeBankPicker = () => {
    const { translate } = this.props

    // tao ra mot array ten cac ngan hang
    const bankListName = this.props.bankList.map(item => item.name)

    // lay ra ngan hang co name trùng với ngân hàng đang được chọn
    const bank = this.props.bankList.filter((item) => {
      return item.name === this.state.selectedBank
    })
    const logo = bank[0] ? iconSrc[bank[0].id] : null

    return (
      <View style={styles.bankPickerContainer}>
        <TextFont font="LatoRegular" size={13} style={{ marginBottom: 10 * Themes.Metrics.ratioScreen }}>
          {translate('NGÂN HÀNG')}
        </TextFont>
        <ModalDropdownPicker
          iconLeft={logo}
          data={bankListName}
          selectedValue={this.state.selectedBank}
          onChangeValue={item => this.setState({ selectedBank: item })}
        />
      </View>
    )
  }

  onSelectDepositMethod = (id) => {
    this.setState({ selectedDepositMethodId: id })
  }

  renderDepositMethodItem = (content, id) => {
    return (
      <View style={{ ...styles.depositMethodItem, marginRight: id === 'ebank' ? 0 : 5 }}>
        <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => this.onSelectDepositMethod(id)}>
          <TextFont font="LatoRegular" size={12}>
            {content}
          </TextFont>

          <Image
            style={{ marginTop: 10 * Themes.Metrics.ratioScreen }}
            source={this.state.selectedDepositMethodId === id ? appSymbols.icon_input_check_act : appSymbols.icon_input_check_nor}
          />
        </TouchableOpacity>
      </View>
    )
  }

  renderDepositMethodRadioGroup = () => {
    const { translate } = this.props
    return (
      <View style={styles.depositMethodContainer}>
        <TextFont font="LatoRegular" size={13} style={{ marginBottom: 10 * Themes.Metrics.ratioScreen }}>
          {translate('PHƯƠNG THỨC NẠP TIỀN')}
        </TextFont>
        <View style={{ flexDirection: 'row', width: '100%' }}>{depositMethodList.map(item => this.renderDepositMethodItem(item.content, item.id))}</View>
      </View>
    )
  }

  onChangeInput(inputValue) {
    const formatedValue = { ...inputValue }
    if (inputValue.moneyAmount) formatedValue.moneyAmount = numeral(inputValue.moneyAmount).format('0,0')
    this.setState({
      inputValue: formatedValue
    })
  }

  renderForm = () => {
    const { translate } = this.props

    const layout = (locals) => {
      return (
        <View style={{ marginTop: 15 }}>
          <TextFont font="LatoLight" size={12} style={styles.label}>
            {translate('SỐ TÀI KHOẢN CHUYỂN')}
          </TextFont>
          <View>{locals.inputs.accountNumber}</View>
          <TextFont font="LatoLight" size={12} style={styles.label}>
            {translate('SỐ TIỀN CHUYỂN')}
          </TextFont>
          <View>{locals.inputs.moneyAmount}</View>
        </View>
      )
    }

    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        accountNumber: {
          template: TcombForm.InputFLoatingLabel_tcomb,
          label: 'Số tài khoản'
        },
        moneyAmount: {
          template: TcombForm.InputFLoatingLabel_tcomb,
          label: 'Số tiền VNĐ',
          keyboardType: 'numeric',
          config: {}
        }
      }
    }

    return (
      <Form
        ref={(form) => {
          this.form = form
        }}
        type={depositInfo}
        options={options}
        onChange={value => this.onChangeInput(value)}
        value={this.state.inputValue}
      />
    )
  }

  render() {
    const { translate } = this.props
    return (
      <View style={styles.container}>
        {this.props.bankList ? (
          <ScrollView style={{ flex: 1 }} scrollEnabled={false}>
            {this.rendeBankPicker()}
            {this.renderDepositMethodRadioGroup()}
            {this.renderForm()}
            <MaterialButton textFont="LatoBold" textSize={15} style={styles.confirmButton}>
              {translate('XÁC NHẬN')}
            </MaterialButton>
          </ScrollView>
        ) : (
          <ActivityIndicator size="small" color={Themes.Colors.red} />
        )}
      </View>
    )
  }
}

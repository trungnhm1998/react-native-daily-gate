import { StyleSheet } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    width: Themes.Metrics.screenWidth,
    backgroundColor: Themes.Colors.white,
    paddingHorizontal: Themes.Metrics.paddingHorizontal * Themes.Metrics.ratioScreen,
    paddingTop: Themes.Metrics.paddingVertical * Themes.Metrics.ratioScreen,
    alignItems: 'center'
  },
  CkContainer: {
    alignItems: 'flex-start',
   
    marginBottom: 10
  },
  CkItem: {
    paddingLeft: Themes.Metrics.paddingHorizontal * Themes.Metrics.ratioScreen,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: 60 * Themes.Metrics.ratioScreen,
    ...Themes.styleGB.materialStyle,
  },
  CkItemContent: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center'
  }
})

export default styles

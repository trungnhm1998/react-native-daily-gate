import React, { Component } from 'react'
import { Text, View } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Policies from './policies'

class PoliciesContainer extends Component {
  render() {
    const { t: translate } = this.context
    const { i18nState } = this.props
    return <Policies translate={translate} i18nState={i18nState} />
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PoliciesContainer)

PoliciesContainer.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}

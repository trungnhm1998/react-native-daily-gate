import React, { Component } from 'react'
import {
  Text, View, Image, TouchableOpacity, ScrollView 
} from 'react-native'
import appSymbols from '../../assets/symbols'
import styles from './policies.styles'
import { TextFont, Themes } from '../../ui'
import { Icon } from '../../components'
export default class Policies extends Component {
  onItemPress = item => () => {}

  // render list chiet khau
  renderListCK = (CkList) => {
    const { translate } = this.props

    const list = CkList.map(Ckitem => (
      <View style={styles.CkContainer} key={Ckitem.name}>
        <TextFont font="LatoRegular" color={Themes.Colors.grey}>
          {translate(Ckitem.name)}
        </TextFont>
        {Ckitem.list.map(item => (
          <TouchableOpacity key={item.name} style={styles.CkItem} onPress={this.onItemPress(item)}>
            <View style={styles.CkItemContent}>
              <Image
                source={item.iconSrc}
                resizeMode="center"
                style={{ alignSelf: 'center', width: 20 * Themes.Metrics.ratioScreen, marginRight: 40 * Themes.Metrics.ratioScreen }}
              />
              <TextFont font="LatoRegular" color={Themes.Colors.black}>
                {translate(item.name)}
              </TextFont>
            </View>
            <Icon name="icon_arrow" style={{ marginLeft: 'auto' }} />
          </TouchableOpacity>
        ))}
      </View>
    ))
    return list
  }

  render() {
    const { CkList } = this.props
    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>{this.renderListCK(CkList)}</ScrollView>
      </View>
    )
  }
}

Policies.defaultProps = {
  CkList: [
    {
      name: 'CHIẾT KHẤU MÃ THẺ',
      list: [
        {
          name: 'Thẻ game online',
          iconSrc: appSymbols.icon_cs_thegameonline
        },
        {
          name: 'Thẻ điện thoại',
          iconSrc: appSymbols.icon_cs_carddt
        }
      ]
    },
    {
      name: 'CHIẾT KHẤU NẠP PHÍ',
      list: [
        {
          name: 'Nạp BẠC game FPT nhanh',
          iconSrc: appSymbols.icon_cs_bacnhanh
        },
        {
          name: 'Nạp tiền game',
          iconSrc: appSymbols.icon_cs_tiengame
        },
        {
          name: 'Nạp tiền điện thoại',
          iconSrc: appSymbols.icon_cs_naptiendt
        }
      ]
    },
    {
      name: 'CHIẾT KHẤU THANH TOÁN',
      list: [
        {
          name: 'Cước di động trả sau',
          iconSrc: appSymbols.icon_cs_cuocpaysau
        },
        {
          name: 'Chuyển tiền sim đa năng',
          iconSrc: appSymbols.icon_cs_simdanang
        }
      ]
    }
  ]
}

// {
//   name: '',
//   list: [
//     {
//       name: '',
//       iconSrc: appSymbols
//     }
//   ]
// }

import Root from './root'
import Signup from './signup'
import SignupPassword from './signup-password'
import Login from './login'
import Home from './home'
import Report from './report'
import Policies from './policies'
import ServicePlace from './service-place'
import Setting from './setting'
import AccountActiveSuccess from './respond-result/account/account-active-success'
import AccountChangePassSuccess from './respond-result/account/account-change-pass-success'
import RegisterInfo from './register-info'
import SecurityPage from './register-info/security-page'
import Notice from './notice'
import NewsDetail from './news-detail'
import PasswordPage from './register-info/password-page'
import CashDeposit from './cash-deposit'
import ListDeposit from './list-deposit'
const AppScreens = {
  Root,
  Signup,
  SignupPassword,
  Login,
  Home,
  Report,
  Policies,
  ServicePlace,
  Setting,
  AccountActiveSuccess,
  AccountChangePassSuccess,
  RegisterInfo,
  SecurityPage,
  Notice,
  NewsDetail,
  PasswordPage,
  CashDeposit,
  ListDeposit
}

export default AppScreens

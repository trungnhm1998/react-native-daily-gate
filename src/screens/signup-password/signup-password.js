import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Button,
  Image,
  ScrollView,
  Modal,
  ActivityIndicator,
  TouchableOpacity
} from 'react-native'
import TimerMixin from 'react-timer-mixin'
import { TextFont, Themes } from '../../ui'
import { TcombForm, MaterialButton } from '../../components'
import appSymbols from '../../assets/symbols'
import styles from './signup-password.styles'
import AppAPI from '../../api';
import StatusBarAndroid from '../../components/statusBar';

const tcomb = require('tcomb-form-native')
const { Form } = tcomb.form

const userAccount = tcomb.struct({
  phoneNumber: tcomb.String,
  activationCode: tcomb.String,
  password: tcomb.String,
  rePassword: tcomb.String
})

export default class SignupPassword extends Component {
  constructor(props) {
    super(props)

    this.state = {
      value: {
        phoneNumber: props.phoneNumber,
        activationCode: '',
        password: '',
        rePassword: ''
      },
      time: props.otpTimeOut / 1000,
      passwordLength: true,
      passwordMatch: true,
    }

    this.timer = undefined
    this.startCountDownTimer()
  }

  startCountDownTimer = () => {
    AppAPI.DailyGateAPI.sendActivationCode()
    const { otpTimeOut } = this.props
    this.timer = TimerMixin.setInterval(() => {
      const { time } = this.state
      this.setState({ time: time - 1 })
    }, 1000)
  }

  onSubmitPressed = () => {
    const { passwordLength, passwordMatch, value } = this.state
    const { password, activationCode } = value
    const { onSubmitPressed, phoneNumber } = this.props
    if (passwordLength && passwordMatch) {
      onSubmitPressed(phoneNumber, password, activationCode)
    }
  }

  renderSubmitButton(translate) {
    return (
      <MaterialButton onPress={() => this.onSubmitPressed()}>
        {translate('HOÀN TẤT')}
      </MaterialButton>
    )
  }

  onChange(value) {
    const { password, rePassword } = value

    // check if typing password/rePassword
    if (password !== this.state.value.password || rePassword !== this.state.value.rePassword) {
      this.setState({
        value,
        passwordLength: password.length >= 6,
        passwordMatch: password === rePassword
      })
    } else {
      this.setState({ value })
    }
  }

  renderCheckIcon() {
    return <Image source={appSymbols.icon_input_check} />
  }

  renderValidationCodeCountDown(translate) {
    if (this.timer !== undefined) {
      return (
        <TextFont color={Themes.Colors.blueText}>
          {`${this.state.time}s`}
        </TextFont>
      )
    }

    return (
      <TouchableOpacity onPress={this.startCountDownTimer}>
        <TextFont color={Themes.Colors.redText}>
          {translate('GỬI LẠI PIN')}
        </TextFont>
      </TouchableOpacity>
    )
  }

  renderForm(translate) {
    const layout = (locals) => {
      return (
        <View style={{ marginTop: 15 }}>
          <View>{locals.inputs.phoneNumber}</View>
          <View>{locals.inputs.activationCode}</View>
          <TextFont color={Themes.Colors.greyText} font="LatoLight" style={{ textAlign: 'left' }}>
            {translate('Mật khẩu phải ít nhất 6 ký tự:')}
          </TextFont>
          <View style={{ marginTop: 15 }}>{locals.inputs.password}</View>
          <View>{locals.inputs.rePassword}</View>
        </View>
      )
    }

    const { phoneNumber } = this.props
    const { passwordLength, passwordMatch } = this.state
    let hasError = false
    let errorMsg = ''
    if (!passwordLength) {
      errorMsg = translate('Mật khẩu phải có chiều dài tối thiểu là 6 ký tự')
      hasError = true
    }

    if (!passwordMatch) {
      errorMsg = translate('Mật khẩu nhập vào phải trùng với nhau')
      hasError = true
    }

    const options = {
      stylesheet: TcombForm.styles,
      template: layout,
      fields: {
        phoneNumber: {
          template: TcombForm.InputFLoatingLabel_tcomb,
          label: translate('Số điện thoại'),
          keyboardType: 'phone-pad',
          value: phoneNumber,
          editable: false,
          config: {
            rightAddon: this.renderCheckIcon(),
            tips: translate('Mã PIN đã gửi về SĐT. Mời nhập bên dưới'),
          }
          // hasError: true,
          // error: 'test'
        },
        activationCode: {
          template: TcombForm.InputFLoatingLabel_tcomb,
          label: translate('Nhập mã kích hoạt'),
          keyboardType: 'phone-pad',
          config: {
            rightAddon: this.renderValidationCodeCountDown(translate)
          }
        },
        password: {
          template: TcombForm.InputFLoatingLabel_tcomb,
          label: translate('Mật khẩu mới'),
          secureTextEntry: true,
          hasError,
          error: errorMsg
        },
        rePassword: {
          template: TcombForm.InputFLoatingLabel_tcomb,
          label: translate('Nhập lại mật khẩu mới'),
          secureTextEntry: true
        }
      }
    }

    return (
      <Form
        ref={(form) => {
          this.form = form
        }}
        type={userAccount}
        options={options}
        onChange={value => this.onChange(value)}
        value={this.state.value}
      />
    )
  }

  render() {
    const { translate, otpTimeOut } = this.props
    if (this.state.time <= 0 && this.timer !== undefined) {
      TimerMixin.clearInterval(this.timer)
      this.timer = undefined
      TimerMixin.setTimeout(() => {
        this.setState({ time: otpTimeOut / 1000 })
      }, 500)
    }
    return (
      <ScrollView style={styles.container} scrollEnabled={false}>
        <StatusBarAndroid translucent={false} />
        {this.renderForm(translate)}
        {this.renderSubmitButton(translate)}
      </ScrollView>
    )
  }
}

import { StyleSheet } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: Themes.Metrics.paddingHorizontal
  }
})

export default styles
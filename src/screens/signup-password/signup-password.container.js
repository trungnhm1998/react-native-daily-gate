import React, { Component } from 'react'
import { BackHandler } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'
import { Navigation } from 'react-native-navigation'
import SignupPassword from './signup-password'
import { gateActions } from '../../redux/daily-gate'
import AppConstants from '../../configs/constant.config'
import { ScreenConfigs, AppNavigation } from '../../navigation'

class SignupPasswordContainer extends Component {

  onSubmitPressed = (phoneNumber, password, activationCode) => {
    const { gateAction, signupState } = this.props
    if (signupState === AppConstants.SignupType.SIGN_UP) {
      gateAction.createNewAccount(phoneNumber, password, activationCode, {
        LatoBold: () => AppNavigation.navToHome()
      })
    } else {
      gateAction.updateAccountPassword(phoneNumber, password, activationCode)
    }
    // const str = 'key1=value1&key2=value2&ob={"test":"ok"}'
    // let test = AppHelper.RSAService.createSignature(str)
    // let verified = AppHelper.RSAService.verifySignature(str, test)
    // let encryptTest = AppHelper.RSAService.Encrypt(str)
    // let decryptedTest = AppHelper.RSAService.Decrypt(encryptTest)
    // console.log('test', test, verified)
    // console.log('encrypted:', encryptTest)
    // console.log('decrypted:', decryptedTest)
  }

  componentDidMount() {
    // prevent user on android to go back and spam phoneNumber verify
    BackHandler.addEventListener('hardwareBackPress', () => {
      return true
    })
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress')
  }

  render() {
    const { t: translate } = this.context
    const { i18nState, phoneNumber } = this.props
    const { isLoading, content } = this.props.gateReducer
    const { resendOTPTimeout } = content
    return (
      <SignupPassword
        translate={translate}
        i18nState={i18nState}
        isLoading={isLoading}
        phoneNumber={phoneNumber}
        otpTimeOut={resendOTPTimeout}
        onSubmitPressed={this.onSubmitPressed}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState,
    gateReducer: state.gateReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    gateAction: bindActionCreators(gateActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignupPasswordContainer)

SignupPasswordContainer.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}

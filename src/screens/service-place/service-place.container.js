import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'
import { Themes } from '../../ui'
import { Button } from '../../components'
import ServicePlace from './service-place'
import { gateActions } from '../../redux/daily-gate'
class ServicePlaceContainer extends Component {
  getServicePlaces = () => {
    const { getServicePlaces } = this.props.gateAction
    getServicePlaces()
  }

  componentDidMount() {
    this.getServicePlaces()
  }

  render() {
    return <ServicePlace />
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState,
    servicePlaceList: state.gateReducer.servicePlaceList
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    gateAction: bindActionCreators(gateActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ServicePlaceContainer)

ServicePlaceContainer.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}

// Set scrollable tabview with animation draft
// constructor(props) {
//   super(props)
//   this.state = { page: 0 }
// }

// changePage = page => () => {
//   this.setState({ page })
// }

// render() {
//   return (
//     <View style={{ flex: 1 }}>
//       <ScrollableTabView style={{ marginTop: 20 }} initialPage={0} page={this.state.page} renderTabBar={() => <DefaultTabBar />}>
//         <View style={{ width: Themes.Metrics.screenWidth }} tabLabel="MEGA 645">
//           <Text>SS</Text>
//         </View>
//         <View style={{ width: Themes.Metrics.screenWidth }} tabLabel="MAX 4D">
//           <Text>Page 1</Text>
//         </View>
//         <View style={{ width: Themes.Metrics.screenWidth }} tabLabel="POWER 655">
//           <Text>Page 2</Text>
//         </View>
//       </ScrollableTabView>
//       <View
//         style={{
//           flex: 1,
//           flexDirection: 'row',
//           position: 'absolute',
//           top: 20,

//           width: '100%',
//           backgroundColor: 'white',
//           height: 50
//         }}
//       >
//         <Button onPress={this.changePage(1)}>SS</Button>
//         <Button onPress={this.changePage(2)}>SS</Button>
//       </View>
//     </View>
//   )
// }

import { StyleSheet } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    width: Themes.Metrics.screenWidth,
    backgroundColor: Themes.Colors.white,

    alignItems: 'center'
  },
  topBar: {
    height: 50 * Themes.Metrics.ratioScreen,
    backgroundColor: Themes.Colors.white,
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    paddingLeft: Themes.Metrics.paddingHorizontal * Themes.Metrics.ratioScreen,
    ...Themes.styleGB.materialStyle
  },
  topButton: {
    width: 50 * Themes.Metrics.ratioScreen,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50 * Themes.Metrics.ratioScreen
  },
  topButton_selected: {
    width: 50 * Themes.Metrics.ratioScreen,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50 * Themes.Metrics.ratioScreen,
    borderBottomColor: Themes.Colors.red,
    borderBottomWidth: 3
  }
})

export default styles

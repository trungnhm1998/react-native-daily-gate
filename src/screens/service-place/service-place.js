import React, { Component } from 'react'
import {
  Text, View, TouchableOpacity, Image, Platform 
} from 'react-native'
import { ModalDropdownPicker, Bill } from '../../components'
import styles from './service-place.styles'
import { Icon } from '../../components'
import { Themes } from '../../ui'
import AppImages from '../../assets/images'
export default class ServicePlace extends Component {
  constructor(props) {
    super(props)
    this.state = {
      cityIndex: 0,
      provineIndex: 0,

      currentPage: 'map'
    }
  }

  renderMapPage = () => {
    return <Image resizeMode="contain" source={AppImages.bgMapDemo} />
  }

  renderAddressPage = () => {}

  renderTopButton = () => {
    return (
      <View style={{ marginLeft: 'auto', flexDirection: 'row', alignItems: 'center' }}>
        <TouchableOpacity onPress={() => this.setState({ currentPage: 'map' })}>
          <View style={this.state.currentPage === 'map' ? styles.topButton_selected : styles.topButton}>
            <Icon size={20} name="icon_map" color={Themes.Colors.black} />
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => this.setState({ currentPage: 'address' })}>
          <View style={this.state.currentPage === 'address' ? styles.topButton_selected : styles.topButton}>
            <Icon size={15} name="icon_hamburger" color={Themes.Colors.black} />
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  // renderTopPicker = () => {
  //   const cityList = this.props.cityList
  //   if (Platform.OS === 'ios') {
  //     return (
  //       <View style={{ marginRight: 'auto', flexDirection: 'row' }}>
  //         <PickerIOS
  //           selectedValue={cityList[this.state.cityIndex].name}
  //           style={{ height: 50, width: 150 }}
  //           onValueChange={(itemValue, itemIndex) => this.setState({
  //             cityIndex: itemIndex,
  //             provineValue: cityList[itemIndex].provineList ? cityList[itemIndex].provineList[0] : null
  //           })
  //           }
  //         >
  //           {cityList.map(item => (
  //             <PickerIOS.Item label={item.name} value={item.name} />
  //           ))}
  //         </PickerIOS>
  //         {cityList[this.state.cityIndex].provineList ? (
  //           <PickerIOS
  //             selectedValue={cityList[this.state.cityIndex].provineList[this.state.provineIndex]}
  //             style={{ height: 50, width: 120 }}
  //             onValueChange={(itemValue, itemIndex) => this.setState({ provineIndex: itemIndex })}
  //           >
  //             {cityList[this.state.cityIndex].provineList.map(item => (
  //               <PickerIOS.Item label={item} value={item} />
  //             ))}
  //           </PickerIOS>
  //         ) : null}
  //       </View>
  //     )
  //   }
  //   return (
  //     <View style={{ marginRight: 'auto', flexDirection: 'row' }}>
  //       <Picker
  //         selectedValue={cityList[this.state.cityIndex].name}
  //         style={{ height: 50, width: 150 }}
  //         onValueChange={(itemValue, itemIndex) => this.setState({
  //           cityIndex: itemIndex,
  //           provineValue: cityList[itemIndex].provineList ? cityList[itemIndex].provineList[0] : null
  //         })
  //         }
  //       >
  //         {cityList.map(item => (
  //           <Picker.Item label={item.name} value={item.name} />
  //         ))}
  //       </Picker>
  //       {cityList[this.state.cityIndex].provineList ? (
  //         <Picker
  //           selectedValue={cityList[this.state.cityIndex].provineList[this.state.provineIndex]}
  //           style={{ height: 50, width: 120 }}
  //           onValueChange={(itemValue, itemIndex) => this.setState({ provineIndex: itemIndex })}
  //         >
  //           {cityList[this.state.cityIndex].provineList.map(item => (
  //             <Picker.Item label={item} value={item} />
  //           ))}
  //         </Picker>
  //       ) : null}
  //     </View>
  //   )
  // }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topBar}>{this.renderTopButton()}</View>
        <View style={{ flex: 1 }} />

        <View style={{ paddingVertical: 20 }}>
          <ModalDropdownPicker
            onChangeValue={item => this.setState({ selectedProvine: item })}
            selectedValue={this.state.selectedProvine}
            data={this.props.servicePlaceList}
            style={{ width: 200 }}
          />
        </View>
     
      </View>
    )
  }
}

ServicePlace.defaultProps = {
  cityList: [
    {
      name: 'TP.HCM',
      provineList: ['Quận 1', 'Quận 2', 'Quận 3', 'Quận 4', 'Quận 5']
    },
    {
      name: 'VŨNG TÀU',
      provineList: ['Phường 1', 'Phường 2', 'Phường 3', 'Phường 4', 'Phường 5']
    }
  ],
  servicePlaceList: ['Quận 1', 'Quận 2', 'Quận 3', 'Quận 4', 'Quận 5', 'Quận 6'],
  billList: [
    [{ name: 'Data dat 1', value: 1 }, { name: 'Data 2', value: 2 }],
    [{ name: 'Data dat3', value: 3 }, { name: 'Data 4', value: 4 }],
    [{ name: 'Data 5', value: 5, highlightValue: true }]
  ]
}

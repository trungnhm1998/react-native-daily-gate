import React, { Component } from 'react'
import {
  Text, View, ActivityIndicator, FlatList, TouchableOpacity, Platform 
} from 'react-native'
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view'
import styles from './notice.styles'
import { Themes, TextFont } from '../../ui'
import { Icon } from '../../components'
import noticeContainer from './notice.container'

export default class Notice extends Component {
  constructor(props) {
    super(props)
    this.state = { pageCount: 1 }
  }

  componentWillMount() {
    // cha hieu sao chi co thang IOS la phai load list news len,
    // thang Android thi tu dong bat event onChangeTab của Scrolltabbar khi mới mountComponent nen no load tu dong listnew luon, khong can de day
    if (Platform.OS === 'ios') {
      if (this.props.categoryNotifyList) {
        const firstItem = this.props.categoryNotifyList[0]
        const { categoryId } = firstItem
        const page = 1
        this.props.getListNewsBycategory(categoryId, page)
      }
    }
  }

  componentWillUnmount() {
    this.props.clearListNews()
  }

  renderNewsItem = (item) => {
    return (
      <View style={styles.newsItem}>
        <TouchableOpacity onPress={() => this.props.onNewsItemPress(item.id)}>
          <TextFont style={{ textAlign: 'left' }} numberOfLines={3} size={15} ellipsizeMode="tail" font="LatoBold">
            {item.title}
          </TextFont>
        </TouchableOpacity>
        <View style={{ flexDirection: 'row', marginVertical: 5 * Themes.Metrics.ratioScreen }}>
          <Icon name="icon_clock" style={{ marginRight: 5 }} />
          <TextFont style={{ textAlign: 'left' }} size={10} font="LatoBold" color={Themes.Colors.lightGrey}>
            {item.createDate}
          </TextFont>
        </View>
      </View>
    )
  }

  loadMoreNews = categoryId => () => {
    this.setState(
      prevState => ({ pageCount: prevState.pageCount + 1 }),
      () => {
        console.log('load page', this.state.pageCount)
        this.props.getListNewsBycategory(categoryId, this.state.pageCount)
      }
    )
  }

  renderListNews = (categoryId) => {
    if (this.props.isLoadingListNew && this.state.pageCount === 1) return <ActivityIndicator size="small" color={Themes.Colors.red} />
    return (
      <FlatList
        data={this.props.currentListNew}
        renderItem={({ item }) => this.renderNewsItem(item)}
        onEndReachedThreshold={0.5}
        showsVerticalScrollIndicator={false}
        onEndReached={this.loadMoreNews(categoryId)}
      />
    )
  }

  handleChangeTab = ({ i, ref, from }) => {
    const item = this.props.categoryNotifyList[i]
    const { categoryId } = item
    const page = 1
    this.setState({ pageCount: 1 }, () => this.props.getListNewsBycategory(categoryId, page))
  }

  renderScrollTab = () => {
    return (
      <ScrollableTabView
        initialPage={0}
        onChangeTab={this.handleChangeTab}
        renderTabBar={() => <ScrollableTabBar style={{ paddingHorizontal: Themes.Metrics.basePadding }} />}
      >
        {this.props.categoryNotifyList.map(item => (
          <View style={styles.tabContainer} tabLabel={item.title}>
            {this.renderListNews(item.categoryId)}
            {this.props.isLoadingListNew && this.state.pageCount !== 1 ? <ActivityIndicator size="small" color={Themes.Colors.red} /> : null}
          </View>
        ))}
      </ScrollableTabView>
    )
  }

  render() {
    return <View style={styles.container}>{this.renderScrollTab()}</View>
  }
}

Notice.defaultProps = {
  categoryNotifyList: []
}

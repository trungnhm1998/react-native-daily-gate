import React, { Component } from 'react'
import { Text, View } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Navigation } from 'react-native-navigation'
import { ScreenConfigs } from '../../navigation'
import Notice from './notice'

import { gateActions } from '../../redux/daily-gate'
import { Themes } from '../../ui'

class NoticeContainer extends Component {
  getListNewsBycategory = (id, page) => {
    console.log('Get list')
    this.props.gateActions.getListNewsByCategory(id, page)
  }

  clearListNews = () => {
    console.log('clear list')
    this.props.gateActions.clearListNews()
  }

  onNewsItemPress = (newsId) => {
    const { t: translate } = this.context
    const passProps = {
      newsId
    }
    Navigation.push(this.props.componentId, {
      component: {
        name: ScreenConfigs.NEWS_DETAIL,
        options: {
          topBar: {
            visible: true,
            drawBehind: false,
            // animate: false,
            backButton: {
              color: Themes.Colors.white
            },
            background: {
              color: Themes.Colors.topBarColor,
              translucent: false
            },
            title: {
              text: translate('Thông báo'),
              color: Themes.Colors.white
            }
          }
        },
        passProps
      }
    })
  }

  render() {
    const { isLoadingListNew, currentListNew } = this.props.gateReducer
    const { categoryNotifyList } = this.props.gateReducer.content
    return (
      <Notice
        getListNewsBycategory={this.getListNewsBycategory}
        categoryNotifyList={categoryNotifyList}
        isLoadingListNew={isLoadingListNew}
        currentListNew={currentListNew}
        clearListNews={this.clearListNews}
        onNewsItemPress={this.onNewsItemPress}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState,
    gateReducer: state.gateReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    gateActions: bindActionCreators(gateActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NoticeContainer)

NoticeContainer.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}

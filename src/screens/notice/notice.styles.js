import { StyleSheet } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    width: Themes.Metrics.screenWidth,
    backgroundColor: Themes.Colors.white,

    paddingTop: Themes.Metrics.paddingVertical * Themes.Metrics.ratioScreen,
    alignItems: 'center'
  },
  tabContainer: {
    flex: 1,
    width: Themes.Metrics.screenWidth
  },
  newsItem: {
    flexDirection: 'column',
    paddingHorizontal: Themes.Metrics.paddingHorizontal * Themes.Metrics.ratioScreen,
    paddingVertical: 10 * Themes.Metrics.ratioScreen,
    height: 80 * Themes.Metrics.ratioScreen,

    borderBottomColor: Themes.Colors.gray,
    borderBottomWidth: 0.4
  }
})

export default styles

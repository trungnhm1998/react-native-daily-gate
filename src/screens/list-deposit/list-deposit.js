import React, { Component } from 'react'
import {
  View, Image, TouchableOpacity, FlatList
} from 'react-native'
import _ from 'lodash'
import { Themes, TextFont } from '../../ui'
import styles from './list-deposit.style'
import appSymbol from '../../assets/symbols'
import AppImages from '../../assets/images'
import { Icon, TcombForm, ModalDropdownPicker } from '../../components'

export default class ListDeposit extends Component {
  constructor(props) {
    super(props)
  }

  _keyExtractor = (item, index) => item.id


  renderIconItem = (item) => {
    if (item.id === 'CashRecharge') {
      return appSymbol.icon_pt_1_naptien
    }
    if (item.id === 'OnlineRecharge') {
      return appSymbol.icon_pt_2_naptienonline
    }
    if (item.id === 'RechargeGate') {
      return appSymbol.icon_pt_3_naptaigate
    }
    if (item.id === 'RechargeByCard') {
      return appSymbol.icon_pt_4_napthecao
    }
    if (item.id === 'BankSupport') {
      return appSymbol.icon_pt_5_banking
    }
  }

  onPressItems = (item) => {
    if (item.id === 'CashRecharge') {
      
    }
    if (item.id === 'OnlineRecharge') {
      
    }
    if (item.id === 'RechargeGate') {
      
    }
    if (item.id === 'RechargeByCard') {
      
    }
    if (item.id === 'BankSupport') {
      
    }
  }

  renderItem = (item) => {
    return (
      <TouchableOpacity style={styles.everyItem} onPress={this.onPressItems(item)}>
        <View style={styles.itemLeft}>
          <View style={styles.borderIcon}>
            <Image source={this.renderIconItem(item)} color={Themes.Colors.red} /> 
          </View>
          <View style={styles.contentText}>
            <TextFont font="LatoRegular" size={12} style={styles.item}>{item.title}</TextFont>
            { item.note && <TextFont font="LatoRegular" size={12} style={styles.itemNote}>{item.note}</TextFont>}
          </View>
        </View>
        <View style={styles.itemRight}>
          <Icon name="icon_arrow" size={15} color={Themes.Colors.red} /> 
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const { translate } = this.props
    return (
      <View style={styles.rechargeContainer}>
        <TextFont font="LatoLight" size={15} style={styles.title}>{translate('Phương thức nạp tiền')}</TextFont>
        <FlatList 
          data={this.props.gateReducer.listCashDeposit}
          renderItem={({ item }) => this.renderItem(item)}
          keyExtractor={this._keyExtractor}
        />
      </View>
    )
  }
}

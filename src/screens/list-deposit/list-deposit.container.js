import React, { Component } from 'react'
import { Text, View } from 'react-native'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { Navigation } from 'react-native-navigation'
import { gateActions } from '../../redux/daily-gate'
import { ScreenConfigs } from '../../navigation'
import { Themes } from '../../ui'

import ListDeposit from './list-deposit'

class ListDepositContainer extends Component {
  componentDidMount() {
    this.props.gateActions.getListCashDeposit()
  }

  render() {
    const { t: translate } = this.context
    const { i18nState, gateReducer } = this.props
    return (
      <ListDeposit 
        translate={translate}
        i18nState={i18nState}
        gateReducer={gateReducer}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    i18nState: state.i18nState,
    gateReducer: state.gateReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    gateActions: bindActionCreators(gateActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListDepositContainer)

ListDepositContainer.contextTypes = {
  t: PropTypes.func
}

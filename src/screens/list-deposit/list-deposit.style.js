import { StyleSheet, Dimensions } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  rechargeContainer: {
    flex: 1,
    backgroundColor: Themes.Colors.backgroundColor,
    paddingHorizontal: Themes.Metrics.baseMargin
  },
  title: {
    textAlign: 'left',
    paddingLeft: Themes.Metrics.baseMargin,
    color: Themes.Colors.black,
    marginVertical: Themes.Metrics.baseMargin
  },
  everyItem: {
    paddingVertical: Themes.Metrics.basePadding,
    borderWidth: 0.3,
    borderColor: Themes.Colors.whiteOpacity,
    flexDirection: 'row',
    width: '100%',
    ...Themes.styleGB.materialStyle,
    marginBottom: Themes.Metrics.baseMargin,
    borderRadius: 10
  },
  itemLeft: {
    flexDirection: 'row',
  },
  borderIcon: {
    marginHorizontal: Themes.Metrics.baseMargin,
    marginVertical: Themes.Metrics.smallMargin
  },
  itemRight: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginRight: Themes.Metrics.baseMargin + Themes.Metrics.smallMargin
  },
  item: {
    paddingLeft: Themes.Metrics.basePadding,
    textAlign: 'left',
    paddingBottom: Themes.Metrics.smallPadding
  },
  itemNote: {
    color: Themes.Colors.gray
  },
  contentText: {
    justifyContent: 'center',
  }
})

export default styles

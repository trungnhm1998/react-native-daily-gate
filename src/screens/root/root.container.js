import React, { Component, AsyncStorage } from 'react'
import { View, Text } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Navigation } from 'react-native-navigation'
import { bindActionCreators } from 'redux'
import { persistStore } from 'redux-persist'
import { ScreenConfigs, AppNavigation } from '../../navigation'
import { Themes } from '../../ui'
import { gateActions } from '../../redux/daily-gate'
import { AppStoreProvider } from '../../configs/store.config'


class RootContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    const { userInfo } = this.props.gateReducer
    // if this is first time using this app, then userInfo will not store in stored before => just nav to login , else get userInfo from Persist, then check if Token is validate
    if (!userInfo) {
      AppNavigation.navToLogin()
    } else if (userInfo.accessToken) {
      this.props.gateAction.checkAccessToken(userInfo.accessToken, AppNavigation)
    } else {
      AppNavigation.navToLogin()
    }

    // console.log('root.container.js mounted', this.props)
    const { getInitConfig } = this.props.gateAction
    getInitConfig()
  }

  // static getDerivedStateFromProps(nextProps, prevState) {
  //   console.log('getDerviedStateFromProps', nextProps, prevState)
  //   if (nextProps.gateReducer.update || nextProps.gateReducer.forceUpdate) {
  //     console.log('appNeed update')
  //   }
  //   return null
  // }

  render() {
    return <View />
  }
}

const mapStateToProps = (state) => {
  return {
    gateReducer: state.gateReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    gateAction: bindActionCreators(gateActions, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RootContainer)

RootContainer.contextTypes = {
  t: PropTypes.func
}

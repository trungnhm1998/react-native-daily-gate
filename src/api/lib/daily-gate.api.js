// import md5 from 'md5'
import Request from '../middleware/daily-gate.request'
import API from '../../configs/api.config'
import appSymbol from '../../assets/symbols'
import AppHelper from '../../helpers';
// import { API_USER } from '../../configs/api.config'

const DailyGateAPI = {
  updateAccountPassword(payload) {
    const { phoneNumber, password } = payload
    console.log('api::updateAccountPassword', payload)

    const mockResponse = {
      code: 1000,
      message: 'Update password succes'
    }

    const testError = false
    const errorMsg = 'some error'

    return new Promise(async (resolve, reject) => {
      setTimeout(() => {
        resolve(mockResponse)
      }, 200)

      if (testError) {
        reject(errorMsg)
      }
    })
  },
  createNewAccount(payload) {
    const { phoneNumber, password } = payload
    console.log('api::createNewAccount', payload)

    const mockResponse = {
      code: 1000,
      message: 'create account success'
    }

    const testError = false
    const errorMsg = 'some error'

    return new Promise(async (resolve, reject) => {
      setTimeout(() => {
        resolve(mockResponse)
      }, 200)

      if (testError) {
        reject(errorMsg)
      }
    })
  },
  checkOtp(payload) {
    const { otp } = payload
    console.log('api::checkOtp', payload)

    const mockResponse = {
      code: 1000,
      message: 'Correct OTP'
    }

    const testError = false
    const errorMsg = 'some error'

    return new Promise(async (resolve, reject) => {
      setTimeout(() => {
        resolve(mockResponse)
      }, 5000)

      if (testError) {
        reject(errorMsg)
      }
    })
  },
  checkAccountExist(data) {
    const { username } = data
    console.log('api::checkAccountExist', username)
    return Request.callAPI(`${API.GATE}/checkAccountExist`, { username })
  },

  checkCMNDNumber(data) {
    const { CMNDNumber } = data

    console.log('api:checkCMNDNumber', CMNDNumber)

    const mockResponse = {
      code: 1000
      // message: 'cmnd exists'
    }

    const testError = false
    const errorMsg = 'some error'

    return new Promise(async (resolve, reject) => {
      setTimeout(() => {
        resolve(mockResponse)
      }, 200)

      if (testError) {
        reject(errorMsg)
      }
    })
  },

  getListAddress() {
    const mockResponse = {
      codeCity: 1000,
      cityList: [
        {
          name: 'TP.HCM',
          provine: ['Quận 1', 'Quận 2', 'Quận 3', 'Quận 4', 'Quận 5']
        },
        {
          name: 'VŨNG TÀU',
          provine: ['Phường 1', 'Phường 2', 'Phường 3', 'Phường 4', 'Phường 5']
        }
      ]
    }

    const testError = false
    const errorMsg = 'some error'

    return new Promise(async (resolve, reject) => {
      setTimeout(() => {
        resolve(mockResponse)
      }, 300)

      if (testError) {
        reject(errorMsg)
      }
    })
  },

  verifyInforRegister(data) {
    console.log('api:verifyInforRegister ', data)

    const mockResponse = {
      code: 1000,
      data: {
        userName: 'Pham Duy Thai',
        email: 'thai@gmail.com',
        cmndNumber: '1468141321',
        fontImage: 'http:ghergkmfbfb',
        backsideImage: 'url',
        address: 'so 5 duong phan chua trinh',
        tinhTP: 'TP.HCM',
        quanHuyen: 'Quan 5'
      }
    }

    const testError = false
    const errorMsg = 'some error'

    return new Promise(async (resolve, reject) => {
      setTimeout(() => {
        resolve(mockResponse)
      }, 200)

      if (testError) {
        reject(errorMsg)
      }
    })
  },

  authorizeUser(data) {
    const { phone, password } = data

    const testError = false
    const errorMsg = 'some error'

    const mockResponse = {
      code: 1000,
      data: {
        accountId: 'wqoiu342io12',
        money: 15000000,
        email: 'abc@gmail.com',
        identityNumber: '123123123',
        frontIdentityUrl: 'https://example.com/front.jpg',
        backIdentityUrl: 'https://example.com/back.jpg',
        address: '123 nguyen tat thanh',
        city: 'Hồ Chí Minh',
        distrist: 'Quận 7',
        displayName: 'SevenA',
        avatar: 'https://example.com/avatar/qowueoquoqwirq.png',
        phone: '84909000100',
        isVerify: 'false',
        accessToken: '1OTEiLCJpYXQiOjE0OTQ1NzY5NzR9.Xu_vUlTCaGXUpSv8P6YkFKz4OrxRebie7cB2uFYC1us==',
        tradeSecurity: 0
      }
    }

    return new Promise(async (resolve, reject) => {
      setTimeout(() => {
        resolve(mockResponse)
      }, 500)

      if (testError) {
        reject(errorMsg)
      }
    })
  },
  sendActivationCode() {
    console.log('api::sendActivationCode')
  },

  getInitConfig() {
    console.log('api::getInitConfig')
    return AppHelper.APIManager.getInstance().callAPI(`${API.GATE}/getInitConfig`)
    // return Request.callAPI(`${API.GATE}/getInitConfig`)
  },
  getListNewsByCategory(data) {
    const { id, page } = data

    const testError = false
    const errorMsg = 'some error'

    const mockResponse = {
      code: 1000,

      listNews: [
        {
          id: '515',
          title: 'Khuyến mãi nạp thẻ ngày 29/02/2019 bla bla...',
          categoryId: 'a1bc',
          thumbnailUrl: 'https://example.com/t.jpg',
          createDate: '08/11/2017'
        },
        {
          id: '512',
          title: 'Khuyến mãi nạp thẻ ngày 29/02/2019 bla bla...',
          categoryId: 'a1bc',
          thumbnailUrl: 'https://example.com/t.jpg',
          createDate: '08/11/2017'
        },
        {
          id: '511',
          title: 'Khuyến mãi nạp thẻ ngày 29/02/2019 bla bla...',
          categoryId: 'a1bc',
          thumbnailUrl: 'https://example.com/t.jpg',
          createDate: '08/11/2017'
        },
        {
          id: '513',
          title: 'Khuyến mãi nạp thẻ ngày 29/02/2019 bla bla...',
          categoryId: 'a1bc',
          thumbnailUrl: 'https://example.com/t.jpg',
          createDate: '08/11/2017'
        },
        {
          id: '514',
          title: 'Khuyến mãi nạp thẻ ngày 29/02/2019 bla bla...',
          categoryId: 'a1bc',
          thumbnailUrl: 'https://example.com/t.jpg',
          createDate: '08/11/2017'
        },
        {
          id: '516',
          title: 'Khuyến mãi nạp thẻ ngày 29/02/2019 bla bla...',
          categoryId: 'a1bc',
          thumbnailUrl: 'https://example.com/t.jpg',
          createDate: '08/11/2017'
        },
        {
          id: '517',
          title: 'Khuyến mãi nạp kim cương đến 50% trong ngày đầu tiên ra mắt ứng dụng. Nhanh tay hốt hàng Bạc Gate nào các chế ơi,',
          categoryId: 'a1bc',
          thumbnailUrl: 'https://example.com/t.jpg',
          createDate: '08/11/2017'
        },
        {
          id: '518',
          title: 'Khuyến mãi nạp thẻ ngày 29/02/2019 bla bla...',
          categoryId: 'a1bc',
          thumbnailUrl: 'https://example.com/t.jpg',
          createDate: '08/11/2017'
        }
      ],
      message: 'lấy data thành công'
    }

    return new Promise(async (resolve, reject) => {
      setTimeout(() => {
        resolve(mockResponse)
      }, 500)

      if (testError) {
        reject(errorMsg)
      }
    })
  },
  getNewsDetail(data) {
    const { newsId } = data

    const testError = false
    const errorMsg = 'some error'

    const mockResponse = {
      code: 1000,

      data: {
        id: '515',
        title: 'Khuyến mãi nạp thẻ ngày 29/02/2019 bla bla...',
        categoryId: 'a1bc',
        thumbnailUrl: 'https://example.com/t.jpg',
        createDate: 0,
        content: '<h1>Hello world</h1>'
      },
      message: 'lấy data thành công'
    }

    return new Promise(async (resolve, reject) => {
      setTimeout(() => {
        resolve(mockResponse)
      }, 500)

      if (testError) {
        reject(errorMsg)
      }
    })
  },
  checkAccessToken(data) {
    const { accessToken } = data

    const testError = false
    const errorMsg = 'some error'

    const mockResponse = {
      code: 200,
      data: {
        accountId: 'wqoiu342io12',
        money: 15000000,
        email: 'abc@gmail.com',
        identityNumber: '123123123',
        frontIdentityUrl: 'https://example.com/front.jpg',
        backIdentityUrl: 'https://example.com/back.jpg',
        address: '123 nguyen tat thanh',
        city: 'Hồ Chí Minh',
        distrist: 'Quận 7',
        displayName: 'SevenA',
        avatar: 'https://example.com/avatar/qowueoquoqwirq.png',
        phone: '84909000100',
        isVerify: 'false',
        accessToken: '1OTEiLCJpYXQiOjE0OTQ1NzY5NzR9.Xu_vUlTCaGXUpSv8P6YkFKz4OrxRebie7cB2uFYC1us==',
        tradeSecurity: 0
      }
    }

    return new Promise(async (resolve, reject) => {
      setTimeout(() => {
        resolve(mockResponse)
      }, 500)

      if (testError) {
        reject(errorMsg)
      }
    })
  },

  getListCashDeposit() {
    const testError = false
    const errorMsg = 'some error'

    const mockResponse = {
      code: 1000,
      data: [
        {
          id: 'CashRecharge',
          title: 'NẠP TIỀN MẶT',
          note: '(Tiền mặt - UNC, ATM, chuyển khoản IB)',
        },
        {
          id: 'OnlineRecharge',
          title: 'NẠP TIỀN ONLINE',
          note: '(ATM, VISA)',
        },
        {
          id: 'RechargeGate',
          title: 'NẠP TIỀN TẠI GATE',
        },
        {
          id: 'RechargeByCard',
          title: 'NẠP TIỀN BẰNG THẺ CÀO',
        },
        {
          id: 'BankSupport',
          title: 'NGÂN HÀNG HỖ TRỢ THANH TOÁN',
        }
      ]
    }
    return new Promise(async (resolve, reject) => {
      setTimeout(() => {
        resolve(mockResponse)
      }, 500)

      if (testError) {
        reject(errorMsg)
      }
    })
  },


  getBankList() {
    const testError = false
    const errorMsg = 'some error'

    const mockResponse = {
      code: 1000,
      data: {
        bankList: [
          {
            name: 'Tien phong bank',
            id: 'TPBank'
          },
          {
            name: 'Viettin bank',
            id: 'Viettinbank'
          },
          {
            name: 'Agribank',
            id: 'Agribank'
          },
          {
            name: 'Vietcombank',
            id: 'Vietcombank'
          },
          {
            name: 'Dong A bank',
            id: 'DongAbank'
          }
        ]
      }
    }

    return new Promise(async (resolve, reject) => {
      setTimeout(() => {
        resolve(mockResponse)
      }, 500)

      if (testError) {
        reject(errorMsg)
      }
    })
  }
}

export default DailyGateAPI

import _ from 'lodash'
import AppHelper from '../../helpers';

const jsonToFormEncoded = (data) => {
  const str = []
  Object.keys(data).forEach((p, index) => {
    if (_.isObject(data[p])) {
      const d = data[p]
      Object.keys(d).forEach((o, index) => {
        if (Object.prototype.hasOwnProperty.call(d, o) && d[o]) {
          str.push(`${p}[${encodeURIComponent(o)}]=${encodeURIComponent(d[o])}`)
        }
      })
    } else if (Object.prototype.hasOwnProperty.call(data, p) && data[p]) {
      str.push(`${encodeURIComponent(p)}=${encodeURIComponent(data[p])}`)
    }
  })
}

const sortParams = (params = {}) => {
  return Object.keys(params).sort()
    .map((key) => {
      if (typeof params[key] === 'object') {
        return `${key}=${JSON.stringify(params[key])}`
      }
      return `${key}=${params[key]}`
    })
    .join('&')
}

const generateParams = (params, method) => {
  // const data = JSON.stringify(params.data)
  // console.log('params', params)
  const data = params !== null ? JSON.stringify(params) : null
  // console.log('data', data)
  const sortedParams = sortParams(params)
  // console.log('sortedParams', sortedParams)
  const signature = AppHelper.RSAService.createSignature(sortedParams)
  // console.log('signature', signature)

  const contentType = data ? 'application/json' : 'application/x-www-form-urlencoded; charset=UTF-8'

  return {
    method,
    headers: {
      'Content-Type': contentType,
      signature
    },
    // body: jsonToFormEncoded({ ...params, data })
    body: data
  }
}

const RequestDocdac = {
  callAPI(api, data, method = 'post') {
    return new Promise(async (resolve, reject) => {
      try {
        const generateData = generateParams(data, method)
        const response = await fetch(api, generateData)
        const json = await response.json()

        if (__DEV__) {
          console.log('[REQUEST]', api, data, generateData, json)
        }
        setTimeout(() => {
          resolve(json)
        }, 500)
      } catch (error) {
        if (__DEV__) {
          console.log(error)
        }
        reject(error)
      }
    })
  }
}
export default RequestDocdac

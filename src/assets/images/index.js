const AppImages = {
  avatarDemo: require('./avatar_demo.png'),
  bgHome: require('./bg_home.png'),
  iconBankTpb: require('./icon_bank_tpb.png'),
  iconBankVcb: require('./icon_bank_vcb.png'),
  iconHomeBannerDemo: require('./icon_home_banner-demo.png'),
  iconNoticeFail: require('./icon_notice_fail.png'),
  iconNoticeSuccess: require('./icon_notice_success.png'),
  iconSuccess: require('./icon_success.png'),
  bgHeader: require('./bg_header.png'),
  bgAccountResult: require('./bg_account_result.png'),
  bgMapDemo: require('./bg_map_demo.png')
}

export default AppImages

import { Navigation } from 'react-native-navigation'
import ScreenConfigs from './screen.config'
import { Themes } from '../ui'
import { Icon } from '../components'
import AppConstants from '../configs/constant.config'

const AppNavigation = {
  navToRoot: () => {
    Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: ScreenConfigs.ROOT
              }
            }
          ]
        }
      }
    })
  },
  navToLogin: () => {
    Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: ScreenConfigs.LOGIN,
                options: {
                  // statusBar: {
                  //   visible: true,
                  //   drawBehind: true,
                  //   backgroundColor: 'rgba(255, 255, 255, 0)'
                  // }
                }
              }
            }
          ],
        }
      }
    })
  },
  // navigation to home.container with bottom tabs
  navToHome: () => {
    Promise.all([
      Icon.getImageSource('logo-white-1', 15, Themes.Colors.tabIconGrey),
      Icon.getImageSource('icon-report', 20, Themes.Colors.tabIconGrey),
      Icon.getImageSource('icon-policies', 20, Themes.Colors.tabIconGrey),
      Icon.getImageSource('icon-service-place', 18, Themes.Colors.tabIconGrey),
      Icon.getImageSource('icon-setting', 20, Themes.Colors.tabIconGrey)
    ])
      .then((values) => {
        const tabs = [
          {
            stack: {
              children: [
                {
                  component: {
                    name: ScreenConfigs.HOME,
                    options: {
                      topBar: {
                        visible: false,
                        drawBehind: true
                      },

                      bottomTab: {
                        text: 'GATE',
                        icon: values[0]
                      }
                    }
                  }
                }
              ]
            }
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: ScreenConfigs.REPORT,
                    options: {
                      topBar: {
                        visible: true,
                        drawBehind: false,
                        noBorder: true,
                        animated: false,
                        background: {
                          color: Themes.Colors.navColor
                        },
                        title: {
                          color: '#fff',
                          text: 'Báo cáo',
                          alignment: 'center',
                          fontFamily: 'SpoqaHanSans-Bold'
                        }
                      },

                      bottomTab: {
                        text: 'Báo cáo',
                        icon: values[1]
                      }
                    }
                  }
                }
              ]
            }
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: ScreenConfigs.POLICIES,
                    options: {
                      topBar: {
                        visible: true,
                        drawBehind: false,
                        noBorder: true,
                        animated: false,
                        background: {
                          color: Themes.Colors.navColor
                        },
                        title: {
                          color: '#fff',
                          text: 'Chính sách',
                          alignment: 'center',
                          fontFamily: 'SpoqaHanSans-Bold'
                        }
                      },

                      bottomTab: {
                        text: 'Chính sách',
                        icon: values[2]
                      }
                    }
                  }
                }
              ]
            }
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: ScreenConfigs.SERVICE_PLACE,
                    options: {
                      topBar: {
                        visible: true,
                        drawBehind: false,
                        noBorder: true,
                        animated: false,
                        background: {
                          color: Themes.Colors.navColor
                        },
                        title: {
                          color: '#fff',
                          text: 'Điạ điểm giao dịch',
                          alignment: 'center',
                          fontFamily: 'SpoqaHanSans-Bold'
                        }
                      },

                      bottomTab: {
                        text: 'Điểm GD',
                        icon: values[3]
                      }
                    }
                  }
                }
              ]
            }
          },
          {
            stack: {
              children: [
                {
                  component: {
                    name: ScreenConfigs.SETTING,
                    options: {
                      topBar: {
                        visible: true,
                        drawBehind: false,
                        noBorder: true,
                        animated: false,
                        background: {
                          color: Themes.Colors.navColor
                        },
                        title: {
                          color: '#fff',
                          text: 'Cài đặt',
                          alignment: 'center',
                          fontFamily: 'SpoqaHanSans-Bold'
                        }
                      },
                      bottomTab: {
                        text: 'Cài đặt',
                        icon: values[4]
                      }
                    }
                  }
                }
              ]
            }
          }
        ]
        Navigation.setRoot({
          root: {
            bottomTabs: {
              children: tabs,
              options: {
                bottomTabs: {
                  // backgroundColor: '#bdbdbd',
                  backgroundColor: '#fff',
                  titleDisplayMode: 'alwaysShow'
                },
                layout: {
                  orientation: 'portrait'
                }
              }
            }
          }
        })
      })
      .catch((error) => {
        console.error('error', error)
      })
  },
  screen: {
    signup: (passProps, translate) => {
      return {
        component: {
          name: ScreenConfigs.SIGNUP,
          passProps: {
            // signupState: type
            ...passProps
          },
          options: {
            topBar: {
              topMargin: 20 * Themes.Metrics.ratioScreen,
              visible: true,
              drawBehind: false,
              animate: false,
              noBorder: true,
              backButton: {
                color: Themes.Colors.white
              },
              background: {
                color: Themes.Colors.topBarColor,
                translucent: false
              },
              title: {
                text: passProps.signupState === AppConstants.SignupType.SIGN_UP ? translate('Đăng ký') : translate('Quên mật khẩu'),
                color: Themes.Colors.white
              }
            }
          }
        }
      }
    }
  }
}

export default AppNavigation

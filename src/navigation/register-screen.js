import React from 'react'
import { ImageBackground } from 'react-native'
import { Navigation } from 'react-native-navigation'
import AppScreens from '../screens'
import ScreenConfigs from './screen.config'
import StoreProvider from '../configs/store-provider.config'
import AppImages from '../assets/images'
import { Loading } from '../components'

type Props = {}

const WrappedComponent = (Component) => {
  return (props: Props) => {
    const HOCstyles = {
      width: '100%',
      height: '100%'
    }
    const HOC = () => (
      <StoreProvider>
        <ImageBackground source={AppImages.bgHome} style={HOCstyles}>
          <Component {...props} />
          <Loading />
        </ImageBackground>
      </StoreProvider>
    )

    return <HOC />
  }
}

export default () => {
  Navigation.registerComponent(ScreenConfigs.ROOT, () => WrappedComponent(AppScreens.Root))
  Navigation.registerComponent(ScreenConfigs.SIGNUP, () => WrappedComponent(AppScreens.Signup))
  Navigation.registerComponent(ScreenConfigs.SIGNUP_PASSWORD, () => WrappedComponent(AppScreens.SignupPassword))
  Navigation.registerComponent(ScreenConfigs.LOGIN, () => WrappedComponent(AppScreens.Login))
  Navigation.registerComponent(ScreenConfigs.HOME, () => WrappedComponent(AppScreens.Home))
  Navigation.registerComponent(ScreenConfigs.REPORT, () => WrappedComponent(AppScreens.Report))
  Navigation.registerComponent(ScreenConfigs.POLICIES, () => WrappedComponent(AppScreens.Policies))
  Navigation.registerComponent(ScreenConfigs.SERVICE_PLACE, () => WrappedComponent(AppScreens.ServicePlace))
  Navigation.registerComponent(ScreenConfigs.SETTING, () => WrappedComponent(AppScreens.Setting))
  Navigation.registerComponent(ScreenConfigs.ACCOUNT_ACTIVE_SUCCESS, () => WrappedComponent(AppScreens.AccountActiveSuccess))
  Navigation.registerComponent(ScreenConfigs.ACCOUNT_CHANGE_PASS_SUCCESS, () => WrappedComponent(AppScreens.AccountChangePassSuccess))
  Navigation.registerComponent(ScreenConfigs.REGISTER_INFO, () => WrappedComponent(AppScreens.RegisterInfo))
  Navigation.registerComponent(ScreenConfigs.SECURITY_PAGE, () => WrappedComponent(AppScreens.SecurityPage))
  Navigation.registerComponent(ScreenConfigs.NOTICE, () => WrappedComponent(AppScreens.Notice))
  Navigation.registerComponent(ScreenConfigs.NEWS_DETAIL, () => WrappedComponent(AppScreens.NewsDetail))
  Navigation.registerComponent(ScreenConfigs.PASSWORD_PAGE, () => WrappedComponent(AppScreens.PasswordPage))
  Navigation.registerComponent(ScreenConfigs.CASH_DEPOSIT, () => WrappedComponent(AppScreens.CashDeposit))
  Navigation.registerComponent(ScreenConfigs.LIST_DEPOSIT, () => WrappedComponent(AppScreens.ListDeposit))
  console.info('All screens have been registered...')
}

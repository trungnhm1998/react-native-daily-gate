import React from 'react'
import PropTypes from 'prop-types'
import { Dimensions, Text, StyleSheet } from 'react-native'
import Fonts from '../themes/fonts'
const { width, height } = Dimensions.get('window')
const flattenStyle = StyleSheet.flatten
const realWidth = height > width ? width : height

const TextFont = ({
  style, children, color, font, size, ...props 
}) => {
  const fontSize = size
  const scaledFontSize = Math.round((fontSize * realWidth) / 375)

  return (
    <Text
      {...props}
      style={[
        {
          backgroundColor: 'transparent',
          color,
          fontFamily: Fonts.type[font],
          fontSize: scaledFontSize,
          textAlign: 'center'
        },
        style
      ]}
    >
      {children}
    </Text>
  )
}

TextFont.propTypes = {
  style: Text.propTypes.style,
  font: PropTypes.string,
  size: PropTypes.number,
  color: PropTypes.string
}

TextFont.defaultProps = {
  style: {},
  color: '#000',
  font: 'LatoRegular',
  size: 16
}

export default TextFont

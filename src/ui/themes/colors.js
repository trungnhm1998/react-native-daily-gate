const colors = {
  green: '#1dcc8c',
  blue: '#457BFE',
  orange: '#fe8a34',
  violet: '#977eb7',
  pink: '#f76c8b',
  red: '#E02729',
  darkRed: '#d02d47',
  lightGreen: '#21caad',
  lightBlue: '#4A90E2',
  lightGrey: '#D8D8D8',
  grey: '#4A4A4A',
  tabIconGrey: '#A1A1A1',
  white: '#fff',
  black: '#000000',
  gray: '#808080',
  whiteOpacity: '#DBE4E1',

  greenText: '#00A664',
  blueText: '#4A90E2',
  redText: '#E02729',
  greyText: '#4A4A4A',

  drawerOverlayColor: 'rgba(82,213,56,0.9)',
  inputTextColor: '#585858',
  inputBorderColor: '#dedede',
  headingColor: '#21caad',
  buttonBackgroundColor: '#1dcc8c',
  buttonBackgroundUnderlayColor: '#1ec286',
  normalTextColor: '#3a3a3a',

  borderColor: '#dedede',
  borderColorError: '#ea373a',
  topBarColor: '#E02729',
  backgroundColor: '#F4F5FA',
  btnColor1: '#E02729',
  btnColor2: 'red',
  navColor: '#c92033',
  max4dColor: '#800593'
}

export default colors

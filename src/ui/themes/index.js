import Colors from './colors'
import Fonts from './fonts'
import Metrics from './metrics'
import styleGB from './styles'

export {
  Colors,
  Fonts,
  Metrics,
  styleGB,
}

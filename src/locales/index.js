import vi from './vi/vi.locale'
// import en from './en/en.locale'

const translations = {
  vi,
  // en
}

export default translations

/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import { Navigation } from 'react-native-navigation'

import App from './src'
import ScreenConfigs from './src/navigation/screen.config'
import { Themes } from './src/ui'

Navigation.registerComponent(ScreenConfigs.ENTRY_APP, () => App)

Navigation.events().registerAppLaunchedListener(() => {
  // TODO: move default options to style config
  Navigation.setDefaultOptions({
    statusBar: {
      style: 'light'
    },
    topBar: {
      visible: false,
      drawBehind: true,
      animate: false
    },
    bottomTab: {
      iconColor: Themes.Colors.tabIconGrey,
      textColor: Themes.Colors.tabIconGrey,
      selectedIconColor: Themes.Colors.red,
      selectedTextColor: Themes.Colors.red,
    },
  })
  Navigation.setRoot({
    root: {
      component: {
        name: ScreenConfigs.ENTRY_APP
      }
    }
  })
})

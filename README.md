DailyGate App
===

This app is native side for mobile phone of
[DailyGate](https://daily.gate.vn/home/ "website")
backed by
[React Native](https://facebook.github.io/react-native/)

## Setup

This project was created by `react-native init`

Below you'll find information about performing common tasks.

We using iphone 6 simulator for development and design, notice and follow the [scripts section](#avaliable-scripts) for more infos

## Table of Contents

* [Updating to New Releases](#updating-to-new-releases)
* [Available Scripts](#available-scripts)
  * [npm start](#npm-start)
  * [npm test](#npm-test)
  * [npm run ios:build](#npm-run-ios:build)
  * [npm run android](#npm-run-android)
* [Adding Flow](#adding-flow)
* [Coding rules](#coding-rules)
* [Troubleshooting](#troubleshooting)
  * [Fix 'stream' module not found](#fix-'stream'-module-not-found)

## Updating to New Releases

Upgrading to a new version of React Native(Currently 0.58.3) requires updating the `react-native` and `react` package versions, and setting the correct `sdkVersion` in `app.json`. See the [versioning guide](https://github.com/react-community/create-react-native-app/blob/master/VERSIONS.md) for up-to-date information about package version compatibility.

## Available Scripts

Project initialized with NPM using `npm run` for execute script

### `npm start`

Runs your app in development mode. With scripts:
```
npm run ios:build // this will run iphone 6 simulator
# or android
npm run android // must run android emulator first either connect android device with debug mode on
```

Sometimes you may need to reset or clear the React Native packager's cache. To do so, you can pass the `--reset-cache` flag to the start script:

```
npm start -- --reset-cache
# or
npm run start:reset
```

### `npm test`

We haven't fully tested with jest yet

Runs the [jest](https://github.com/facebook/jest) test runner on your tests.

### `npm run ios:build`

Will open iphone 6 simulator and run the app with development mode on it.

### `npm run android`

Like `npm run ios:build`, but also attempts to open your app on a connected Android device or emulator. Requires an installation of Android build tools (see [React Native docs](https://facebook.github.io/react-native/docs/getting-started.html) for detailed setup).

You can also use Genymotion/Android Emulator as your Android emulator. Once you've finished setting up the native build environment, make sure that using the right `adb` for development enviroment

## Coding rules

## Troubleshooting
### Fix 'stream' module not found:
Error message:
> bundling failed: Error: Unable to resolve module `stream`

run this command:
`./node_modules/.bin/rn-nodeify --hack --install`